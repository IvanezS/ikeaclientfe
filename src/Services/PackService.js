import axios from 'axios'

async function getAll() {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + '/DPackets');
  return response.data;
};

async function get(id) {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DPackets/${id}`);
  return response.data;
};

async function create (data) {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + '/DPackets', data);
  return response;
};

async function getByPartId(id) {
  const response = await  axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DPackets/Part/${id}`);
  return response.data;
};

async function update(id, dataa, paramss) {
  const response = await axios({method: 'put', url: process.env.REACT_APP_WEB_HOST_API_URL +  `/DPackets/${ id }`, data: dataa, params : paramss});
  return response;
};

async function remove (id) {
  const response = await  axios.delete(process.env.REACT_APP_WEB_HOST_API_URL + `/DPackets/${id}`);
  return response;
};

async function markScan (id) {
  const response = await  axios.post(process.env.REACT_APP_WEB_HOST_API_URL + `/DPackets/MarkScan?id=${id}`);
  return response;
};

async function removeAll () {
  const response = await  axios.delete(process.env.REACT_APP_WEB_HOST_API_URL + `/DPackets`);
  return response;
};

async function findByTitle (title) {
  const response = await  axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DPackets?title=${title}`);
  return response.data;
};

async function CreatePDF(id) {
  const response = await  axios.post(process.env.REACT_APP_WEB_HOST_API_URL + `/DPackets/CreatePDF?Id=${id}`);
  return response;
};

async function PrintRpackPDF(id) {
  const response = await  axios.post(process.env.REACT_APP_WEB_HOST_API_URL + `/DPackets/PrintRpackPDF?Id=${id}`);
  return response;
};

export default {
  getAll,
  get,
  getByPartId,
  create,
  update,
  remove,
  removeAll,
  findByTitle,
  markScan,
  CreatePDF,
  PrintRpackPDF
};