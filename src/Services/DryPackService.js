import axios from 'axios'

async function getAll() {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + '/DDryPackets');
  return response.data;
};

async function get(id) {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DDryPackets/${id}`);
  return response.data;
};

async function create (dataa) {
  const response = await axios({method: 'post', url: process.env.REACT_APP_WEB_HOST_API_URL +  '/DDryPackets', data: dataa});
  return response;
};

async function update (id, data) {
  const response = await  axios.put(process.env.REACT_APP_WEB_HOST_API_URL + `/DDryPackets/${id}`, data);
  return response;
};

async function remove (id) {
  const response = await  axios.delete(process.env.REACT_APP_WEB_HOST_API_URL + `/DDryPackets/${id}`);
  return response;
};

async function CreatePDF(id) {
  const response = await  axios.post(`/DDryPackets/CreatePDF?Id=${id}`);
  return response;
};

async function PrintRpackPDF(id) {
  const response = await  axios.post(`/DDryPackets/PrintRpackPDF?Id=${id}`);
  return response;
};

export default {
  getAll,
  get,
  create,
  update,
  remove,
  CreatePDF,
  PrintRpackPDF
};