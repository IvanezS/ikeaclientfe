import axios from 'axios'

async function getAll() {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + '/Smena');
  return response.data;
};

async function create(data) {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/Smena", data);
  return response;
};


async function finish() {
  const response = await axios.put(process.env.REACT_APP_WEB_HOST_API_URL + "/Smena");
  return response;
};

export default {
  getAll,
  create,
  finish
};