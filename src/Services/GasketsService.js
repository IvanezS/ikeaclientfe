import axios from 'axios'

async function getAll() {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + '/DGaskets');
  return response.data;
};

async function  get(id ) {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DGaskets/${id}`);
  return response.data;
};

async function create (data) {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/DGaskets", data);
  return response;
};

async function update (id, data) {
  const response = await  axios.put(process.env.REACT_APP_WEB_HOST_API_URL + `/DGaskets/${id}`, data);
  return response;
};

async function remove (id) {
  const response = await  axios.delete(process.env.REACT_APP_WEB_HOST_API_URL + `/DGaskets/${id}`);
  return response;
};

async function ImportCSV() {
  const response = await  axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/DGaskets/ImportCSV");
  return response;
};

async function ExportCSV(){
  const response = await  axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/DGaskets/ExportCSV");
  return response;
};


export default {
  getAll,
  get,
  create,
  update,
  remove,
  ImportCSV,
  ExportCSV
};