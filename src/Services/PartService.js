import axios from 'axios'

async function getAll() {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + '/DParts');
  return response.data;
};

async function get(id) {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DParts/${id}`);
  return response.data;
};

async function create (data, ProvId) {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL +  `/DParts?ProvId=${ProvId}`, data);
  return response;
};

async function update(id, dataa, paramss) {
  var urla = process.env.REACT_APP_WEB_HOST_API_URL + '/DParts/' + id;
  const response = await axios({method: 'PUT', url: urla, data: dataa, params : paramss});
  return response;
};

async function remove(id) {
  const response = await axios.delete(process.env.REACT_APP_WEB_HOST_API_URL + `/DParts/${id}`);
  return response;
};

async function removeAll() {
  const response = await axios.delete(process.env.REACT_APP_WEB_HOST_API_URL + `/DParts`);
  return response;
};

async function findByTitle (title) {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DParts?title=${title}`);
  return response.data;
};

export default {
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  findByTitle
};