import axios from 'axios'

async function getAll() {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + '/Logs');
  return response.data;
};

export default {
  getAll
};