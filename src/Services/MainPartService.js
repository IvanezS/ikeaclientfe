import axios from 'axios'

async function getAll() {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + '/DMainParts');
  return response.data;
};

async function get(id) {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DMainParts/${id}`);
  return response.data;
};

async function create (data) {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL +  `/DMainParts`, data);
  return response;
};

async function update(id, data) {
  const response = await axios.put(process.env.REACT_APP_WEB_HOST_API_URL + `/DMainParts/${id}`, data);
  return response;
};

async function remove(id) {
  const response = await axios.delete(process.env.REACT_APP_WEB_HOST_API_URL + `/DMainParts/${id}`);
  return response;
};



export default {
  getAll,
  get,
  create,
  update,
  remove
};