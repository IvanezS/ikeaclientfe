import axios from 'axios'

async function getAll() {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + '/DResortedPackets');
  return response.data;
};

async function get(id) {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DResortedPackets/${id}`);
  return response.data;
};

async function create (data) {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + '/DResortedPackets', data);
  return response;
};

async function getByPartId(id) {
  const response = await  axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DResortedPackets/Part/${id}`);
  return response.data;
};

async function update(id, data) {
  const response = await  axios.put(process.env.REACT_APP_WEB_HOST_API_URL + `/DResortedPackets/${id}`, data);
  return response;
};

async function remove (id) {
  const response = await  axios.delete(process.env.REACT_APP_WEB_HOST_API_URL + `/DResortedPackets/${id}`);
  return response;
};

async function markScan (id) {
  const response = await  axios.post(process.env.REACT_APP_WEB_HOST_API_URL + `/DResortedPackets/MarkScan?id=${id}`);
  return response;
};

async function removeAll () {
  const response = await  axios.delete(process.env.REACT_APP_WEB_HOST_API_URL + `/DResortedPackets`);
  return response;
};

async function findByTitle (title) {
  const response = await  axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DResortedPackets?title=${title}`);
  return response.data;
};

async function CreatePDF(id) {
  const response = await  axios.post(process.env.REACT_APP_WEB_HOST_API_URL + `/DResortedPackets/CreatePDF?Id=${id}`);
  return response;
};

async function PrintRpackPDF(id) {
  const response = await  axios.post(process.env.REACT_APP_WEB_HOST_API_URL + `/DResortedPackets/PrintRpackPDF?Id=${id}`);
  return response;
};

export default {
  getAll,
  get,
  getByPartId,
  create,
  update,
  remove,
  removeAll,
  findByTitle,
  CreatePDF,
  PrintRpackPDF,
  markScan
};