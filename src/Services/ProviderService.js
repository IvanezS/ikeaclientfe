import axios from 'axios'

async function  getAll() {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + '/DProviders');
  return response.data;
};

async function  get(id ) {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DProviders/${id}`);
  return response.data;
};

async function create (data) {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/DProviders", data);
  return response;
};

async function update (id, data) {
  const response = await  axios.put(process.env.REACT_APP_WEB_HOST_API_URL + `/DProviders/${id}`, data);
  return response;
};

async function remove (id) {
  const response = await  axios.delete(process.env.REACT_APP_WEB_HOST_API_URL + `/DProviders/${id}`);
  return response;
};

async function removeAll () {
  const response = await  axios.delete(process.env.REACT_APP_WEB_HOST_API_URL + `/DProviders`);
  return response;
};

async function findByTitle (title) {
  const response = await  axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DProviders?title=${title}`);
  return response.data;
};

async function ImportCSV() {
  const response = await  axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/DProviders/ImportCSV");
  return response;
};

async function ExportCSV(){
  const response = await  axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/DProviders/ExportCSV");
  return response;
};

export default {
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  findByTitle,
  ImportCSV,
  ExportCSV
};