import axios from 'axios'

async function  getPlcDB() {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + '/PlcDB');
  return response.data;
};

async function getPlcSetDB() {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + "/PlcDB/getSettings");
  return response.data;
};

async function getPlcImDb() {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/PlcDB/ims`);
  return response.data;
};

async function setPlcDB(data) {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/PlcDB/SetPlcDB", data);
  return response;
};
async function setMainSettings (data) {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/PlcDB/setMainSettings", data);
  return response;
};
async function ChangePackParam  (SetNumLayer, SetNumPlankInLayer) {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + `/PlcDB/ChangePackParam?SetNumLayer=${SetNumLayer}&SetNumPlankInLayer=${SetNumPlankInLayer}`);
  return response;
};
async function ChangePackParamBrak  (SetNumPlankInLayerBrak) {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + `/PlcDB/ChangePackParamBrak?SetNumPlankInLayerBrak=${SetNumPlankInLayerBrak}`);
  return response;
};


async function startNewPack ()  {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/PlcDB/startNewPack");
  return response;
};
async function finishPack ()  {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/PlcDB/finishPack");
  return response;
};
async function PackReadyRidOff ()  {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/PlcDB/PackReadyToRidOff");
  return response;
};
async function GasketsRidedOff ()  {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/PlcDB/GasketsRidedOff");
  return response;
};
async function finishDefectPack () {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/PlcDB/finishDefectPack");
  return response;
};
async function defectPackPushed () {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/PlcDB/DefectPackPushed");
  return response;
};
async function GasketsBrakPut () {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/PlcDB/GasketsBrakPut");
  return response;
};
async function ReadSettingsFromPlc () {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/PlcDB/ReadSettingsFromPlc");
  return response;
};
async function GetActualAlarms () {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/PlcDB/GetActualAlarms`);
  return response.data;
};
async function SetSpeed (speed) {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + `/PlcDB/ApplySpeed?speed=${speed}`);
  return response;
};
async function GetPackSrez () {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + `/PlcDB/GetPackSrez`);
  return response;
};
async function AddGasketsLayer () {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + `/PlcDB/AddGasketsLayer`);
  return response;
};
async function SmenaNumCorrect (SmenaNum) {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + `/PlcDB/SmenaNumCorrect?smenaNum=${SmenaNum}`);
  return response;
};
async function PackNumCorrect (PackNum) {
  const response = await axios.post(process.env.REACT_APP_WEB_HOST_API_URL + `/PlcDB/PackNumCorrect?PackNum=${PackNum}`);
  return response;
};

export default {
  getPlcDB,
  getPlcSetDB,
  getPlcImDb,
  setPlcDB,
  startNewPack,
  finishPack,
  finishDefectPack,
  defectPackPushed,
  setMainSettings,
  ChangePackParam,
  ReadSettingsFromPlc,
  GetActualAlarms,
  SetSpeed,
  PackReadyRidOff,
  GetPackSrez,
  GasketsRidedOff,
  ChangePackParamBrak,
  AddGasketsLayer,
  SmenaNumCorrect,
  PackNumCorrect,
  GasketsBrakPut
};