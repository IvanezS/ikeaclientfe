import axios from 'axios'

async function getAll() {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + '/DItems');
  return response.data;
};

async function get(id) {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DItems/${id}`);
  return response.data;
};

async function getItemsByTW(T,W) {
  const response = await axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DItems/ItemsTW?T=${T}&W=${W}`);
  return response.data;
};

async function create (data) {
  const response = await  axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/DItems", data);
  return response;
};

async function getItemByData (data) {
  const response = await  axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/DItems/GetItemByParams", data);
  return response;
};

async function ImportCSV() {
  const response = await  axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/DItems/ImportCSV");
  return response;
};

async function ExportCSV(){
  const response = await  axios.post(process.env.REACT_APP_WEB_HOST_API_URL + "/DItems/ExportCSV");
  return response;
};

async function update (id, data) {
  const response = await  axios.put(process.env.REACT_APP_WEB_HOST_API_URL + `/DItems/${id}`, data);
  return response;
};

async function remove (id) {
  const response = await  axios.delete(process.env.REACT_APP_WEB_HOST_API_URL + `/DItems/${id}`);
  return response;
};

async function removeAll () {
  const response = await  axios.delete(process.env.REACT_APP_WEB_HOST_API_URL + `/DItems`);
  return response;
};

async function findByTitle (title) {
  const response = await  axios.get(process.env.REACT_APP_WEB_HOST_API_URL + `/DItems?title=${title}`);
  return response.data;
};

export default {
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  findByTitle,
  getItemByData,
  ImportCSV,
  ExportCSV,
  getItemsByTW
};