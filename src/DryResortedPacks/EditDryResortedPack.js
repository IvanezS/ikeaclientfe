import React, { useEffect, useState } from 'react';
import { Form, Button, Container, Jumbotron, Modal } from 'react-bootstrap';
import PackDataService from "../Services/DryPackService";
import ItemDataService from "../Services/ItemService";
import { useHistory, useParams } from "react-router-dom";


function EditDryResortedPack() {


    const [modalShow, setModalShow] = useState(false);
    const [validated, setValidated] = useState(false);
    const [item, setItem] = useState(null);
    const [pack, setPack] = useState({});

    let history = useHistory();
    let param = useParams();

    useEffect(() => {
        async function getPack() {
            var response = await PackDataService.get(param.id) 
            setPack(response);
            console.log("pack",response)
        }
    
        async function getItems() {
            var response = await ItemDataService.getAll()
            setItem(response);
        }
        
        getItems();
        getPack();
    }, [])




    const handleChange = e => {
        const { name, value } = e.target;
        setPack(prevState => ({ ...prevState, [name]: value }));
    }

    async function UpdatePack() {
        debugger
        var response = await PackDataService.update(
            Number(param.id),
            {
                packetNumber: pack.packetNumber,
                totalPlanks: Number(pack.totalPlanks),
                totalVolume: Number(pack.totalVolume),
                itemId: Number(pack.itemId)
            }
        )

        if (response.status === 200) {
            alert('Data saved successfully');
        }
        else {
            alert('Data not saved');
        }
        return response

    }

    const handleSubmit = async (event) => {

        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);

        //if (form.checkValidity() === true) {
            if (pack.itemId !== undefined 
                && pack.itemId !== 0 
                && pack.packetNumber.length > 0 
                && pack.totalPlanks > 0 
                && pack.totalVolume >0 
                && pack.packetNumber !== undefined 
                && pack.totalVolume !== undefined 
                && pack.totalPlanks !== undefined) {
            var response = await UpdatePack();
            if (response.status === 200) {
                redir();
            } else {
                setModalShow(true)
            }
        }

    };

    function redir() {
        history.push("/");
        history.replace("/ResortedDryPacksList/" );
    }

    function ErrorModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Не получилось создать пакет</h4>
                    <p>
                        Проверьте задаваемые параметры, убедитесь в наличии связи
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>Ok</Button>
                </Modal.Footer>
            </Modal>
        );
    }

        return (
            <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Редактирование сухого пересортированного пакета </h1>
            </Jumbotron>

            <ErrorModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
            <Form noValidate validated={validated} onSubmit={handleSubmit}>

                <Form.Group controlId="validationItemId">
                    <Form.Label >ITEM-коды</Form.Label>
                    <Form.Control

                        as="select"
                        name="itemId"
                        placeholder="Выберите Item"
                        onChange={handleChange}
                        size="md"
                        title="ITEM-код для пакета"
                        value={pack.itemId}
                        isInvalid={pack.itemId === undefined || pack.itemId === 0}
                        isValid={pack.itemId !== undefined &&  pack.itemId !== 0}
                    >

                        {
                            item && item.map(res => {

                                return (
                                    <option key={res.id} value={res.id}>
                                        ITEM-код: {res.itemName}, T: {res.thick} мм, W: {res.width} мм, Смешанная L: {res.mixedLength}, L {res.length} мм, Качество {res.quality}, Порода {res.breed}, Сторона: {res.side}, Влажность: {res.moisture}
                                    </option>
                                )
                            })}

                    </Form.Control>
                    <Form.Control.Feedback type="invalid">
                        Пожалуйста, выберите ITEM-код для пакета
                    </Form.Control.Feedback>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Номер пакета</Form.Label>
                    <Form.Control required type="text" name="packetNumber" placeholder="Введите номер пакета" value={pack && pack.packetNumber} onChange={handleChange} />
                    <Form.Control.Feedback type="invalid">
                        Пожалуйста, введите номер пакета
                    </Form.Control.Feedback>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Кол-во досок в пакете, шт</Form.Label>
                    <Form.Control required type="number" name="totalPlanks"  placeholder="Кол-во досок в пакете" value={pack.totalPlanks} onChange={handleChange} />
                    <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">
                        Пожалуйста, введите кол-во досок в пакете
                    </Form.Control.Feedback>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Объём пакета, м3</Form.Label>
                    <Form.Control required type="text" name="totalVolume"  placeholder="Объём пакета" value={pack.totalVolume} onChange={handleChange} />
                    <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">
                        Пожалуйста, введите объём пакета
                    </Form.Control.Feedback>
                </Form.Group>


                <Button type="button" className="btn btn-primary btn-block" onClick={handleSubmit}>Обновить пакет</Button>
                <Button type="button" className="btn btn-secondary btn-block" onClick={() => history.goBack()}>Назад</Button>
            </Form>

        </Container>
        )
    

}



export default EditDryResortedPack