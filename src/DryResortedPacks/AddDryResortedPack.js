import React, { useEffect, useState } from 'react';
import { Form, Button, Container, Jumbotron, Modal } from 'react-bootstrap';
import PackDataService from "../Services/DryPackService";
import ItemDataService from "../Services/ItemService";
import PartDataService from "../Services/PartService";
import { useHistory, useParams } from "react-router-dom";


function AddDryResortedPack() {

    const [modalShow, setModalShow] = useState(false);
    const [validated, setValidated] = useState(false);
    const [item, setItem] = useState(null);
    const [pack, setPack] = useState({});
    const [, setPart] = useState({})
    let history = useHistory();
    let param = useParams();

    useEffect(() => {
        async function getItems() {
            var response = await ItemDataService.getAll()
            setItem(response)
            console.log(response, 'item');
        }
        getItems();
    }, [])


     const handleChange = e => {
        const { name, value } = e.target;
        setPack(prevState => ({ ...prevState, [name]: value }));
    }

    async function AddPack() {

        var response = await PackDataService.create(
            {
                packetNumber: "0",
                totalPlanks: Number(pack.totalPlanks),
                totalVolume: Number(pack.totalVolume),
                itemId: Number(pack.itemId)
            }
        )

        if (response.status === 200) {
            alert('Data saved successfully');
        }
        else {
            alert('Data not saved');
        }
        return response

    }

    const handleSubmit = async (event) => {
debugger
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);

        //if (form.checkValidity() === true) {
            if (pack.itemId !== undefined &&  pack.itemId !== "0" 
            && pack.totalPlanks.length >0 && pack.totalPlanks !== undefined 
            && pack.totalVolume.length >0 && pack.totalVolume !== undefined) {
            var response = await AddPack();
            if (response.status === 200) {
                redir();
            } else {
                setModalShow(true)
            }
        }

    };

    function redir() {
        history.push("/");
        history.replace("/ResortedDryPacksList/");
    }

    function ErrorModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Не получилось создать пакет</h4>
                    <p>
                        Проверьте задаваемые параметры, убедитесь в наличии связи
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>Ok</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Введите данные нового пакета </h1>
            </Jumbotron>

            <ErrorModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
            <Form noValidate validated={validated} onSubmit={handleSubmit}>

                <Form.Group controlId="validationItemId">
                    <Form.Label >ITEM-коды (убедитесь, что для партии задано сечение досок)</Form.Label>
                    <Form.Control

                        as="select"
                        name="itemId"
                        placeholder="Выберите Item"
                        onChange={handleChange}
                        size="md"
                        title="ITEM-код для пакета"
                        value={pack.itemId}
                        isInvalid={pack.itemId === undefined || pack.itemId === "0"}
                        isValid={pack.itemId !== undefined &&  pack.itemId !== "0"}
                    >
                        <option value={0}>Выберите ITEM-код для пакета</option>
                        {
                            item && item.map(res => {

                                return (
                                    <option key={res.id} value={res.id}>
                                        ITEM-код: {res.itemName}, T: {res.thick} мм, W: {res.width} мм, Смешанная L: {res.mixedLength}, L {res.length} мм, Качество {res.quality}, Порода {res.breed}, Сторона: {res.side}, Влажность: {res.moisture}
                                    </option>
                                )
                            })}

                    </Form.Control>
                    <Form.Control.Feedback type="invalid">
                        Пожалуйста, выберите ITEM-код для пакета
                    </Form.Control.Feedback>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Кол-во досок в пакете, шт</Form.Label>
                    <Form.Control required type="number" name="totalPlanks"  placeholder="Кол-во досок в пакете" value={pack.totalPlanks} onChange={handleChange} />
                    <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">
                        Пожалуйста, введите кол-во досок в пакете
                    </Form.Control.Feedback>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Объём пакета, м3</Form.Label>
                    <Form.Control required type="text" name="totalVolume" placeholder="Объём пакета" value={pack.totalVolume} onChange={handleChange} />
                    <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">
                        Пожалуйста, введите объём пакета
                    </Form.Control.Feedback>
                </Form.Group>

                <Button type="button" className="btn btn-primary btn-block" onClick={handleSubmit}>Создать пакет</Button>
                <Button type="button" className="btn btn-secondary btn-block" onClick={() => history.goBack()}>Назад</Button>

            </Form>

        </Container>
    )
}

export default AddDryResortedPack