import React, { useState, useEffect } from 'react';
import { Form, Container, Jumbotron, Button, Modal } from 'react-bootstrap';
import PackDataService from "../Services/DryPackService";
import { useHistory } from "react-router-dom";
import { PencilFill, Printer, Trash } from 'react-bootstrap-icons';
import { useSelector } from 'react-redux'
import { Fragment } from 'react';
import axios from 'axios'

function ResortedDryPacksList() {
    const user = useSelector(state => state.auth.user)
    let history = useHistory();
    const [packs, setPacks] = useState(null)
    const [activePackId, setActivePackId] = useState(null);
    const [modalShow, setModalShow] = useState(false);
    const [modalErrShow, setModalErrShow] = useState(false);

    useEffect(() => {

        axios.all([
            PackDataService.getAll()
          ])
          .then(axios.spread((obj1) => {
            console.log("Pack data", obj1);
            setPacks(obj1);
          }));

    }, [])

    async function DeletePack() {

        var response = await PackDataService.remove(activePackId)
        if (response.status === 200) {
            history.push("/")
            history.replace("/ResortedDryPacksList/")
        }else{
            setModalErrShow(true)
        }
    }

    function setActivePack(id) {
        setActivePackId(id);
        setModalShow(true)
    }

    async function SetPackScaned(id) {
        var response = await PackDataService.markScan(id)
        if (response.status === 200) {
            alert('Пакет успешно помечен');
            window.location.reload();
        }
    }

    async function PrintPack() {
        var response = await PackDataService.PrintRpackPDF(packs.id)
        if (response.status === 200) {
            alert('Пакет успешно распечатан');
        }
    }

    function setStatePfunc(state) {
        if (state === 0) return ("")
        if (state === 1) return ("table-success")
    }

    function gotoResort(id) {
        history.push("/ResortDryPacks/")
    }

    function DeletedModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Вы собираетесь удалить пакет</h4>
                    <p>
                        Нажмите кнопку "Удалить" для подтверждения или кнопку "Отмена"
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={DeletePack}>Удалить</Button>
                    <Button onClick={props.onHide}>Отмена</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function ErrorModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ОШИБКА!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>Что-то пошло не так. Проверьте связь, обновите страницу</p>

                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>OK</Button>
                </Modal.Footer>
            </Modal>
        );
    }



    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Список сухих пересортированных пакетов</h1>
            </Jumbotron>
            
            <DeletedModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

            <ErrorModal
                show={modalErrShow}
                onHide={() => setModalErrShow(false)}
            />


                {user && (user.profile.role === "Admin" || user.profile.role === "Oper") ?
                <Fragment>
                    <hr />
                    <Button type="button" onClick={() => history.push("/AddDryResortedPack/")} className="btn btn-primary" >Добавить новый пакет</Button>
                    <Button type="button" onClick={() => history.push("/ResortedDryPacksList/")} className="btn btn-warning" >Пересортировка</Button>
                    <hr />
                </Fragment>
                :''}
            


            <Form className="form">
                {packs ?
                    <table className="table">
                        <thead>
                            <tr>
                                <th>№ пакета</th>
                                <th>№ смены</th>
                                <th>Имя смены</th>
                                <th>Тип пакета</th>
                                <th>Кол-во</th>
                                <th>Объём</th>
                                <th>Дата</th>
                                <th></th>

                            </tr>
                        </thead>
                        <tbody>
                            {packs && packs.map(function (el, index) {
                                return (

                                    <tr key={index}>
                                        <td>{el.packetNumber}</td>
                                        <td>{el.smenaNum}</td>
                                        <td>{el.smenaName}</td>
                                        <td>{el.isDefected ? "Брак" : "Норма"}</td>
                                        <td>{el.sortedPlanks}/{el.totalPlanks}</td>
                                        <td>{el.totalVolume}</td>
                                        <td>{el.packDate}</td>
                                        <td>
                                            <Button type="button" onClick={() => history.push("/EditDryResortedPack/" + el.id)} className="btn btn-success" title="Редактировать пакет"><PencilFill /></Button>
                                            <Button type="button" onClick={() => setActivePack(el.id)} className="btn btn-danger" title="Удалить пакет"><Trash /></Button>
                                            <Button type="button" onClick={() => PrintPack(el.id)} className="btn btn-info" title="Распечатать этикетку"><Printer /></Button>
                                            {/* <Button type="button" onClick={() => SetPackScaned(el.id)} className="btn btn-secondary" title="Сделать пакет отсканированным"><UpcScan /></Button> */}
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                    : "Нет пакетов"
                }
            </Form>

        </Container>
    )

}

export default ResortedDryPacksList