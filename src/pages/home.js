import React from 'react'
import { signoutRedirect } from '../Services/UserService'
import { useSelector } from 'react-redux'
import { Button, Container, Jumbotron } from 'reactstrap';
//import { prettifyJson } from '../utils/jsonUtils'
function Home() {
  
  const user = useSelector(state => state.auth.user)

  function signOut() {
    signoutRedirect()
  }

  
  return (

            <Container fluid className="App">
    
            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4 ">ЛИНИЯ ПЕРЕШТАБЕЛИРОВАНИЯ</h1>
            </Jumbotron>
              <h3>Здравствуйте, {user.profile.name}!</h3>
              <p>Вы можете пользоваться доступными вам средствами сайта для управления линией перештабелирования </p>
              <p>Нажмите на кнопку "Выйти" по окончании работы</p>
  
              <Button onClick={() => signOut()}>Выйти</Button>

                {/* <p>{prettifyJson(user)}</p> */}

            </Container>

  )
}

export default Home
