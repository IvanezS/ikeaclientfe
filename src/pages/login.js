import React from 'react'
import { signinRedirect } from '../Services/UserService'
import { Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { Button, Container, Jumbotron } from 'reactstrap';
function Login() {
  const user = useSelector(state => state.auth.user)

  function login() {
    signinRedirect()
  }

  return (

    (user) ?
      (<Redirect to={'/'} />)
      :
      (
        <Container fluid className="App">
    
          <Jumbotron fluid className="vertical-center justify-content-center">
              <h1 className="display-4">ЛИНИЯ ПЕРЕШТАБЕЛИРОВАНИЯ</h1>
          </Jumbotron>
            <h3>Здравствуйте!</h3>
            <p>Перед началом работы необходимо пройти аутентификацию пользователя</p>
            <p>Нажмите на кнопку "Войти" для продолжения</p>

            <Button onClick={() => login()}>Войти</Button>

          </Container>
      )
      
  )
}

export default Login
