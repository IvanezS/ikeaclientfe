import React from 'react';
import { Accordion, Card, Button, ListGroup, Row, Col, Container, Table, Form } from 'react-bootstrap'
import { AlarmFill, Book, PeopleFill, List, ListCheck, ArrowRight, Gear } from 'react-bootstrap-icons';
import PlcDataService from "../Services/PlcDBService";
import SmenaService from "../Services/SmenaService";
import LogsService from "../Services/LogsService";
import PartService from "../Services/PartService";
import { Fragment } from 'react';
import { Link } from 'react-router-dom' 

class SecondPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            alarms: [{
                on: false,
                timeOn: "",
                message: ""
            }],
            logs: [{
                message: "",
                logType: "",
                user: "",
                oN_OFF_text: "",
                logTime: ""
            }],
            settings: {},
            MainSettingsDto: {
                smenaName: "",
                operatorSurname: ""
            },
            ActualPartId : 0,
            ActualMainPartId : 0,
            CurrentPart: "БЕЗ ПАРТИИ",
            smenaNumAct: 0,
            rpackNumberAct: 0
        }
        this.startSmena=this.startSmena.bind(this);
        this.finishSmena=this.finishSmena.bind(this);
        this.smenaCorrect=this.smenaCorrect.bind(this);
        this.packCorrect=this.packCorrect.bind(this);

    }

    componentDidMount() {
        this.interval = setInterval(() => { this.getAlarms(); this.getLogs(); this.UpdateNavButtons() }, 1000);
        this.settings()
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    handleChange(e) {
        debugger
        const { name, value } = e.target;
        this.setState(prevState => ({ MainSettingsDto: { ...prevState.MainSettingsDto, [name]: value} }))

    }

    handleSett(e) {
        debugger
        const { name, value } = e.target;
        this.setState(prevState => ({ ...prevState, [name]: value} ))

    }

 

    async startSmena() {
        debugger
        var response = await SmenaService.create({
            smenaName : this.state.MainSettingsDto.smenaName,
            operatorSurname : this.state.MainSettingsDto.operatorSurname
        })
        if (response.status !== 200) {
            alert("Не удалось начать смену")
        }else{
            this.settings()
        }
    }
    async finishSmena() {
        var response = await SmenaService.finish()
        if (response.status !== 200) {
            alert("Не удалось закончить смену")
        }else{
            this.settings()
        }
    }
    async smenaCorrect() {
        debugger
        var response = await PlcDataService.SmenaNumCorrect(Number(this.state.smenaNumAct))
        if (response.status !== 200) {
            alert("Ну удалось откорректировать номер смены")
        }else{
            this.settings()
        }
    }
    async packCorrect() {
        debugger
        var response = await PlcDataService.PackNumCorrect(Number(this.state.rpackNumberAct))
        if (response.status !== 200) {
            alert("Ну удалось откорректировать номер смены")
        }else{
            this.settings()
        }
    }
    async getAlarms() {
        const alrms = await PlcDataService.GetActualAlarms()
        this.setState({ alarms: alrms })
    }
    async getLogs() {
        const logg = await LogsService.getAll()
        this.setState({ logs: logg })
    }
    async getPart(id) {
        return await PartService.get(id)
    }

    async UpdateNavButtons() {

        const setti = await PlcDataService.getPlcSetDB()
        this.setState(prevState => ({ settings: 
            { ...prevState.settings, 
                counter: setti.counter
            } 
        }))
        if (setti.partId > 0){
            //console.log("Settings", setti)
            const part = await this.getPart(setti.partId)
            this.setState(prevState => ({ ...prevState, ActualPartId : part.id, ActualMainPartId : part.mainPartId, CurrentPart:  part.partNumber}))
        } else {
            this.setState(prevState => ({ ...prevState, ActualPartId : 0, ActualMainPartId : 0, CurrentPart:  "БЕЗ ПАРТИИ"}))
        }
    }

    async settings() {
        debugger
        const sett = await PlcDataService.getPlcSetDB()
        this.setState({ settings: sett })
        this.setState(prevState => ({ MainSettingsDto: 
            { ...prevState.MainSettingsDto, 
                smenaName: sett.smenaName === "" ? "Смена 1" : sett.smenaName, 
                operatorSurname: sett.operatorSurname
            } 
        }))
    }
    render() {
        return (
            <Accordion>
                <Card>
                    <Card.Header>
                        <Row>
                            <Col sm="2">
                                <Accordion.Toggle as={Button} variant="link" eventKey="0" title="Список актуальных аварий">
                                    <AlarmFill />
                                </Accordion.Toggle>
                                <Accordion.Toggle as={Button} variant="link" eventKey="1" title="Журнал событий">
                                    <Book />
                                </Accordion.Toggle>
                                <Accordion.Toggle as={Button} variant="link" eventKey="2" title="Смена">
                                    <PeopleFill />
                                </Accordion.Toggle>
                                <Accordion.Toggle as={Button} variant="link" eventKey="3" title="Настройки">
                                    <Gear />
                                </Accordion.Toggle>
                                {this.state.settings.counter}
                            </Col>
                            <Col sm="7">
                                <ListGroup>
                                    {this.state.alarms[0] ? <ListGroup.Item style={{ backgroundColor: '#FFDDDD' }} action title="Последняя авария">{this.state.alarms[0].message}</ListGroup.Item> : <ListGroup.Item action title="Последняя авария">Нет аварий</ListGroup.Item>}
                                </ListGroup>
                            </Col>
                            <Col sm="2">
                                {this.state.ActualMainPartId > 0 && this.state.ActualPartId > 0 ?
                                <Fragment>
                                    <Link to={'/AddItemPart/'+ this.state.ActualMainPartId}><Button type="button" className="btn btn-dark" title="Список ITEM-кодов текущей партии"><List /> </Button></Link>
                                    <Link to={'/AddPack/'+ this.state.ActualPartId}><Button type="button" className="btn btn-secondary" title="Список исходных пакетов текущей подпартии"><List /></Button></Link>
                                    <Link to={'/AddResortedPack/'+ this.state.ActualPartId}><Button type="button"  className="btn btn-primary" title="Список пересортированных пакетов текущей подпартии"><ListCheck /></Button></Link>
                                    <Link to={'/ResortPart/'+ this.state.ActualPartId}><Button type="button" className="btn btn-warning" title="Пересортировка пакетов текущей подпартии"><ArrowRight /></Button> </Link>
                                </Fragment>
                                : ""}
                            </Col>
                            
                            <Col sm="1">
                                   Текущая подпартия:<br/><b>{this.state.CurrentPart}</b>
                            </Col>

                        </Row>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body style={{ backgroundColor: '#FFEEEE' }}>
                            <Container fluid style={{ height: "300px" }} className="overflow-auto">
                                <h3>Список актуальных аварий</h3>
                                {this.state.alarms[0] ?
                                    <Table striped bordered hover size="sm">
                                        <thead>
                                            <tr>
                                                {/* <th>Состояние</th> */}
                                                <th>Наименование аварии</th>
                                                <th>Время</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.alarms && this.state.alarms.map(function (el, index) {
                                                return <tr key={index}>
                                                    <td>{el.message}</td>
                                                    <td>{el.timeON}</td>
                                                </tr>
                                            })}
                                        </tbody>
                                    </Table>
                                    :
                                    "Нет аварий"}

                            </Container>


                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
                <Card>
                    <Accordion.Collapse eventKey="1">
                        <Card.Body style={{ backgroundColor: '#FFFFEE' }}>
                            <Container fluid style={{ height: "300px" }} className="overflow-auto">
                                <h3>Журнал событий</h3>
                                {this.state.logs[0] ?
                                    <Table striped bordered hover size="sm">
                                        <thead>
                                            <tr>
                                                <th>Состояние</th>
                                                <th>Тип события</th>
                                                <th>Событие</th>
                                                <th>Пользователь</th>
                                                <th>Время</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.logs && this.state.logs.map(function (el, index) {
                                                return <tr key={index}>
                                                    <td>{el.oN_OFF_text}</td>
                                                    <td>{el.logType}</td>
                                                    <td>{el.message}</td>
                                                    <td>{el.user}</td>
                                                    <td>{el.logTime}</td>
                                                </tr>
                                            })}
                                        </tbody>
                                    </Table>
                                    :
                                    "Нет событий"}
                            </Container>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
                <Card>
                    <Accordion.Collapse eventKey="2">
                        <Card.Body style={{ backgroundColor: '#EEEEFF' }}>
                            <Container fluid style={{ height: "300px" }} className="overflow-auto">
                                <h3>Смена</h3>
                                <Form >

                                <Form.Row>
                                    <Form.Group as={Col}>
                                        <Form.Label for="operatorSurname">Фамилия оператора</Form.Label>
                                        <Form.Control type="text" name="operatorSurname" value={this.state.MainSettingsDto.operatorSurname} onChange={this.handleChange.bind(this)} disabled = {this.state.settings.smenaActive}/>
                                    </Form.Group>


                                    <Form.Group as={Col}>
                                        <Form.Label for="smenaName">Название смены</Form.Label>
                                        <Form.Control
                                            required
                                            as="select"
                                            name="smenaName"
                                            value={this.state.MainSettingsDto.smenaName}
                                            placeholder="Выберите текущую смему"
                                            onChange={this.handleChange.bind(this)}
                                            size="md"
                                            title="Выбор смены"
                                            disabled = {this.state.settings.smenaActive}
                                        >
                                            
                                            <option value="Смена 1">Смена 1</option>
                                            <option value="Смена 2">Смена 2</option>

                                                
                                            
                                        </Form.Control>
                                    </Form.Group>

                                </Form.Row>

                                <Form.Row>
                                    <Form.Group as={Col}>
                                        <Form.Label for="smenaNum">Текущий номер смены</Form.Label>
                                        <Form.Control type="number" name="smenaNum" value={this.state.settings.smenaNum} disabled/>
                                    </Form.Group>

                                    <Form.Group as={Col}>
                                        <Form.Label for="smenaNumAct">Откорректировать номер смены</Form.Label>
                                        <Form.Control type="number" name="smenaNumAct" value={this.state.smenaNumAct} onChange={this.handleSett.bind(this)}/>
                                    </Form.Group>

                                    <Form.Group as={Col}>
                                    <Form.Label > </Form.Label>
                                    <Button type="button" className="btn btn-primary btn-block" onClick={(this.smenaCorrect)}>Откорректировать</Button>
                                    </Form.Group>

                                </Form.Row>

                                    <Button type="button" className="btn btn-primary btn-block" onClick={this.startSmena} disabled = {this.state.settings.smenaActive}>Начать смену</Button>
                                    <Button type="button" className="btn btn-secondary btn-block" onClick={this.finishSmena} disabled = {!this.state.settings.smenaActive}>Закончить смену</Button>

                                </Form>
                            </Container>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
                <Card>
                    <Accordion.Collapse eventKey="3">
                        <Card.Body style={{ backgroundColor: '#EEFFEE' }}>
                            <Container fluid style={{ height: "300px" }} className="overflow-auto">
                                <h3>Настройки</h3>
                                <Form>
                                <Form.Row>
                                    <Form.Group as={Col}>
                                        <Form.Label for="rpackNumber">Текущий номер пакета</Form.Label>
                                        <Form.Control type="number" name="smenaNum" value={this.state.settings.rpackNumber} disabled/>
                                    </Form.Group>

                                    <Form.Group as={Col}>
                                        <Form.Label for="rpackNumberAct">Откорректировать номер пакета</Form.Label>
                                        <Form.Control type="number" name="rpackNumberAct" value={this.state.rpackNumberAct} onChange={this.handleSett.bind(this)}/>
                                    </Form.Group>

                                    <Form.Group as={Col}>
                                    <Form.Label > </Form.Label>
                                    <Button type="button" className="btn btn-primary btn-block" onClick={(this.packCorrect)} >Откорректировать</Button>
                                    </Form.Group>

                                </Form.Row>
                                </Form>
                            </Container>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        );
    }
}

export default SecondPanel