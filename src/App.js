import React, { useEffect } from 'react'
import './App.css';

import { Provider } from 'react-redux';
import store from './store';
import userManager, { loadUserFromStorage } from './Services/UserService'
import AuthProvider from './utils/authProvider'
import Navigation from './utils/Navigation'

function App() {
  useEffect(() => {
    // fetch current user from cookies
    loadUserFromStorage(store).then(response => {console.log('resp = ',response)})
  }, [])
  
  return (
    <Provider store={store}>
      <AuthProvider userManager={userManager} store={store}>
        <Navigation/>
      </AuthProvider>
    </Provider>
  );
}

export default App;
