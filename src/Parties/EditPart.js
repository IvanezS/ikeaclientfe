import React, { useState, useEffect } from 'react';
import { Form, Container, Jumbotron, Button, Col, Modal, InputGroup } from 'react-bootstrap';
import MainPartDataService from "../Services/MainPartService";
import ProviderDataService from "../Services/ProviderService";
import { useHistory, useParams } from "react-router-dom";

function EditPart() {

    const [modalShow, setModalShow] = useState(false);
    const [validated, setValidated] = useState(false);
    const [providers, setProviders] = useState(null);
    const [part, setPart] = useState(
        {
            providerId: 0,
            comments: "",
            mainPartNumber: "",
            state: 0,
            packets:[{}]
        }
    );
    const param = useParams();
    let history = useHistory();

    useEffect(
        getPart
    , [])
    useEffect(
        getProviders
    , [])

    async function getPart() {
        var response = await MainPartDataService.get(param.id)
        setPart(response)
        console.log("part=", response)
    }

    async function getProviders() {
        const providers = await ProviderDataService.getAll()
        setProviders(providers)
    }

    function redir() {
        history.push("/");
        history.replace("/MainPartList");
    }

    const handleChange = e => {
        const { name, value } = e.target;
        if (name === "mainPartNumber") {
            let c = value.match('[0-3][0-9]-[0-1][0-9]-([0-9]+)?$')
            if (c == null) return;
        }
        setPart(prevState => ({ ...prevState, [name]: value }));
    }

    async function UpdatePart() {
        return await MainPartDataService.update(
            param.id,
            {
                mainPartNumber: part.mainPartNumber,
                providerId: Number(part.providerId),
                comment: part.comments

            })

    }


    const handleSubmit = async (event) => {

        debugger
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);

        //if (form.checkValidity() === true) {
        if (part && part.mainPartNumber.match('[0-3][0-9]-[0-1][0-9]-([0-9]+)$')
            ) {
            var response = await UpdatePart();
            if (response.status === 200) {
                redir();
            } else {
                setModalShow(true)
            }
        }

    };
    
    function ErrorModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Не получилось создать поставщика</h4>
                    <p>
                        Проверьте задаваемые параметры, убедитесь в наличии связи
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>Ok</Button>
                </Modal.Footer>
            </Modal>
        );
    }



    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Редактирование партии</h1>
            </Jumbotron>

            <ErrorModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

            <Form noValidate validated={validated} onSubmit={handleSubmit}>

                <Form.Row>

                    <Form.Group as={Col} controlId="validationPartNumber">
                        <Form.Label>Номер партии</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            name="mainPartNumber"
                            placeholder="Введите номер партии"
                            onChange={handleChange}
                            value={part && part.mainPartNumber}
                            isInvalid={part && part.mainPartNumber===undefined && part.mainPartNumber.match('[0-3][0-9]-[0-1][0-9]-([0-9]+)$') == null }
                        />
                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                            Пожалуйста, заполните поле с номером партии
                        </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group as={Col}  controlId="validationProviderId">
                        <Form.Label>Поставщик</Form.Label>
                        <Form.Control
                            required
                            custom
                            as="select"
                            name="providerId"
                            value={part.providerId}
                            placeholder="Выберите поставщика"
                            onChange={handleChange}
                            size="md"
                            isInvalid={part === "undefined" || part.providerId === 0}
                            //defaultValue={0}
                        >
                            <option key={0} value={0}>...</option>
                            {
                                providers && providers.map(res => {
                                    return (
                                        <option key={res.id} value={res.id}>
                                            {res.name}
                                        </option>
                                    )
                                })
                            }
                        </Form.Control>


                        <Form.Control.Feedback type="invalid">
                            Выберите поставщика
                        </Form.Control.Feedback>

                    </Form.Group>
                    </Form.Row>
                    <Form.Row>

                    <Form.Group as={Col}  controlId="validationProviderId">
                    <Form.Label>Комментарий</Form.Label>
                    <InputGroup className="mb-3">
                    
                    <Form.Control
                            type="text"
                            name="comments"
                            placeholder="Введите комментарий"
                            onChange={handleChange}
                            value={part.comments}
                        />
                        <InputGroup.Append>
                        <Button variant="outline-secondary" onClick = {()=> setPart(prevState => ({ ...prevState, comments: ""}))}>Удалить</Button>
                        </InputGroup.Append>
                    </InputGroup>
                </Form.Group>

                    </Form.Row>

                    <Button variant="secondary" size="sm" onClick = {()=> setPart(prevState => ({ ...prevState, comments: part.comments === undefined ? "Сучки; " : part.comments + "Сучки; " }))}>Сучки</Button>
                    <Button variant="secondary" size="sm" onClick = {()=> setPart(prevState => ({ ...prevState, comments: part.comments === undefined ? "Трещины; " : part.comments + "Трещины; " }))}>Трещины</Button>
                    <Button variant="secondary" size="sm" onClick = {()=> setPart(prevState => ({ ...prevState, comments: part.comments === undefined ? "Синева; " : part.comments + "Синева; " }))}>Синева</Button>
                    <Button variant="secondary" size="sm" onClick = {()=> setPart(prevState => ({ ...prevState, comments: part.comments === undefined ? "Обзол; " : part.comments + "Обзол; " }))}>Обзол</Button>
                    <Button variant="secondary" size="sm" onClick = {()=> setPart(prevState => ({ ...prevState, comments: part.comments === undefined ? "Размер; " : part.comments + "Размер; " }))}>Размер</Button>
                    <Button variant="secondary" size="sm" onClick = {()=> setPart(prevState => ({ ...prevState, comments: part.comments === undefined ? "Грязь; " : part.comments + "Грязь; " }))}>Грязь</Button>
                    <Button variant="secondary" size="sm" onClick = {()=> setPart(prevState => ({ ...prevState, comments: part.comments === undefined ? "Инородные включения; " : part.comments + "Инородные включения; " }))}>Инородные включения</Button>
                    <Button variant="secondary" size="sm" onClick = {()=> setPart(prevState => ({ ...prevState, comments: part.comments === undefined ? "Гниль; " : part.comments + "Гниль; " }))}>Гниль</Button>
                    <Button variant="secondary" size="sm" onClick = {()=> setPart(prevState => ({ ...prevState, comments: part.comments === undefined ? "Мех. повреждения; " : part.comments + "Мех. повреждения; " }))}>Мех. повреждения</Button>
                    <Button variant="secondary" size="sm" onClick = {()=> setPart(prevState => ({ ...prevState, comments: part.comments === undefined ? "Сердцевина; " : part.comments + "Сердцевина; " }))}>Сердцевина</Button>
                    <Button variant="secondary" size="sm" onClick = {()=> setPart(prevState => ({ ...prevState, comments: part.comments === undefined ? "Червоточина; " : part.comments + "Червоточина; " }))}>Червоточина</Button>
                    <Button variant="secondary" size="sm" onClick = {()=> setPart(prevState => ({ ...prevState, comments: part.comments === undefined ? "Прорость; " : part.comments + "Прорость; " }))}>Прорость</Button>
                    <Button variant="secondary" size="sm" onClick = {()=> setPart(prevState => ({ ...prevState, comments: part.comments === undefined ? "Порода; " : part.comments + "Порода; " }))}>Порода</Button>
                            
<p></p>
                <Button type="button" className="btn btn-primary btn-block" onClick={handleSubmit}>Обновить партию</Button>
                <Button type="button" className="btn btn-secondary btn-block" onClick={() => history.goBack()}>Назад</Button>

            </Form>
        </Container>
    )

}

export default EditPart