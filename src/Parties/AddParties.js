import React, { useEffect, useState } from 'react';
import { Form, Container, Jumbotron, Button, Col, Modal } from 'react-bootstrap';
import MainPartDataService from "../Services/MainPartService";
import ProviderDataService from "../Services/ProviderService";
import { useHistory } from "react-router-dom";


function AddParties() {

    const [modalShow, setModalShow] = useState(false);
    const [validated, setValidated] = useState(false);
    const [providers, setProviders] = useState(null);
    const [parts, setParts] = useState(
        {
            providerId: 0,
            comments: "",
            partNumber: getPrefix()

        }
    );
    let history = useHistory();

    useEffect(() => {
        getProviders();
    }, [])

    function getPrefix() {

        let newDate = new Date()

        return newDate.getDate().toLocaleString('en-US', {
            minimumIntegerDigits: 2,
            useGrouping: false
        }) + "-" + (newDate.getMonth() + 1).toLocaleString('en-US', {
            minimumIntegerDigits: 2,
            useGrouping: false
        }) + "-";
    }

    const handleChange = e => {
        debugger
        const { name, value } = e.target;
        if (name === "partNumber") {
            let c = value.match('[0-3][0-9]-[0-1][0-9]-([0-9]+)?$')
            if (c == null) return;
        }
        setParts(prevState => ({ ...prevState, [name]: value }));
    }

    async function CreatePart() {
        return await MainPartDataService.create(
            {
                state: 0,
                mainPartNumber: parts.partNumber,
                providerId: Number(parts.providerId),
                comment: parts.comments
            }

        );
    }


    function redir() {
        history.push("/");
        history.replace("/MainPartList");
    }

    const handleSubmit = async (event) => {

        debugger
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);

        if (parts.partNumber.match('[0-3][0-9]-[0-1][0-9]-([0-9]+)$')) {
            var response = await CreatePart();
            if (response.status === 200) {
                redir();
            } else {
                setModalShow(true)
            }
        }

    };

    async function getProviders() {
        const providers = await ProviderDataService.getAll()
        setProviders(providers)
        console.log(providers, 'providers');
    }

    function ErrorModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Не получилось создать партию</h4>
                    <p>
                        Проверьте задаваемые параметры, убедитесь в наличии связи
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>Ok</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Добавление партии</h1>
            </Jumbotron>

            <ErrorModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

            <Form noValidate validated={validated} onSubmit={handleSubmit}>

                <Form.Row>

                    <Form.Group as={Col} controlId="validationPartNumber">
                        <Form.Label>Номер партии</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            name="partNumber"
                            placeholder="Введите номер партии"
                            onChange={handleChange}
                            value={parts.partNumber}
                            isInvalid={parts && parts.partNumber.match('[0-3][0-9]-[0-1][0-9]-([0-9]+)$') == null}
                        />
                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                            Пожалуйста, заполните поле с номером партии
                        </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group as={Col} controlId="validationProviderId">
                        <Form.Label>Поставщик</Form.Label>
                        <Form.Control
                            required
                            custom
                            as="select"
                            name="providerId"
                            value={parts.providerId}
                            placeholder="Выберите поставщика"
                            onChange={handleChange}
                            size="md"
                            isInvalid={parts === "undefined" || parts.providerId === 0}
                        >
                            <option value={0} >...</option>
                            {
                                providers && providers.map(res => {
                                    return (
                                        <option key={res.id} value={res.id}>
                                            {res.name}
                                        </option>
                                    )
                                })
                            }
                        </Form.Control>
                        <Form.Control.Feedback type="invalid">
                            Выберите поставщика
                        </Form.Control.Feedback>
                    </Form.Group>

                </Form.Row>


                    <Form.Group controlId="validationComments">
                        <Form.Label>Комментарий</Form.Label>
                        <Form.Control
                            type="text"
                            name="comments"
                            placeholder="Введите комментарий"
                            onChange={handleChange}
                            value={parts.comments}
                        />
                    </Form.Group>
      


                <Button type="button" className="btn btn-primary btn-block" onClick={handleSubmit}>Создать партию</Button>
                <Button type="button" className="btn btn-secondary btn-block" onClick={() => history.goBack()}>Назад</Button>

            </Form>
        </Container>
    )

}

export default AddParties