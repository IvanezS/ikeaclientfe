import React, { useEffect, useState } from 'react';
import { Form, Button, Container, Jumbotron, Modal, Card, Row, Col } from 'react-bootstrap';
import ItemDataService from "../Services/ItemService";
import PartDataService from "../Services/PartService";
import MainPartDataService from "../Services/MainPartService";
import { useHistory, useParams } from "react-router-dom";
import { Fragment } from 'react';
import { List, Trash, ListCheck, ArrowRight} from 'react-bootstrap-icons';
//import SelectSearch from 'react-select-search';

function AddItemPart() {
    let history = useHistory();
    let param = useParams();
    const [modalShow, setModalShow] = useState(false);
    const [modalDelShow, setModalDelShow] = useState(false);
    const [modalWarShow, setModalWarShow] = useState(false);
    const [validated, setValidated] = useState(false);
    const [activePartId, setActivePartId] = useState(null);
    const [search, setSearch] = useState({ searchtextT: "", searchtextW: "" });
    const [searchChanged, setSearchChanged] = useState(false)
    const [itemsF, setItemsF] = useState(null)
    const [items, setItems] = useState([{
        "id": 0,
        "itemName": "string",
        "thick": 0,
        "width": 0,
        "length": 0,
        "mixedLength": "No",
        "quality": "string",
        "breed": "string",
        "side": "string",
        "moisture": "string",
    }]);

    const [part, setPart] = useState({
        "state": 0,
        "partNumber": "",
        "mainPartId": 0,
        "itemId": -1,
        "comments": ""
    })

    const [mainPart, setMainPart] = useState(null)

    async function getMainPart() {
        var response = await MainPartDataService.get(param.id)
        setMainPart(response);
    }

    async function getItems() {
        var response = await ItemDataService.getAll()
        setItems(response);
    }

    useEffect(() => {
        getItems();
        getMainPart();
    }, [])

    useEffect(() => {
        SearchItem();
    }, [items,mainPart])

    useEffect(() => {
        SearchItem();
    }, [searchChanged])

    async function handleChange(e) {
        const { name, value } = e.target;
        await setPart(prevState => ({ ...prevState, [name]: value }));
    }

    async function handleChangeSearch(e) {
        const { name, value } = e.target;
        await setSearch(prevState => ({ ...prevState, [name]: value }))
        console.log(search, "search")
        setSearchChanged(true)
    }

    async function AddPart() {

        var response = await PartDataService.create(
            {
                state: 0,
                partNumber: mainPart.mainPartNumber,
                mainPartId: Number(param.id),
                itemId: Number(part.itemId),
                comments: part.comments
            }
        )

        if (response.status === 200) {
            alert('Data saved successfully');
            //history.push('/EditPartPacks/' + param.id);
        }
        else {
            alert('Data not saved');
        }
        return response

    }

    async function DeletePart() {
        setModalShow(false)
        await PartDataService.remove(activePartId)
            .then(json => {
                if (json.status === 200) {
                }
            })
        redir();
    }

    const handleSubmit = async (event) => {

        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);

        if (part.itemId !== undefined && part.itemId !== 0) {
            var response = await AddPart();
            if (response.status === 200) {
                //redir();
                getMainPart();
                setSearchChanged(true)
            } else {
                setModalShow(true)
            }
        }

    };


    function redir() {
        history.push("/");
        history.replace("/AddItemPart/" + param.id);
    }

    function ErrorModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Не получилось создать пакет</h4>
                    <p>
                        Проверьте задаваемые параметры, убедитесь в наличии связи
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>Ok</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function WarningModal(props) {

        var part = activePartId && mainPart && mainPart.itemParts.find(data => {if (data.id === activePartId) return data})
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
                    </Modal.Title>
                </Modal.Header>
                
                    {part && part.packets.length === 0 ? 
                        <Fragment>
                            <Modal.Body>
                                <h4>Нет исходных пакетов для данной партии!</h4>
                                <p>Для доступа к пересортировке. Добавьте исходные пакеты для данной партии</p>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button onClick={props.onHide}>ОК</Button>
                            </Modal.Footer>
                        </Fragment>
                        :
                        <Fragment>
                            <Modal.Body>
                                <h4>Начать пересортировку партии?</h4>
                                <p>Для доступа к пересортировке нажмите кнопку "Начать пересортировку"</p>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="warning" onClick={()=>history.push("/ResortPart/" + activePartId)}>Начать пересортировку</Button>
                                <Button onClick={props.onHide}>Отмена</Button>
                            </Modal.Footer>
                        </Fragment>
                    }

            </Modal>
        );
    }


    function DeletedModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Вы собираетесь удалить партию со всеми пакетами и статистикой</h4>
                    <p>Нажмите кнопку "Удалить" для подтверждения или кнопку "Отмена"</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={DeletePart}>Удалить</Button>
                    <Button onClick={props.onHide}>Отмена</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function setStatePfunc(state) {

        if (state === 0) return ("")
        if (state === 1) return ("warning")
        if (state === 2) return ("success")
    }


    function SearchItem() {


        var itemsOfPart = mainPart && mainPart.itemParts.reduce((prev, current) => prev.concat(current.item), [])

        var filteredItems = itemsOfPart && items && items.filter(e => !itemsOfPart.map(e2 => e2.itemName).includes(e.itemName));

        var filteredSearchItem = filteredItems && filteredItems.filter(data => {
            if (search.searchtextT === "" && search.searchtextW === "") return data
            if (search.searchtextT !== "" && search.searchtextW !== "" && data.thick === Number(search.searchtextT) && data.width === Number(search.searchtextW)) return data
            if (search.searchtextT !== "" && search.searchtextW === "" && data.thick === Number(search.searchtextT)) return data
            if (search.searchtextW !== "" && search.searchtextT === "" && data.width === Number(search.searchtextW)) return data

        })

        var combo = filteredSearchItem && filteredSearchItem.map(function (ol, index) {

            return (
                <option key={index} value={ol.id}>
                    {ol.itemName}:   {ol.thick}x{ol.width}x{ol.mixedLength === "Yes" ? "MIX" : ol.length };   {ol.quality}; {ol.breed}; {ol.moisture}
                </option>
            )
        })
        setSearchChanged(false)
        setItemsF(() => combo)
    }

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Список ITEM-кодов партии {mainPart && mainPart.mainPartNumber}</h1>
            </Jumbotron>

            <DeletedModal
                show={modalDelShow}
                onHide={() => setModalDelShow(false)}
            />
            <WarningModal
                show={modalWarShow}
                onHide={() => setModalWarShow(false)}
            />
            <ErrorModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
            <Form noValidate validated={validated} onSubmit={handleSubmit}>

                <Form.Row>
                    <Form.Group as={Col}>
                        <Form.Label>Поиск по толщине ITEM кода</Form.Label>
                        <Form.Control
                            required
                            type="number"
                            name="searchtextT"
                            placeholder="Поиск"
                            onChange={handleChangeSearch}
                            value={search.searchtextT}
                        />
                    </Form.Group>
                    <Form.Group as={Col}>
                        <Form.Label>Поиск по ширине ITEM кода</Form.Label>
                        <Form.Control
                            required
                            type="number"
                            name="searchtextW"
                            placeholder="Поиск"
                            onChange={handleChangeSearch}
                            value={search.searchtextW}
                        />
                    </Form.Group>
                </Form.Row>
                <Form.Group controlId="validationItemId">
                    <Form.Label> Список ITEM-кодов</Form.Label>
                    <Form.Control

                        as="select"
                        name="itemId"
                        placeholder="Выберите Item"
                        onChange={handleChange}
                        size="md"
                        title="ITEM-код для пакета"
                        value={part.itemId}
                        isInvalid={part.itemId === undefined || part.itemId < 0}
                        isValid={part.itemId !== undefined && part.itemId >= 0}
                    >
                        <option value={-1}>Выберите ITEM-код для пакета</option>
                        
                        {itemsF}
                    </Form.Control>
                    <Form.Control.Feedback type="invalid">
                        Пожалуйста, выберите ITEM-код для пакета
                    </Form.Control.Feedback>
                </Form.Group>


                <Form.Group>
                    <Form.Label>Комментарий</Form.Label>
                    <Form.Control required type="text" name="comments" id="comments" placeholder="Комментарий" value={part.comments} onChange={handleChange} />
                </Form.Group>

                <Button type="button" className="btn btn-primary btn-block" onClick={handleSubmit}>Создать ITEM для партии</Button>
                <Button type="button" className="btn btn-secondary btn-block" onClick={() => history.goBack()}>Назад</Button>
                {/* <Button type="button" className="btn btn-secondary btn-block" onClick={() => {history.push("/"); history.replace("/MainPartList")}}>К списку партий</Button> */}
            </Form>
            <p> </p>
            {
                mainPart && mainPart.itemParts.map(function (ol, index) {
                    return (
                        <Fragment key={index}>

                            <Card border={setStatePfunc(ol.state)} fluid>
                                <Card.Header>
                                    <Row>
                                        <Col sm={2}>
                                            <h3>{ol.partNumber}</h3>
                                        </Col>
                                        <Col>
                                            <h3>{ol.item.itemName}, {ol.item.thick}x{ol.item.width} L: {ol.item.mixedLength === "Yes" ? "MIX" : ol.item.length}, {ol.item.quality}, {ol.item.breed}, {ol.item.side}, {ol.item.moisture}</h3>
                                        </Col>
                                        <Col sm={2}>
                                            
                                            <Button type="button" onClick={() => { setActivePartId(ol.id); setModalDelShow(true) }} className="btn btn-danger" title="Удалить ITEM из партии"><Trash /></Button>
                                            <Button type="button" onClick={() => history.push("/AddPack/" + ol.id)} className="btn btn-secondary" title="К списку исходных пакетов"><List /></Button>
                                            <Button type="button" onClick={() => history.push("/AddResortedPack/" + ol.id)} className="btn btn-primary" title="К списку пакетов"><ListCheck /></Button>
                                            <Button type="button" onClick={() => {  setActivePartId(ol.id); setModalWarShow(true) }} className="btn btn-warning" title="Пересортировка пакетов данной партии"><ArrowRight /></Button>
                                        </Col>

                                    </Row>
                                </Card.Header>
                            </Card>

                        </Fragment>

                    )
                })
            }


        </Container>
    )
}

export default AddItemPart