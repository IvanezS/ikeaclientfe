import React, { useState, useEffect } from 'react';
import { Form, Container, Jumbotron, Modal, Button, Row, Col } from 'react-bootstrap';
import PartDataService from "../Services/PartService";
import { useHistory } from "react-router-dom";
import { ArrowRight, List, ListCheck, PencilFill, Trash, ClipboardData } from 'react-bootstrap-icons';
import { useSelector } from 'react-redux'
import { Fragment } from 'react';

function PartList() {

    let history = useHistory();
    const [parties, setParties] = useState(null)
    const [activePartId, setActivePartId] = useState(null);
    const [modalShow, setModalShow] = useState(false);
    const [modalWarShow, setModalWarShow] = useState(false);
    const [modalStatisticShow, setModalStatisticShow] = useState(false);
    const [search, setSearch] = useState({searchtext: ""});
    const user = useSelector(state => state.auth.user)

    useEffect(() => {
        
        async function getParties() {
            const party = await PartDataService.getAll()
            setParties(party);
            console.log(party, 'parties');
        }
        getParties();
    }, [])


    async function DeletePart() {
        setModalShow(false)
        await PartDataService.remove(activePartId)
            .then(json => {
                if (json.status === 200) {
                }
            })
        history.push("/");
        history.replace("/PartList");
    }

    function DeletedModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Вы собираетесь удалить партию со всеми пакетами и статистикой</h4>
                    <p>Нажмите кнопку "Удалить" для подтверждения или кнопку "Отмена"</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={DeletePart}>Удалить</Button>
                    <Button onClick={props.onHide}>Отмена</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function WarningModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Нет исходных пакетов для данной партии!</h4>
                    <p>Для доступа к пересортировке. Добавьте исходные пакеты для данной партии</p>
                </Modal.Body>
                <Modal.Footer>
                    {/* <Button variant="danger" onClick={gotoResort}>Продолжить</Button> */}
                    <Button onClick={props.onHide}>ОК</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function StatisticModal(props) {
        debugger
        var part = parties && parties.find(data => {
            if (data.id === activePartId) return data
        })
        var totalVolumeIn = part && part.packets.reduce((total, currentValue) => total = total + currentValue.totalVolume, 0)
        var totalVolumeOut = part && part.resortedPackets.reduce((total, currentValue) => total = total + currentValue.totalVolume, 0)
        var BadPackets = part && part.resortedPackets.filter(data => {
            if (data.isDefected) return data
        })
        var totalVolumeBad = part && BadPackets.reduce((total, currentValue) => total = total + currentValue.totalVolume, 0)
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                    СТАТИСТИКА ПО {part && part.state === 2 ? "ЗАКРЫТОЙ" : "НЕЗАКРЫТОЙ"} ПАРТИИ {part && part.partNumber}   
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col>
                            <h4>Исходные пакеты:</h4>
                            <p>Кол-во: {part && part.packets.length} шт</p>
                            <p>Объём: {part && totalVolumeIn.toFixed(3)} м3</p>
                        </Col>

                        <Col>
                            <h4>Нормальные пакеты:</h4>
                            <p>Кол-во: {part && part.resortedPackets.length - BadPackets.length} шт</p>
                            <p>Объём: {part && (totalVolumeOut - totalVolumeBad).toFixed(3) } м3</p>
                        </Col>
                        <Col>
                            <h4>Бракованные пакеты:</h4>
                            <p>Кол-во: {part && BadPackets.length} шт</p>
                            <p>Объём: {part && totalVolumeBad.toFixed(3)} м3</p>
                        </Col>
                        <Col>
                            <h4>Суммарно по пакетам:</h4>
                            <p>Кол-во: {part && part.resortedPackets.length} шт</p>
                            <p>Объём: {part && totalVolumeOut.toFixed(3)} м3</p>
                        </Col>
                    </Row>
                    <h4>Итоги:</h4>
                    <p>Баланс по объёму (Vdelta = Vисх - Vпересорт): {part && (totalVolumeIn - totalVolumeOut).toFixed(3)} м3</p>
                    <p>Процент брака: {part && (totalVolumeBad * 100 / totalVolumeOut).toFixed(3)} %</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>ОК</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function setActivePart(id) {
        setActivePartId(id);
        setModalShow(true)
    }

    function setWarningPart(id) {
        setActivePartId(id);
        var part = parties.find(element => { return element.id === id });
        part.packets.length === 0 ? setModalWarShow(true) : gotoResort(id);
    }

    function gotoResort(id) {
        history.push("/ResortPart/" + id)
    }

    function setStatePfunc(state) {

        if (state === 0) return ("")
        if (state === 1) return ("table-warning")
        if (state === 2) return ("table-success")
    }


    function QuantPack(packs) {
        var result = packs.packets.filter(element => { return element.state === 1 })
        return result.length
    }

    const handleChange = e => {
        const { name, value } = e.target;
        setSearch(prevState => ({ ...prevState, [name]: value }));
    }

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Список партий</h1>
            </Jumbotron>

            <DeletedModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

            <WarningModal
                show={modalWarShow}
                onHide={() => setModalWarShow(false)}
            />

            
            <StatisticModal
                show={modalStatisticShow}
                onHide={() => setModalStatisticShow(false)}
            />

            
            {user && (user.profile.role === "Admin" || user.profile.role === "Master") ?
                <Fragment>
                    <hr />
                    <Button type="button" onClick={() => history.push("/AddParties")} className="btn btn-primary" title="Редактировать данные партии">Добавить новую партию</Button>
                    <hr />
                </Fragment>
            : ''}
            

            <Form className="form">
                    <Form.Group>
                        <Form.Label>Поиск по номеру партии или поставщику</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            name="searchtext"
                            placeholder="Поиск"
                            onChange={handleChange}
                            value={search.searchtext}
                        />
                    </Form.Group>
                {parties ?
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Номер партии</th>
                                <th>Поставщик</th>
                                <th>Сечение</th>
                                <th>Комментарий</th>
                                <th title="Общее кол-во пришедших пакетов на вход из общего кол-ва -> Получившееся кол-во пакетов">Пакеты</th>
                                <th>Действие</th>
                            </tr>
                        </thead>

                        <tbody>
                            {parties && parties.filter(data => {
                                if (search === null) {
                                    return data
                                } else {
                                    if(data.provider.name.toLowerCase().includes(search.searchtext.toLowerCase()) || data.partNumber.toLowerCase().includes(search.searchtext.toLowerCase())) return data
                                }
                            }).map(function (el, index) {
                                return (
                                    <tr key={index} className={setStatePfunc(el.state)}>
                                        <td>{el.partNumber}</td>
                                        <td>{el.provider.name}</td>
                                        <td>{el.width}x{el.thick}</td>
                                        <td>{el.comments}</td>
                                        <td>({QuantPack(el)}/{el.packets.length})-{'>'}{el.resortedPackets.length}</td>
                                        <td>
                                        <Button type="button" onClick={() => { setActivePartId(el.id);setModalStatisticShow(true)}} className="btn btn-info" title="Статистика по партии"><ClipboardData /></Button>
                                            {user && (user.profile.role === "Admin" || user.profile.role === "Master") ?
                                                <Fragment>
                                                    <Button type="button" onClick={() => history.push("/EditPart/" + el.id)} className="btn btn-success" title="Редактировать данные партии"><PencilFill /></Button>
                                                    <Button type="button" onClick={() => setActivePart(el.id)} className="btn btn-danger" title="Удалить партию"><Trash /></Button>
                                                </Fragment>
                                            : ''}
                                            <Button type="button" onClick={() => history.push("/EditPartPacks/" + el.id)} className="btn btn-secondary" title="Список исходных пакетов партии"><List /></Button>
                                            {user && (user.profile.role === "Admin" || user.profile.role === "Oper") ?
                                                <Fragment>
                                                    <Button type="button" onClick={() => history.push("/ResortedPacksList/" + el.id)} className="btn btn-primary" title="Список пересортированных пакетов партии"><ListCheck /></Button>
                                                    <Button type="button" onClick={() => setWarningPart(el.id)} className="btn btn-warning" title="Пересортировка пакетов данной партии"><ArrowRight /></Button>
                                                </Fragment>
                                            : ''}
                                            
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>

                    </table>
                    : "Нет ни одной партии"}
            </Form>

        </Container>
    );


}

export default PartList