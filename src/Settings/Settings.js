import React, { useState, useEffect } from 'react';
import PlcDataService from "../Services/PlcDBService";
import { Button, Container, Jumbotron, Form, Modal } from 'react-bootstrap';
import { useHistory } from "react-router-dom";

function Settings() {

    let history = useHistory();
    const [modalShow, setModalShow] = useState(false);
    const [sett, setSett] = useState({
        rpackNumber: 0,
        dryPackNumber: 0,
        smenaName: "Смена 1",
        smenaNum: 0,
        smenaArr: [
            { value: "Смена 1" },
            { value: "Смена 2" },
            { value: "Смена 3" },
            { value: "Смена 4" }
        ]
    })



    useEffect(() => {
        async function getSetPlcDB() {
            var response = await PlcDataService.getPlcSetDB()
            setSett(prevState => ({
                ...prevState,
                rpackNumber: response.rpackNumber,
                dryPackNumber: response.dryPackNumber,
                smenaName: (response.smenaName === "") ? "Смена 1" : response.smenaName,
                smenaNum: response.smenaNum,
            }));
        }
        getSetPlcDB();
    }, [])



    const handleChange = e => {
        debugger
        const { name, value } = e.target;
        setSett(prevState => ({ ...prevState, [name]: value }));
    }

    async function onSubmit () {
        debugger;
        await PlcDataService.setMainSettings(
            {
                smenaNum: Number(sett.smenaNum), //Номер смены
                smenaName: sett.smenaName, //Имя смены
                rpackNumber: Number(sett.rpackNumber), //Текущий номер пакета
                dryPackNumber: Number(sett.dryPackNumber) //Текущий номер пакета

            }
        )
            .then(
                response => {
                    console.log(response);
                }
            )
            .catch(
                function (error) {
                    console.log(error);
                }
            );
            setModalShow(false);
    }

    function WarModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Вы собираетесь загрузить текущие настройки</h4>
                    <p>Нажмите кнопку "Продолжить" для того чтобы загрузить настройки в ПЛК или на кнопку "Отмена"</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button type="button" onClick={onSubmit} className="btn btn-primary">Продолжить</Button>
                    <Button className="btn btn-secondary" onClick={props.onHide}>Отмена</Button>
                </Modal.Footer>
            </Modal>
        );
    }


    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Общие настройки</h1>
            </Jumbotron>

            <WarModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

            <Form className="form" onSubmit={onSubmit}>

                <Form.Group>
                    <Form.Label for="rpackNumber">Текущий номер пересортированного пакета партии</Form.Label>
                    <Form.Control type="number" name="rpackNumber" id="rpackNumber" value={sett.rpackNumber} onChange={handleChange} />
                </Form.Group>

                <Form.Group>
                    <Form.Label for="dryPackNumber">Текущий номер сухого пакета</Form.Label>
                    <Form.Control type="number" name="dryPackNumber" id="dryPackNumber" value={sett.dryPackNumber} onChange={handleChange} />
                </Form.Group>

                <Form.Group>
                    <Form.Label for="smenaNum">Текущий номер смены</Form.Label>
                    <Form.Control type="number" name="smenaNum" id="smenaNum" value={sett.smenaNum} onChange={handleChange} />
                </Form.Group>

                <Form.Group>
                    <Form.Label for="smenaName">Текущая смена</Form.Label>
                    <Form.Control
                        required
                        as="select"
                        name="smenaName"
                        id="smenaName"
                        placeholder="Выберите текущую схему"
                        onChange={handleChange}
                        size="md"
                        title="Выбор смены"
                    >
                        {
                            sett.smenaArr.map(res => {
                                if (sett.smenaName === res.value) {
                                    return (
                                        <option selected value={res.value}>
                                            {res.value}
                                        </option>
                                    )
                                }
                                else {
                                    return (
                                        <option value={res.value}>
                                            {res.value}
                                        </option>
                                    )
                                }
                            })
                        }
                    </Form.Control>
                </Form.Group>


                <Button type="button" className="btn btn-primary btn-block" onClick={()=>setModalShow(true)}>Сохранить</Button>
                <Button type="button" className="btn btn-secondary btn-block" onClick={() => history.goBack()}>Назад</Button>

            </Form>
        </Container>
    );


}



export default Settings;






