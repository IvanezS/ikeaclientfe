import React, { useEffect, useState, Fragment } from 'react';
import { Form, Button, Container, Jumbotron, Modal, Row, Col, InputGroup, Card, Accordion } from 'react-bootstrap';
import PackDataService from "../Services/ResortedPackService";
import PartDataService from "../Services/PartService";
import MainPartDataService from "../Services/MainPartService";
import { useHistory, useParams } from "react-router-dom";
import { PencilFill, Printer, Trash, ArrowRight, List, ListCheck, Globe } from 'react-bootstrap-icons';

function AddResortedPack() {

    const [modalShow, setModalShow] = useState(false);
    const [modalErrShow, setModalErrShow] = useState(false);
    const [validated, setValidated] = useState(false);
    const [pack, setPack] = useState({ "QPack": 0, "VPack": 0, "status": false });
    const [part, setPart] = useState(null)
    const [mainPart, setMainPart] = useState(null)
    const [otherPacks, SetOtherPacks] = useState(null)
    let history = useHistory();
    let param = useParams();
    const [activePackId, setActivePackId] = useState(null);
    const [modalWarShow, setModalWarShow] = useState(false);
    const [activePartId, setActivePartId] = useState(null);

    useEffect(() => {
        getPart();
    }, [])

    useEffect(() => {
        CalculateV()
        console.log("pack.QPack =", pack.QPack)
    }, [pack.QPack])

    useEffect(() => {
        getOtherPacks()
    }, [mainPart])

    async function getPart() {
        var response = await PartDataService.get(param.id)
        {response.resortedPackets.sort(function (a, b) {
            if (a.packDate < b.packDate) {
              return 1;
            }
            if (a.packDate > b.packDate) {
              return -1;
            }
            // a должно быть равным b
            return 0;
          })}
        setPart(response)
        console.log(response, 'part');
        getMainPart(response.mainPartId) 
    }

    async function getMainPart(mainPartId) {
        var response = await MainPartDataService.get(Number(mainPartId))
        setMainPart(response)
        console.log(response, 'MainPart');
    }

    async function getOtherPacks() {
        var otherParts = mainPart && mainPart.itemParts.filter(data => {
            if (Number(param.id) !== Number(data.id)) return data
        })
        var ResortedPackets = otherParts && otherParts.reduce((prev, current) => prev.concat(current.resortedPackets), [])
        ResortedPackets && ResortedPackets.sort(function (a, b) {
            if (a.packDate < b.packDate) {
              return 1;
            }
            if (a.packDate > b.packDate) {
              return -1;
            }
            // a должно быть равным b
            return 0;
          });
        SetOtherPacks(ResortedPackets)
    }

    function getPartNameById(id) {
        debugger
        var otherParts = mainPart && mainPart.itemParts.filter(data => {
            if (Number(param.id) !== (data.id)) return data
        })
        var filteredPart = otherParts && otherParts.find(data => {
            if (id === data.id) return data
        })
        return filteredPart && filteredPart.partNumber
    }

    async function CalculateV() {
        debugger
        if (part && part.item.mixedLength === "No") {
            let V = part.item.thick * part.item.width * part.item.length * Number(pack.QPack) * 0.000000001
            await setPack(prevState => ({ ...prevState, VPack: V }));
        }
    }

    async function DeletePack() {
        debugger
        var response = await PackDataService.remove(activePackId)
        if (response.status === 200) {
            getPart();
        } else {
            setModalErrShow(true)
        }
        setModalShow(false)
    }

    const handleChange = e => {
        const { name, value } = e.target;
        setPack(prevState => ({ ...prevState, [name]: value }));
    }

    async function PrintPack(id) {
        var response = await PackDataService.PrintRpackPDF(id)
        if (response.status === 200) {
            alert('Пакет успешно распечатан');
        }
    }

    async function AddPack() {
debugger
        var response = await PackDataService.create(
            {

                state: 0,
                packetNumber: 0,
                isDefected: pack.status,
                partId: Number(param.id),
                totalPlanks: Number(pack.QPack),
                totalVolume: Number(pack.VPack),
                smenaNum: 0,
                smenaName: ""


            }
        )
        setModalShow(false);
        return response

    }

    const handleSubmit = async (event) => {
        debugger
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);

        //if (form.checkValidity() === true) {
        if ( pack.QPack !== undefined && !pack.VPack <= 0
       &&  pack.VPack !== undefined && !pack.VPack <= 0
            
            ) {
            var response = await AddPack();
            if (response.status === 200) {
                getPart();
                alert("Пакет успешно создан вручную")
            } else {
                setModalShow(true)
            }
        }

    };

    function setActivePack(id) {
        setActivePackId(id);
        setModalShow(true)
    }

    function WarningModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
                    </Modal.Title>
                </Modal.Header>
                
                    {part && part.packets.length === 0 ? 
                        <Fragment>
                            <Modal.Body>
                                <h4>Нет исходных пакетов для данной партии!</h4>
                                <p>Для доступа к пересортировке. Добавьте исходные пакеты для данной партии</p>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button onClick={props.onHide}>ОК</Button>
                            </Modal.Footer>
                        </Fragment>
                        :
                        <Fragment>
                            <Modal.Body>
                                <h4>Начать пересортировку партии?</h4>
                                <p>Для доступа к пересортировке нажмите кнопку "Начать пересортировку"</p>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="warning" onClick={()=>history.push("/ResortPart/" + activePartId)}>Начать пересортировку</Button>
                                <Button onClick={props.onHide}>Отмена</Button>
                            </Modal.Footer>
                        </Fragment>
                    }

            </Modal>
        );
    }

    function DeletedModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Вы собираетесь удалить пакет</h4>
                    <p>
                        Нажмите кнопку "Удалить" для подтверждения или кнопку "Отмена"
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={DeletePack}>Удалить</Button>
                    <Button onClick={props.onHide}>Отмена</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function ErrorModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Не получилось создать пакет</h4>
                    <p>
                        Проверьте задаваемые параметры, убедитесь в наличии связи
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>Ok</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Список пересортированных пакетов {part && part.partNumber} </h1>
            </Jumbotron>


            <WarningModal
                show={modalWarShow}
                onHide={() => setModalWarShow(false)}
            />

            <Row>
                <Col>
                    <h3>{part && part.item.itemName}, {part && part.item.thick}x{part && part.item.width} L: {part && part.item.mixedLength === "Yes" ? "MIX" : part && part.item.length}, {part && part.item.quality}, {part && part.item.breed}, {part && part.item.side}, {part && part.item.moisture}</h3>

                </Col>
                <Col sm="4">
                    <Card>
                        <Card.Header>
                                                      
                            <Button type="button" onClick={() => {history.push("/AddItemPart/" + part.mainPartId)}} className="btn btn-dark" title="Список ITEM-кодов для партии"><List /> </Button>
                            <Button type="button" onClick={() => history.push("/AddPack/" + param.id)} className="btn btn-secondary" title="Список исходных пакетов партии"><List /></Button>
                            <Button type="button" onClick={() => history.push("/AddResortedPack/" + param.id)} className="btn btn-primary" title="Список пересортированных пакетов партии"><ListCheck /></Button>
                            <Button type="button" onClick={() => {  setActivePartId(param.id); setModalWarShow(true) }} className="btn btn-warning" title="Пересортировка пакетов данной партии"><ArrowRight /></Button>
                        </Card.Header>
                    </Card>
                </Col>
            </Row>
            <hr />

            <ErrorModal
                show={modalErrShow}
                onHide={() => setModalErrShow(false)}
            />

            <DeletedModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

            <Accordion>
                <Card>
                    <Card.Header>
                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                        Добавить пересортированный пакет вручную
                    </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                    <Card.Body>
                        <Form noValidate validated={validated} onSubmit={handleSubmit}>

                            <Row>

                                <Form.Group as={Col}>
                                    <Form.Label>Кол-во досок в пакете, шт</Form.Label>
                                    <Form.Control required type="number" min = "1" max = "2000" name="QPack" id="QPack" placeholder="Кол-во досок в пакете" value={pack.QPack} onChange={handleChange} isInvalid = {pack.QPack <= 0}/>
                                    <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                                    <Form.Control.Feedback type="invalid">
                                        Пожалуйста, введите кол-во досок в пакете
                                    </Form.Control.Feedback>
                                </Form.Group>


                                <Form.Group as={Col}>
                                    <Form.Label>Объём пакета, м3</Form.Label>
                                    <Form.Control required type="number" min = "0.001" max = "20" step = "0.001" name="VPack" id="VPack" placeholder="Объём пакета" value={(part && part.item.mixedLength === "No") ? pack.VPack.toFixed(3) : pack.VPack} onChange={handleChange} disabled={part && part.item.mixedLength === "No"}  isInvalid = {pack.VPack <= 0}/>
                                    <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                                    <Form.Control.Feedback type="invalid">
                                        Пожалуйста, введите объём пакета
                                    </Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group as={Col}>
                                    <Form.Label>Статус пакета</Form.Label>
                                    <InputGroup className="mb-3">
                                        <InputGroup.Prepend>
                                            <Button type="button" className="btn btn-success" onClick={() => setPack(prevState => ({ ...prevState, status: false }))}>OK</Button>
                                            <Button type="button" className="btn btn-danger" onClick={() => setPack(prevState => ({ ...prevState, status: true }))}>NOK</Button>
                                        </InputGroup.Prepend>
                                        <Form.Control required type="text" name="status" placeholder="Введите статус пакета" value={pack.status === true ? "NOK" : "OK"} disabled />
                                    </InputGroup>
                                </Form.Group>

                            </Row>

                            <Button type="button" className="btn btn-primary btn-block" onClick={handleSubmit}>Создать пакет</Button>
                            <Button type="button" className="btn btn-secondary btn-block" onClick={() => history.goBack()}>Назад</Button>

                        </Form>
                    </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>


            {(part && part.resortedPackets.length > 0) ?
                <table className="table">
                    <thead>
                        <tr>
                            <th>№ пакета</th>
                            <th>№ смены</th>
                            <th>Имя смены</th>
                            <th>Тип пакета</th>
                            <th>Кол-во</th>
                            <th>Объём</th>
                            <th>Дата</th>
                            <th></th>

                        </tr>
                    </thead>
                    <tbody>
                        {part && part.resortedPackets.map(function (el, index) {
                            return (

                                <tr key={index}>
                                    <td>{el.packetNumber}</td>
                                    <td>{el.smenaNum}</td>
                                    <td>{el.smenaName}</td>
                                    <td>{el.isDefected ? "NOK" : "OK"}</td>
                                    <td>{el.totalPlanks}</td>
                                    <td>{el.totalVolume.toFixed(3)}</td>
                                    <td>{el.packDate}</td>
                                    <td>
                                        <Button type="button" onClick={() => history.push("/EditResortedPack/" + el.id)} className="btn btn-success" title="Редактировать пакет"><PencilFill /></Button>
                                        <Button type="button" onClick={() => setActivePack(el.id)} className="btn btn-danger" title="Удалить пакет"><Trash /></Button>
                                        <Button type="button" onClick={() => PrintPack(el.id)} className="btn btn-info" title="Распечатать этикетку"><Printer /></Button>
                                        <Button type="button" className="btn btn-secondary" title="Отправить пакет в М3" disabled ><Globe/> </Button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                : "Нет пакетов партии"
            }

            <hr />
            <h3>Другие пакеты партии:</h3>
            <hr />

            {(otherPacks && otherPacks.length > 0)  ?
                <table className="table">
                    <thead>
                        <tr>
                            <th>Имя партии</th>
                            <th>№ пакета</th>
                            <th>№ смены</th>
                            <th>Имя смены</th>
                            <th>Тип пакета</th>
                            <th>Кол-во</th>
                            <th>Объём</th>
                            <th>Дата</th>
                            <th></th>

                        </tr>
                    </thead>
                    <tbody>
                        {otherPacks && otherPacks.map(function (el, index) {
                            return (

                                <tr key={index}>

                                    <td>{getPartNameById(el.partId)}</td>
                                    <td>{el.packetNumber}</td>
                                    <td>{el.smenaNum}</td>
                                    <td>{el.smenaName}</td>
                                    <td>{el.isDefected ? "NOK" : "OK"}</td>
                                    <td>{el.totalPlanks}</td>
                                    <td>{el.totalVolume.toFixed(3)}</td>
                                    <td>{el.packDate}</td>
                                    <td>
                                        <Button type="button" onClick={() => history.push("/EditResortedPack/" + el.id)} className="btn btn-success" title="Редактировать пакет"><PencilFill /></Button>
                                        <Button type="button" onClick={() => setActivePack(el.id)} className="btn btn-danger" title="Удалить пакет"><Trash /></Button>
                                        <Button type="button" onClick={() => PrintPack(el.id)} className="btn btn-info" title="Распечатать этикетку"><Printer /></Button>
                                        <Button type="button" className="btn btn-secondary" title="Отправить пакет в М3" disabled ><Globe/> </Button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                : "Нет других пакетов партии"
            }
        </Container>
    )
}

export default AddResortedPack