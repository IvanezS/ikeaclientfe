import React, { useEffect, useState } from 'react';
import { Form, Button, Container, Jumbotron, Modal, Row, Col, InputGroup } from 'react-bootstrap';
import PackDataService from "../Services/ResortedPackService";
import ItemDataService from "../Services/ItemService";
import { useHistory, useParams } from "react-router-dom";


function EditResortedPack() {


    const [modalShow, setModalShow] = useState(false);
    const [validated, setValidated] = useState(false);
    const [pack, setPack] = useState({ "totalPlanks": 0, "totalVolume": 0, "isDefected": false });
    const [item, setItem] = useState(null);

    let history = useHistory();
    let param = useParams();

    useEffect(() => {
        async function getPack() {
            var response = await PackDataService.get(param.id)
            setPack(response);
            getItem(response.part.itemId)
            console.log("pack", response)
        }

        async function getItem(id) {
            var response = await ItemDataService.get(id)
            setItem(response);
            console.log("item", response)
        }


        getPack();
    }, [])


    useEffect(() => {
        CalculateV()
        console.log("pack.totalPlanks =", pack.totalPlanks)
    }, [pack.totalPlanks])

    async function CalculateV() {
        debugger
        if (item && item.mixedLength === "No") {
            let V = item.thick * item.width * item.length * Number(pack.totalPlanks) * 0.000000001
            await setPack(prevState => ({ ...prevState, totalVolume: V }));
        }
    }

    const handleChange = e => {
        const { name, value } = e.target;
        setPack(prevState => ({ ...prevState, [name]: value }));
    }

    async function UpdatePack() {
        debugger
        var response = await PackDataService.update(
            Number(param.id),
            {

                state: 0,
                packetNumber: Number(pack.packetNumber),
                isDefected: pack.isDefected,
                partId: 0,
                totalPlanks: Number(pack.totalPlanks),
                totalVolume: Number(pack.totalVolume),
                smenaNum: 0,
                smenaName: ""


            }
        )

        return response

    }

    const handleSubmit = async (event) => {

        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);

        //if (form.checkValidity() === true) {
        if (pack.totalPlanks > 0
            && pack.totalVolume > 0
            && pack.totalVolume !== undefined
            && pack.totalPlanks !== undefined) {
                debugger
            var response = await UpdatePack();
            if (response.status === 200) {
                redir();
            } else {
                setModalShow(true)
            }
        }

    };

    function redir() {
        history.push("/");
        history.replace("/AddResortedPack/" + pack.partId);
    }

    function ErrorModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Не получилось создать пакет</h4>
                    <p>
                        Проверьте задаваемые параметры, убедитесь в наличии связи
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>Ok</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Редактирование пересортированного пакета </h1>
            </Jumbotron>

            <ErrorModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
            <Form noValidate validated={validated} onSubmit={handleSubmit}>

                <Row>
                    <Form.Group as={Col}>
                        <Form.Label>Номер пакета</Form.Label>
                        <Form.Control required type="number" name="packetNumber" placeholder="Номер пакета" value={pack && pack.packetNumber} onChange={handleChange}/> 
                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                            Пожалуйста, введите номер пакета
                        </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group as={Col}>
                        <Form.Label>Кол-во досок в пакете, шт</Form.Label>
                        <Form.Control required type="number" name="totalPlanks"  placeholder="Кол-во досок в пакете" value={pack && pack.totalPlanks} onChange={handleChange} />
                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                            Пожалуйста, введите кол-во досок в пакете
                        </Form.Control.Feedback>
                    </Form.Group>


                    <Form.Group as={Col}>
                        <Form.Label>Объём пакета, м3</Form.Label>
                        <Form.Control required type="text" name="totalVolume" placeholder="Объём пакета" value={(item && item.mixedLength === "No") ? pack && pack.totalVolume.toFixed(3) : pack && pack.totalVolume} onChange={handleChange} disabled={item && item.mixedLength === "No"} /> 
                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                            Пожалуйста, введите объём пакета
                        </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group as={Col}>
                        <Form.Label>Статус пакета</Form.Label>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <Button type="button" className="btn btn-success" onClick={() => setPack(prevState => ({ ...prevState, isDefected: false }))}>OK</Button>
                                <Button type="button" className="btn btn-danger" onClick={() => setPack(prevState => ({ ...prevState, isDefected: true }))}>NOK</Button>
                            </InputGroup.Prepend>
                            <Form.Control required type="text" name="status" placeholder="Введите статус пакета" value={pack && pack.isDefected === true ? "NOK" : "OK"} disabled />
                        </InputGroup>
                    </Form.Group>

                </Row>
                <Button type="button" className="btn btn-primary btn-block" onClick={handleSubmit}>Обновить пакет</Button>
                <Button type="button" className="btn btn-secondary btn-block" onClick={() => history.goBack()}>Назад</Button>
            </Form>

        </Container>
    )


}



export default EditResortedPack