import React, { useState, useEffect } from 'react';
import { Form, Container, Jumbotron, Button, Modal } from 'react-bootstrap';
import PartDataService from "../Services/PartService";
import PackDataService from "../Services/ResortedPackService";
import { useHistory, useParams } from "react-router-dom";
import { PencilFill, Printer, Trash } from 'react-bootstrap-icons';
import { useSelector } from 'react-redux'
import { Fragment } from 'react';
import axios from 'axios'

function ResortedPacksList() {
    const user = useSelector(state => state.auth.user)
    let history = useHistory();
    let param = useParams();
    const [parties, setParties] = useState(null)
    const [packs, setPacks] = useState(null)
    const [activePackId, setActivePackId] = useState(null);
    const [modalShow, setModalShow] = useState(false);
    const [modalErrShow, setModalErrShow] = useState(false);
    const [modalWarShow, setModalWarShow] = useState(false);

    useEffect(() => {

        axios.all([
            PartDataService.get(param.id),
            PackDataService.getByPartId(param.id)

          ])
          .then(axios.spread((obj1, obj2) => {
            console.log("Part data", obj1);
            setParties(obj1);
            console.log("Pack data", obj2);
            setPacks(obj2);
          }));

    }, [])

    async function DeletePack() {
        debugger
        var response = await PackDataService.remove(activePackId)
        if (response.status === 200) {
            //window.location.reload();
            history.push("/")
            history.replace("/ResortedPacksList/" + param.id)
        }else{
            setModalErrShow(true)
        }
    }

    function setActivePack(id) {
        setActivePackId(id);
        setModalShow(true)
    }

    async function PrintPack() {
        var response = await PackDataService.PrintRpackPDF(packs.id)
        if (response.status === 200) {
            alert('Пакет успешно распечатан');
        }
    }

    function setWarningPack(id) {
        parties &&  parties.packets.length === 0 ? setModalWarShow(true) : gotoResort(id);
    }

    function gotoResort(id) {
        history.push("/ResortPart/" + id)
    }

    function DeletedModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Вы собираетесь удалить пакет</h4>
                    <p>
                        Нажмите кнопку "Удалить" для подтверждения или кнопку "Отмена"
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={DeletePack}>Удалить</Button>
                    <Button onClick={props.onHide}>Отмена</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function ErrorModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ОШИБКА!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>Что-то пошло не так. Проверьте связь, обновите страницу</p>

                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>OK</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function WarningModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Нет исходных пакетов для данной партии!</h4>
                    <p>Для доступа к пересортировке. Добавьте исходные пакеты для данной партии</p>
                </Modal.Body>
                <Modal.Footer>
                    {/* <Button variant="danger" onClick={gotoResort}>Продолжить</Button> */}
                    <Button onClick={props.onHide}>ОК</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Список пересортированных пакетов партии {parties && parties.partNumber}</h1>
            </Jumbotron>
            <p>Имя: "{parties && parties.partNumber}", Комментарий: "{parties && parties.comments}"</p>
            
            <DeletedModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

            <ErrorModal
                show={modalErrShow}
                onHide={() => setModalErrShow(false)}
            />

            <WarningModal
                show={modalWarShow}
                onHide={() => setModalWarShow(false)}
            />

                {user && (user.profile.role === "Admin" || user.profile.role === "Oper") ?
                <Fragment>
                    <hr />
                    <Button type="button" onClick={() => history.push("/AddResortedPack/" + param.id)} className="btn btn-primary" title="Пересортировка пакетов данной партии">Добавить новый пакет</Button>
                    <Button type="button" onClick={() => setWarningPack(param.id)} className="btn btn-warning" title="Пересортировка пакетов данной партии">Пересортировка</Button>
                    <hr />
                </Fragment>
                :''}
            


            <Form className="form">
                {packs ?
                    <table className="table">
                        <thead>
                            <tr>
                                <th>№ пакета</th>
                                <th>№ смены</th>
                                <th>Имя смены</th>
                                <th>Тип пакета</th>
                                <th>Кол-во</th>
                                <th>Объём</th>
                                <th>Дата</th>
                                <th></th>

                            </tr>
                        </thead>
                        <tbody>
                            {packs && packs.map(function (el, index) {
                                return (

                                    <tr key={index}>
                                        <td>{el.packetNumber}</td>
                                        <td>{el.smenaNum}</td>
                                        <td>{el.smenaName}</td>
                                        <td>{el.isDefected ? "Брак" : "Норма"}</td>
                                        <td>{el.sortedPlanks}/{el.totalPlanks}</td>
                                        <td>{el.totalVolume}</td>
                                        <td>{el.packDate}</td>
                                        <td>
                                            <Button type="button" onClick={() => history.push("/EditResortedPack/" + el.id)} className="btn btn-success" title="Редактировать пакет"><PencilFill /></Button>
                                            <Button type="button" onClick={() => setActivePack(el.id)} className="btn btn-danger" title="Удалить пакет"><Trash /></Button>
                                            <Button type="button" onClick={() => PrintPack(el.id)} className="btn btn-info" title="Распечатать этикетку"><Printer /></Button>
                                            {/* <Button type="button" onClick={() => SetPackScaned(el.id)} className="btn btn-secondary" title="Сделать пакет отсканированным"><UpcScan /></Button> */}
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                    : "Нет пакетов партии"
                }
            </Form>

        </Container>
    )

}

export default ResortedPacksList