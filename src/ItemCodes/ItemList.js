import React, { useState, useEffect } from 'react';
import ItemDataService from "../Services/ItemService";
import { Form, Container, Jumbotron, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom";
import { PencilFill, Trash } from 'react-bootstrap-icons';
import { useSelector } from 'react-redux'
import { Fragment } from 'react';


export default function ItemList() {
    let history = useHistory();
    const [search, setSearch] = useState({ searchtext: "" });
    const user = useSelector(state => state.auth.user)
    const [items, setItems] = useState(null)


    useEffect(() => {
        async function getItems() {
            var response = await ItemDataService.getAll()
            setItems(response);
        }
        getItems();
    }, [])

    async function DeleteItem(id) {
        await ItemDataService.remove(id)
            .then(json => {
                if (json.status === 200) {
                }
                history.push("/");
                history.replace("/ItemList");
            })

    }

    const handleChange = e => {
        const { name, value } = e.target;
        setSearch(prevState => ({ ...prevState, [name]: value }));
    }

    function ImportCSV() {
        ItemDataService.ImportCSV()
            .then(json => {
                console.log("json", json);
                if (json.status === 200) {
                    alert('ITEM-код успешно импортирован');
                }
                else {
                    alert('Не получилось имспортиовать');

                }
                history.push("/");
                history.replace("/ItemList");
            })
            .catch(
                function (error) {
                    alert('Не получилось импортиовать');
                    console.log(error);
                    history.push("/");
                    history.replace("/ItemList");
                }
            )

    }

    function ExportCSV() {
        ItemDataService.ExportCSV()
            .then(json => {
                console.log("json", json);
                if (json.status === 200) {
                    alert('ITEM-коды успешно экспортированы');
                    console.log(json.data.Status);
                }
                else {
                    alert('Не получилось экспортиовать');
                }
            })
            .catch(
                function (error) {
                    console.log(error);
                    debugger;
                }
            )
    }

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Список ITEM-кодов</h1>
            </Jumbotron>

            {user && (user.profile.role === "Admin" || user.profile.role === "Master") ?
                <Fragment>
                    <hr />
                    <div className="row">
                        <div className="col">
                            <Link to={"/AddItems/"} className="btn btn-primary">Добавить новый ITEM-код</Link>
                        </div>

                        <div className="col">
                            {/* <Button color="primary" onClick={ImportCSV} >Импорт</Button> */}
                            <Button color="primary" onClick={ExportCSV} >Экспорт</Button>
                        </div>
                    </div>
                    <hr />
                </Fragment>
            : ''}

            <Form className="form">
                <Form.Group>
                    <Form.Label>Поиск по ITEM-номеру</Form.Label>
                    <Form.Control
                        required
                        type="text"
                        name="searchtext"
                        placeholder="Поиск"
                        onChange={handleChange}
                        value={search.searchtext}
                    />
                </Form.Group>
                {items ?
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Наименование</th>
                                <th>Толщина, мм</th>
                                <th>Ширина, мм</th>
                                <th>Длина, мм</th>
                                <th>Качество</th>
                                <th>Порода</th>
                                <th>Сторона</th>
                                <th>Влажность</th>
                                <th>Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            {items && items.filter(data => {
                                if (search === null) {
                                    return data
                                } else {
                                    if (data.itemName.toLowerCase().includes(search.searchtext.toLowerCase())) return data
                                }
                            }).map(function (el, index) {
                                return (
                                    <tr key={index}>
                                        <td>{el.itemName}</td>
                                        <td>{el.thick}</td>
                                        <td>{el.width}</td>
                                        <td>{el.mixedLength === "Yes" ? "MIX" : el.length}</td>
                                        <td>{el.quality}</td>
                                        <td>{el.breed}</td>
                                        <td>{el.side}</td>
                                        <td>{el.moisture}</td>
                                        <td>
                                            {user && (user.profile.role === "Admin" || user.profile.role === "Master") ?
                                                <Fragment>
                                                    <Button  onClick={() => history.push("/EditItems/" + el.id)} className="btn btn-success"><PencilFill /></Button>
                                                    <Button onClick={() => DeleteItem(el.id)} className="btn btn-danger"><Trash /></Button>
                                                </Fragment>
                                            : ''}
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>

                    </table>
                    : "Нет данных"}

            </Form>




        </Container>
    );


}