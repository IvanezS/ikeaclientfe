import React from 'react';
import { Button, Label, Input, Container, Jumbotron} from 'reactstrap';
import { Form, InputGroup } from 'react-bootstrap';
import ItemDataService from "../Services/ItemService";

class EditItems extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            id : 0,
            thick : 0,
            width : 0,
            length : 0,
            mixedLength : "Yes",
            quality: "B",
            breed: "Pine",
            side: "Center part",
            itemName: '',
            moisture: "Raw",
            mix: true
        }
    }
    
    componentDidMount(){
        debugger
        this.getItem();
    }

    async getItem() {
        var response = await ItemDataService.get(this.props.match.params.id)
        console.log("ITEM = ", response)
        this.setState(response)
    }

    handleChange = (e) => {
        this.setState(
            {[e.target.name]:e.target.value}
        );

    }

    EditItem() {
        debugger
        ItemDataService.update(
            Number(this.props.match.params.id),
            {
                id: Number(this.props.match.params.id),
                itemName: this.state.itemName,
                thick: Number(this.state.thick),
                width: Number(this.state.width),
                length: Number(this.state.length),
                mixedLength: this.state.mixedLength,
                quality: this.state.quality,
                breed: this.state.breed,
                side: this.state.side,
                moisture: this.state.moisture
            }
        )            
        .then(json => {
            console.log("json", json);
            if (json.status === 200) {
                console.log(json.data.Status);
                alert('Data saved successfully');
                this.props.history.push('/ItemList/');
            }
            else {
                alert('Data not saved');
            }
        })
        .catch(
            function (error) {
                console.log(error);
                debugger;
            }
        )
    }

    onDropdownMoistreSelected = (e) => {
        e.preventDefault();
        this.setState({moisture: e.target.value});
        return(this.state.moisture);
    }

    onDropdownQualitySelected = (e) => {
        e.preventDefault();
        this.setState({quality: e.target.value});
        return(this.state.quality);
    }

    onDropdownBreedSelected = (e) => {
        e.preventDefault();
        this.setState({breed: e.target.value});
        return(this.state.breed);
    }

    onDropdownSideSelected = (e) => {
        e.preventDefault();
        this.setState({side: e.target.value});
        return(this.state.side);
    }

    toggleMix = (e) => {
        const { target } = e;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({mix: value});
        this.setState({mixedLength: value ? "Yes" : "No"});
        this.forceUpdate()
    }


    render() {
        return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Редактирование ITEM-кода </h1>
            </Jumbotron>

            
            <Form className="form">

                    <Form.Group>
                        <Label for="thick">Толщина доски, мм</Label>
                        <Input type="number" min = "10" max = "300" name="thick" id="thick" placeholder="Введите ширину доски, мм" value = {this.state.thick} onChange={this.handleChange}/> 
                    </Form.Group>

                    <Form.Group>
                        <Label for="width">Ширина доски, мм</Label>
                        <Input type="number" min = "10" max = "300" name="width" id="width" placeholder="Введите толщину доски, мм" value = {this.state.width} onChange={this.handleChange}/>
                    </Form.Group>

                    <Form.Group>
                        <Label for="length">Длина доски, мм</Label>
                        
                        <InputGroup>
                            <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <Input addon type="checkbox" aria-label="Checkbox for following text input" checked = {this.state.mix} onChange={ this.toggleMix } title ="Смешанная длина в пакете"/>
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <Input type="number" min = "10" max = "6300" name="length" id="length" placeholder="Введите толщину доски, мм" value = {this.state.length} onChange={this.handleChange} disabled = {this.state.mix}/>
                            </InputGroup>
                    
                    </Form.Group>

                    <Form.Group>
                    <Label for="sx">Влажность</Label>
                                            <Form.Control
                                                required
                                                as="select"
                                                name="sx"
                                                placeholder="Выберите схему"
                                                onChange={this.onDropdownMoistreSelected}
                                                size="md"
                                                value={this.state.moisture}
                                                //disabled = {!this.state.settings.checkPlankL}
                                                title ="Выбор схемы нижних слоёв прокладок"
                                                >
                                                <option value = "Raw">Raw</option>
                                                <option value = "TranspDry">TranspDry</option>
                                                <option value = "Dry">Dry</option>

                                            </Form.Control>
                    </Form.Group>
                                    

                    <Form.Group>
                        <Label for="quality">Качество</Label>
                                            <Form.Control
                                                required
                                                as="select"
                                                name="quality"
                                                placeholder="Выберите каество"
                                                onChange={this.onDropdownQualitySelected}
                                                size="md"
                                                value={this.state.quality}
                                                //disabled = {!this.state.settings.checkPlankL}
                                                title ="Выбор качества"
                                                >
                                                <option value = "A">A</option>
                                                <option value = "B">B</option>
                                                <option value = "C">C</option>
                                                <option value = "1-3">1-3</option>
                                                <option value = "1-4">1-4</option>
                                                <option value = "1-5">1-5</option>
                                                <option value = "3-4">3-4</option>
                                                <option value = "3">3</option>
                                                <option value = "4">4</option>
                                                <option value = "5">5</option>
                                                <option value = "SF">SF</option>
                                                <option value = "US5">US5</option>


                                            </Form.Control>
                    </Form.Group>

                    <Form.Group>
                        <Label for="side">Сторона</Label>
                        <                   Form.Control
                                                required
                                                as="select"
                                                name="side"
                                                placeholder="Выберите сторону"
                                                onChange={this.onDropdownSideSelected}
                                                size="md"
                                                value={this.state.side}
                                                //disabled = {!this.state.settings.checkPlankL}
                                                title ="Выбор стороны"
                                                >
                                                <option value = "Center part">Center part</option>
                                                <option value = "Side board">Side board</option>



                                            </Form.Control>
                    </Form.Group>

                    <Form.Group>
                        <Label for="breed">Порода</Label>

                        <                   Form.Control
                                                required
                                                as="select"
                                                name="breed"
                                                placeholder="Выберите породу"
                                                onChange={this.onDropdownBreedSelected}
                                                size="md"
                                                value={this.state.breed}
                                                //disabled = {!this.state.settings.checkPlankL}
                                                title ="Выбор породы"
                                                >
                                                <option value = "Pine">Pine</option>
                                                <option value = "Spruce">Spruce</option>
                                                <option value = "Birch">Birch</option>
                                                <option value = "Cedar">Cedar</option>


                                            </Form.Control>
                    </Form.Group>

                    <Form.Group>
                        <Label for="itemName">ITEM-код</Label>
                        <Input type="text"  name="itemName" id="itemName" placeholder="ITEM-код" value = {this.state.itemName}  onChange={this.handleChange} required/>
                    </Form.Group>

                    

            </Form>

                     <Button type="button" onClick={this.EditItem.bind(this)} className="btn btn-secondary">Редактировать ITEM-код</Button>

   
        </Container>
        )
    }

}

export default EditItems