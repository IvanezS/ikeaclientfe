import { useSelector } from 'react-redux'

function User() {
  const user = useSelector(state => state.auth.user)
  return user && user.profile.name
}

function Role() {
  const user = useSelector(state => state.auth.user)
  return user && user.profile.name
}

export default {User, Role}
