import { useSelector } from 'react-redux'
import React from 'react'
import AddProviders from '../Providers/AddProviders';
import EditProvider from '../Providers/EditProvider';
import ProviderList from '../Providers/ProviderList';

import AddParties from '../Parties/AddParties';
import AddItemPart from '../Parties/AddItemPart';
import EditPart from '../Parties/EditPart';
import PartList from '../Parties/PartList';
import MainPartList from '../MainParties/MainPartList';
import ResortPart from '../Parties/ResortPart';

import PackList from '../Packs/PackList';
import AddPack from '../Packs/AddPack';
import EditPack from '../Packs/EditPack';

import AddGaskets from '../Gaskets/AddGaskets';
import GasketList from '../Gaskets/GasketList';
import EditGaskets from '../Gaskets/EditGaskets';

import ItemList from '../ItemCodes/ItemList';
import AddItems from '../ItemCodes/AddItems';
import EditItems from '../ItemCodes/EditItems';

import AddResortedPack from '../ResortedPacks/AddResortedPack';
import EditResortedPack from '../ResortedPacks/EditResortedPack';
import ResortedPacksList from '../ResortedPacks/ResortedPacksList';

import Resorting from '../Resorting/Resorting';

import Reports from '../Reports/Reports';

import Settings from '../Settings/Settings';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Navbar, Nav, NavDropdown, Form, Container } from 'react-bootstrap'
import { LinkContainer } from "react-router-bootstrap";

import SecondPanel from '../SecondPanel/SecondPanel'

import SigninOidc from '../pages/signin-oidc'
import SignoutOidc from '../pages/signout-oidc'
import Home from '../pages/home'
import Login from '../pages/login'

import PrivateRoute from './protectedRoute'
import UserInfo from './getUser'


function Navigation() {

    const user = useSelector(state => state.auth.user)

    return (

        <Router>

            <Container fluid>

                <Container fluid>
                    <Navbar bg="light" variant="light" expand="lg" sticky="top">
                        <Navbar.Brand>Икея Вятка</Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">

                            {user && user.profile.role === "Admin" ? 
                                    <LinkContainer to="/ProviderList">
                                        <NavDropdown.Item>Поставщики</NavDropdown.Item>
                                    </LinkContainer>
                            : ''}

                            {user && (user.profile.role === "Admin" || user.profile.role === "Oper" || user.profile.role === "Master") ? 
                               <LinkContainer to="/MainPartList">
                                    <NavDropdown.Item>Партии</NavDropdown.Item>
                                </LinkContainer>
                            : ''}
                            {user && (user.profile.role === "Admin" || user.profile.role === "Oper" ) ? 
                                <LinkContainer to="/GasketList">
                                    <NavDropdown.Item>Прокладки</NavDropdown.Item>
                                </LinkContainer>
                            : ''}
                            {user && (user.profile.role === "Admin"  || user.profile.role === "Oper" || user.profile.role === "Master") ? 
                                <LinkContainer to="/ItemList">
                                    <NavDropdown.Item>ITEM-коды</NavDropdown.Item>
                                </LinkContainer>
                            : ''}
                            {user && (user.profile.role === "Admin"  ) ? 
                                <LinkContainer to="/Reports">
                                    <NavDropdown.Item>Отчёт</NavDropdown.Item>
                                </LinkContainer>
                            : ''} 
                            {/* {user && (user.profile.role === "Admin" || user.profile.role === "Oper" ) ? 
                                <LinkContainer to="/ResortedDryPacksList">
                                    <NavDropdown.Item>Сухие пакеты</NavDropdown.Item>
                                </LinkContainer>
                            : ''} */}
                            {user && (user.profile.role === "Admin" || user.profile.role === "Oper" ) ? 
                                <LinkContainer to="/Resorting">
                                    <NavDropdown.Item>Перештабелирование</NavDropdown.Item>
                                </LinkContainer>
                            : ''}
                            </Nav>

                            <Nav>
                                <Form inline><LinkContainer to="/login">
                                    <Nav.Item><UserInfo.User /></Nav.Item></LinkContainer>
                                </Form>
                            </Nav>

                        </Navbar.Collapse>

                    </Navbar>

                    <SecondPanel />

                </Container>

                <Switch>

                    <PrivateRoute path='/AddProviders' component={AddProviders}></PrivateRoute>
                    <PrivateRoute path='/EditProvider/:id' component={EditProvider}></PrivateRoute>
                    <PrivateRoute path='/ProviderList' component={ProviderList}></PrivateRoute>

                    <PrivateRoute path='/AddItemPart/:id' component={AddItemPart}></PrivateRoute>
                    <PrivateRoute path='/AddParties' component={AddParties}></PrivateRoute>
                    <PrivateRoute path='/EditPart/:id' component={EditPart}></PrivateRoute>
                    <PrivateRoute path='/PartList' component={PartList}></PrivateRoute>
                    <PrivateRoute path='/MainPartList' component={MainPartList}></PrivateRoute>



                    <PrivateRoute path='/Reports' component={Reports}></PrivateRoute>
                    

                    <PrivateRoute path='/EditPartPacks/:id' component={PackList}></PrivateRoute>
                    <PrivateRoute path='/AddPack/:id' component={AddPack}></PrivateRoute>
                    <PrivateRoute path='/EditPack/:id' component={EditPack}></PrivateRoute>
                    <PrivateRoute path='/ResortPart/:id' component={ResortPart}></PrivateRoute>

                    <PrivateRoute path='/GasketList' component={GasketList}></PrivateRoute>
                    <PrivateRoute path='/AddGaskets' component={AddGaskets}></PrivateRoute>
                    <PrivateRoute path='/EditGaskets/:id' component={EditGaskets}></PrivateRoute>

                    <PrivateRoute path='/ItemList' component={ItemList}></PrivateRoute>
                    <PrivateRoute path='/AddItems' component={AddItems}></PrivateRoute>
                    <PrivateRoute path='/EditItems/:id' component={EditItems}></PrivateRoute>

                    <PrivateRoute path='/AddResortedPack/:id' component={AddResortedPack}></PrivateRoute>
                    <PrivateRoute path='/EditResortedPack/:id' component={EditResortedPack}></PrivateRoute>
                    <PrivateRoute path='/ResortedPacksList/:id' component={ResortedPacksList}></PrivateRoute>

                    <PrivateRoute path='/Resorting/' component={Resorting}></PrivateRoute>

                    <PrivateRoute path='/Settings' component={Settings}></PrivateRoute>

                    <Route path="/login" component={Login} />
                    <Route path="/signout-oidc" component={SignoutOidc} />
                    <Route path="/signin-oidc" component={SigninOidc} />
                    <Route path="/Home" component={Home} />
                    <PrivateRoute exact path="/" component={Home} />

                </Switch>



            </Container >


        </Router>

    )
}

export default Navigation