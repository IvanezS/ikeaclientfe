import React, { useState, useEffect } from 'react';
import { Form, Container, Jumbotron, Button, Row, Col, Card, ButtonGroup, Table } from 'react-bootstrap';
import MainPartDataService from "../Services/MainPartService";
import SmenaService from "../Services/SmenaService";
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker'
import moment from "moment"

function MainPartList() {

    const [parties, setParties] = useState(null)
    const [packetsF, setPacketsF] = useState(null)
    const [smenas, setSmenas] = useState(null)
    const [smenaF, setSmenasF] = useState(null)
    const [search, setSearch] = useState({ searchtextMainIDpart: "", searchtextIDpart: "", searchtextW: "", searchtextSmenaNum: "" });
    const [searchChanged, setSearchChanged] = useState(false)
    const [sum, setSum] = useState(null)
    const [dateChanged, setDateChanged] = useState(false)
    let now = new Date();
    let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
    let end = moment(start).add(1, "days").subtract(1, "seconds");
    const [DT, setDT] = useState({
        start: start,
        end: end
    })

    function getSmenasButton() {
        var filteredSmenas = smenas && smenas.filter(data=>{
            if ((DT.start <= moment(data.dateTimeStart, "YYYY-MM-DD HH:mm:ss") && (moment(data.dateTimeStop, "YYYY-MM-DD HH:mm:ss") <= DT.end)))return data
        })

        var butt = filteredSmenas && filteredSmenas.map(function (el, i) {
            return (
                <Button key={i} type="button" className="btn btn-light btn-block" onClick={() => SetSearchSmenaNum(el.smenaNum)}>№{el.smenaNum}-{el.smenaName}: {el.surname}</Button>
            )
        })
        setDateChanged(false)
        setSmenasF(() => butt)
        
    }


    function applyCallback(startDate, endDate) {
        debugger
        setDT({
            start: startDate,
            end: endDate
        })
        FilterResortedPackets();
        getSmenasButton();
        setSearchChanged(true)
        setDateChanged(true)
    }

    useEffect(() => {
        getParties();
        getSmenas();
        FilterResortedPackets();
    }, [])

    useEffect(() => {
        getSmenasButton();
    }, [dateChanged])

    useEffect(() => {
        FilterResortedPackets();
    }, [searchChanged])

    useEffect(() => {
        FilterResortedPackets();
        getSmenas();
    }, [parties])

    useEffect(() => {
        getSmenasButton();
    }, [smenas])

    async function getParties() {
        const partie = await MainPartDataService.getAll()
        setParties(partie);
        console.log(partie, 'parties');
    }

    async function getSmenas() {
        const smen = await SmenaService.getAll()
        setSmenas(smen);
        console.log(smen, 'smenas');
    }

    async function handleChangeSearch(e) {
        const { name, value } = e.target;
        await setSearch(prevState => ({ ...prevState, [name]: value }))
        console.log(search, "search")
        setSearchChanged(true)
    }

    function SetSearchSmenaNum(num) {
        setSearch(prevState => ({ ...prevState, searchtextSmenaNum: num }))
        setSearchChanged(true)
    }

    function FilterResortedPackets() {
debugger

        var filteredMainParts = parties && parties.filter(data => {
            if (search.searchtextMainIDpart === "" || search.searchtextMainIDpart === data.mainPartNumber) return data
        })
        var itemsOfFilteredMainParts = filteredMainParts && filteredMainParts.reduce((prev, current) => prev.concat(current.itemParts), [])
        var filteredParts = itemsOfFilteredMainParts && itemsOfFilteredMainParts.filter(data => {
            if (search.searchtextIDpart === "" || search.searchtextIDpart === data.partNumber) return data
        })
        var ResortedPackets = filteredParts && filteredParts.reduce((prev, current) => prev.concat(current.resortedPackets), [])

        var filteredPacketsBySmenaNum = ResortedPackets && ResortedPackets.filter(data => {
            if (search.searchtextSmenaNum === "" || Number(search.searchtextSmenaNum) === data.smenaNum) return data
        })


        var filteredPacketsByDate = filteredPacketsBySmenaNum && filteredPacketsBySmenaNum.filter(data => {
            if ((DT.start <= moment(data.packDate, "YYYY-MM-DD HH:mm:ss")) && (moment(data.packDate, "YYYY-MM-DD HH:mm:ss") <= DT.end)) return data
        })

        filteredPacketsByDate && filteredPacketsByDate.sort(function (a, b) {
            if (a.packDate < b.packDate) {
              return 1;
            }
            if (a.packDate > b.packDate) {
              return -1;
            }
            // a должно быть равным b
            return 0;
          });

        var rows = filteredPacketsByDate && filteredPacketsByDate.map(function (ol, index) {

            return (
                <tr key={index}>
                    <td>{ol.packetNumber}</td>
                    <td>{ol.smenaNum}</td>
                    <td>{getPartNameById(filteredParts, ol.partId)}</td>
                    <td>{ol.isDefected ? "NOK" : "OK"}</td>
                    <td>{getItemNameById(filteredParts, ol.partId)}</td>
                    <td>{ol.totalPlanks}</td>
                    <td>{ol.totalVolume.toFixed(3)}</td>
                    <td>{moment(ol.packDate, "YYYY-MM-DD HH:mm:ss").format("YYYY-MM-DD HH:mm:ss")}</td>
                </tr>
            )
        })

        setSearchChanged(false)
        setPacketsF(() => rows)

        var VolumePackets = filteredPacketsByDate && filteredPacketsByDate.reduce((total, currentValue) => total = total + currentValue.totalVolume, 0);
        var QuantPlanks = filteredPacketsByDate && filteredPacketsByDate.reduce((total, currentValue) => total = total + currentValue.totalPlanks, 0);
        var QuantPackets =  filteredPacketsByDate && filteredPacketsByDate.length;       
        var smena = GetSmenaByNum(Number(search.searchtextSmenaNum))

        var Summary = 
            <Card>
                <Card.Header>
                    ИТОГИ [{DT.start.format("YYYY-MM-DD HH:mm:ss")} ... {DT.end.format("YYYY-MM-DD HH:mm:ss")}]
                </Card.Header>
                <Card.Body>
                    {search.searchtextSmenaNum === "" ? <p><b>Не выбрана смена</b></p> :  <p><b>Выбрана смена №{search.searchtextSmenaNum}:</b> Имя смены: {smena.smenaName}, Имя оператора: {smena.surname}, Время: [{moment(smena.dateTimeStart).format("YYYY-MM-DD HH:mm:ss") + " ... " + moment(smena.dateTimeStop).format("YYYY-MM-DD HH:mm:ss")}]</p>}

                <p>Кол-во пакетов: <b>{QuantPackets} шт</b>, Объём пакетов: <b>{VolumePackets && VolumePackets.toFixed(3)} м3</b>, Кол-во досок в пакетах: <b>{QuantPlanks} шт</b></p>
                </Card.Body>
            </Card>


        
        setSum(() => Summary)





    }
    function GetSmenaByNum(num) {
        return smenas && smenas.find(data =>{
            if(Number(data.smenaNum) === num) return data
        })
    }

    function getPartNameById(filteredParts, id) {

        var filteredPart = filteredParts && filteredParts.find(data => {
            if (id === data.id) return data
        })
        return filteredPart.partNumber
    }

    function getItemNameById(filteredParts, id) {
        var filteredPart = filteredParts && filteredParts.find(data => {
            if (id === data.id) return data
        })
        var l = filteredPart.item.mixedLength === "Yes" ? "MIX" : filteredPart.item.length
        var si = filteredPart.item.itemName + ": " + filteredPart.item.thick + "x" + filteredPart.item.width + "x" + l + "," + filteredPart.item.quality + "," + filteredPart.item.breed + "," + filteredPart.item.moisture
        return si
    }

    function GetPicker() {
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
        let ranges = {
            "Today Only": [moment(start), moment(end)],
            "Yesterday Only": [moment(start).subtract(1, "days"), moment(end).subtract(1, "days")],
            "3 Days": [moment(start).subtract(3, "days"), moment(end)]
        }
        let local = {
            "format": "DD-MM-YYYY HH:mm",
            "sundayFirst": false
        }
        let maxDate = moment(start).add(24, "hour")
        return (
            <Form.Group as={Col}>
                <DateTimeRangeContainer
                    ranges={ranges}
                    start={DT.start}
                    end={DT.end}
                    local={local}
                    maxDate={maxDate}
                    applyCallback={applyCallback}
                >
                    
                        <Form.Label>Поиск по времени</Form.Label>
                        <Form.Control
                            id="formControlsTextB"
                            type="text"
                            placeholder="Выберите диапазон времени"
                            value = {DT.start.format("YYYY-MM-DD HH:mm:ss")+" ... "+ DT.end.format("YYYY-MM-DD HH:mm:ss")}
                        />
                    
                </DateTimeRangeContainer>
            </Form.Group>
        );
    }

    return (

        <Container fluid className="App">
            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Информация по пересортированным пакетам</h1>
            </Jumbotron>

            <Form >

                <Form.Row>

                    <Form.Group as={Col}>
                        <Form.Label>Поиск по ID грузовой единице</Form.Label>
                        <Form.Control
                            type="text"
                            name="searchtextMainIDpart"
                            placeholder="Поиск"
                            onChange={handleChangeSearch}
                            value={search.searchtextMainIDpart}
                        />
                    </Form.Group>

                    <Form.Group as={Col}>
                        <Form.Label>Поиск по ID партии</Form.Label>
                        <Form.Control
                            type="text"
                            name="searchtextIDpart"
                            placeholder="Поиск"
                            onChange={handleChangeSearch}
                            value={search.searchtextIDpart}
                        />
                    </Form.Group>



                    {GetPicker()}



                </Form.Row>
            </Form>


            {sum}


            <Row>
                <Col sm="3">
                    <Card>
                        <Card.Header>
                            Смены
                        </Card.Header>


                        <Card.Body fluid style={{ height: "400px" }} className="overflow-auto">
                            {/* <Container> */}
                            <ButtonGroup vertical className="btn-block">
                                <Button type="button" className="btn btn-light btn-block" onClick={() => SetSearchSmenaNum("")}>Без смены</Button>
                                {smenaF}
                            </ButtonGroup>
                            {/* </Container> */}
                        </Card.Body>
                    </Card>
                </Col>
                <Col>
                    <Card>
                        <Card.Header>
                            Список пересортированных пакетов
                        </Card.Header>
                        <Card.Body fluid style={{ height: "400px" }} className="overflow-auto">

                            <Table striped bordered hover size="sm">
                                <thead>
                                    <tr>
                                        <th>№ пакета</th>
                                        <th>№ смены</th>
                                        <th>ID партии</th>
                                        <th>Тип пакета</th>
                                        <th>ITEM</th>
                                        <th>Кол-во</th>
                                        <th>Объём</th>
                                        <th>Дата</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {packetsF}
                                </tbody>
                            </Table>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>

        </Container>
    )

}
export default MainPartList