import React, { useState } from 'react';
import GasketDataService from "../Services/GasketsService";
import { Button, Form, Container, Jumbotron, Modal } from 'react-bootstrap';
import { useHistory } from "react-router-dom";

function AddGaskets() {

    const [modalShow, setModalShow] = useState(false);
    const [validated, setValidated] = useState(false);
    const [gasket, setGasket] = useState({name: "", sxema: 0})
    let history = useHistory();
    const items = [];
    const items2 = [];
    const CB_Array = [];

    async function createGasket() {
        debugger;
        var response = await GasketDataService.create(

            {
                id: 0,
                name: gasket.name,
                sxema: gasket.sxema
            }
        )
        return response

    }

    const handleChange = e => {
        const { name, value } = e.target;
        setGasket(prevState => ({ ...prevState, [name]: value }));
    }

    const toggleChange = (e) => {
        debugger
        console.log("this.state.CB_Array = ", CB_Array)
        const { target } = e;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        //const { name } = target;
        
        CB_Array[parseInt(target.id) - 1] = value;


        var num=0;
        CB_Array.map(function (res, i) {
            if (res) num = num + Math.pow(2, Number(20-i));
        });
        
        console.log("num = ", num)

        setGasket(prevState => ({ ...prevState, sxema: num }));

        
    };

    
    
        function getCH1() 
        {
            let num = 0.0;
                for(let i = 0; i < 21; i++) {
                    items.push(<th key = {i}>{ num.toFixed(1)}</th>)
                    num = num + 0.3;
                }
    
            return items
        }
    
        function getCH2() 
        {
            if (gasket) {
    
                for(let i = 0; i < 21; i++) {
    
                    gasket.sxema & Math.pow(2, Number(20-i)) ? CB_Array[i] = true : CB_Array[i] = false;
                    items2.push(<td key={i}>{ 
                                        <Form.Group key={i}>
                                            <Form.Check type="checkbox" id = {i+1} checked={CB_Array[i]} onChange={toggleChange}  />
                                        </Form.Group>
                    }</td>)
    
                }
            }
            return items2
        }






    const handleSubmit = async (event) => {
        debugger

        console.log("event", event);

        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();

        }

        setValidated(true);

        if (gasket.name.length > 0) {
            var response = await createGasket();
            if (response.status === 200) {
                redir();
            } else {
                setModalShow(true)
            }
        }

    }

        function redir() {
            history.push("/");
            history.replace("/GasketList/");
        }


        function ErrorModal(props) {
            return (
                <Modal
                    {...props}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">
                            ВНИМАНИЕ!
                  </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h4>Не получилось создать схему укладок прокладок</h4>
                        <p>
                            Проверьте задаваемые параметры, убедитесь в наличии связи
                  </p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={props.onHide}>Ok</Button>
                    </Modal.Footer>
                </Modal>
            );
        }

        return (
            <Container fluid className="App">

                <Jumbotron fluid className="vertical-center justify-content-center">
                    <h1 className="display-4">Создать схему укладки прокладок</h1>
                </Jumbotron>

                <ErrorModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

                <Form noValidate validated={validated} onSubmit={handleSubmit}>

                    <Form.Group>
                        <Form.Label>Имя схемы</Form.Label>
                        <Form.Control required type="text" name="name" placeholder="Введите имя схемы" value={gasket && gasket.name} onChange={handleChange} />
                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                        Введите имя схемы
                        </Form.Control.Feedback>
                    </Form.Group>

                    <table className="table table-striped">
                        <thead>
                            <tr>
                                { getCH1() }
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                { getCH2() }
                            </tr>
                        </tbody>
                    </table>
                </Form>


                <Button type="button" className="btn btn-primary btn-block" onClick={handleSubmit}>Создать схему</Button>
                <Button type="button" className="btn btn-secondary btn-block" onClick={() => history.goBack()}>Назад</Button>

            </Container>
        );
    

}

export default AddGaskets