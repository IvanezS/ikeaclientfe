import React, { useState, useEffect } from 'react';
import GasketDataService from "../Services/GasketsService";
import { Form, Container, Jumbotron, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom";
import { PencilFill, Trash } from 'react-bootstrap-icons';

function GasketList () {

    let history = useHistory();
    const [gaskets, setGaskets] = useState(null)
    const [CBh, setCBh] = useState(null)
    const [CB, setCB] = useState(null)

    useEffect(() => {
        getGaskets();
    }, [])

    useEffect(() => {
        GetComboBoxes();
    }, [gaskets])

    async function getGaskets() {
        var response = await GasketDataService.getAll()
        setGaskets(response);
    }

        async function DeleteGaskets (id) {
            var response = await GasketDataService.remove(id)
            
                if (response.status === 200) {
                    alert('Схема успешно удалёна');
                }else{
                    alert('Схема не удалёна'); 
                }
                history.push("/");
                history.replace("/GasketList");
            
        }



        function ImportCSV() {
            GasketDataService.ImportCSV()
                .then(json => {
                    console.log("json", json);
                    if (json.status === 200) {
                        alert('Таблица прокладок успешно импортирована');
                    }
                    else {
                        alert('Не получилось имспортиовать');
    
                    }
                    history.push("/");
                    history.replace("/GasketList");
                })
                .catch(
                    function (error) {
                        console.log(error);
                        history.push("/");
                        history.replace("/GasketList");
                    }

                )
                
        }
    
        function ExportCSV() {
            GasketDataService.ExportCSV()
                .then(json => {
                    console.log("json", json);
                    if (json.status === 200) {
                        alert('Таблица прокладок успешно экспортирована');
                        console.log(json.data.Status);
                    }
                    else {
                        alert('Не получилось экспортиовать');
                    }
                })
                .catch(
                    function (error) {
                        console.log(error);
                        debugger;
                    }
                )
        }


        function GetComboBoxes() {


            var Combo = gaskets && gaskets.map(function(el, i) {
                var items = [];
                for(let i = 0; i < 21; i++) {
                    items.push(<td key={i}>{ el.sxema & Math.pow(2, Number(20-i)) ? <input type="checkbox" checked={true} readOnly /> : <input type="checkbox" checked={false} readOnly />}</td>)
                }
                return(
                    <tr>
                        <td>{el.name}</td>
                        {items}
                        <td>
                            <Link to = {"/EditGaskets/" + el.id} className="btn btn-success" title = "Изменить"><PencilFill/></Link>
                            <button type="button" onClick={()=>DeleteGaskets(el.id)} className="btn btn-danger" title = "Удалить"><Trash/></button>
                        </td>
                    </tr>
                )
            })
            setCB(()=>Combo)
   
        }

        function getCH1() 
        {
            var items = [];
            let num = 0.0;
                for(let i = 0; i < 21; i++) {
                    items.push(<th key = {i}>{ num.toFixed(1)}</th>)
                    num = num + 0.3;
                }
    
            return items
        }
    
        return (
            <Container fluid className="App">

                <Jumbotron fluid className="vertical-center justify-content-center">
                    <h1 className="display-4">Схемы укладки прокладок</h1>
                </Jumbotron>

                    <hr/>
                    
                    <Row>
                        <Col>
                            <Link to={"/Addgaskets/"} className="btn btn-primary">Добавить новую схему</Link>
                        </Col>

                        <Col>
                            {/* <Button color="primary" onClick={ImportCSV} >Импорт</Button> */}
                            {gaskets ? <Button color="primary" onClick={ExportCSV} >Экспорт</Button> : " "}
                        </Col>
                    </Row>


                    <hr />

                <Form className="form">
                {gaskets ?
                    <table className="table table-striped">
                        
                        <thead>
                            <tr>
                                <th>Имя</th>
                                { getCH1() }
                                <th>Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            {CB}
                        </tbody>
                        

                    </table>
                    : "Нет схем прокладок"}
                </Form>

            </Container>
        );
    

}

export default GasketList