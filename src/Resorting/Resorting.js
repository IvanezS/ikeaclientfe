import React, { useState, useEffect, useRef } from 'react';
import { Form, ButtonGroup, Row, Col, Card, ProgressBar, Container, Jumbotron, Button, ButtonToolbar, FormControl, Modal } from 'react-bootstrap';
import ItemDataService from "../Services/ItemService";
import PlcDataService from "../Services/PlcDBService";
import GasketDataService from "../Services/GasketsService";
import { CloudDownload, CloudUpload, StopCircle, Truck, XCircle, BoxArrowUpRight, BagPlus, BoxArrowInDownLeft } from 'react-bootstrap-icons';
import { useHistory, useParams } from "react-router-dom";
import axios from 'axios'

function Resorting() {

    //const user = useSelector(state => state.auth.user)
    const flagUpdate = useRef(true);
    const flagItemPresent = useRef(true);
    let param = useParams();
    let history = useHistory();
    const [validated, setValidated] = useState(false);
    const [, setIntervalId] = useState();
    const [modalShow, setModalShow] = useState(false);
    const [items, setItems] = useState(null);
    const [itemsF, setItemsF] = useState(null)
    const [modalReadShow, setModalReadShow] = useState(false);
    const [gaskets, setGaskets] = useState(
        [
            {
                id: 0,
                name: "",
                sxema: 0
            }
        ]
    );

    const [part, setPart] = useState(null);
    const [search, setSearch] = useState({ searchtextT: "", searchtextW: "" });
    const [searchChanged, setSearchChanged] = useState(false)
    const [settings, setSettings] = useState({
        startNewPack: false,
        packResortedReaded: false,
        finishPackResorting: false,
        enable2Sxema: false,
        enableRolling: false,
        settingsWasApplyied: false,
        checkPlankL: false,
        finishDefectPack: false,
        defectPacketPushed: false,
        packDefectReaded: false,
        packParamChanged: false,
        packToPack: false,
        qLayersInPack: 0,
        vPlanksInPack: 0,
        qPlanksInLayer: 0,
        qPlanksInLayerBrak: 0,
        qpLankLayersBrak: 0,
        sxema: 0,
        sxemaNum: 0,
        plankW: 0,
        plankT: 0,
        sxema2: 0,
        sxema2Qlayers: 0,
        packL: 0,
        partId: 0,
        plankL: 0,
        itemId: -1,
        setNumLayer: 0,
        setNumPlankInLayer: 0,
        generalSpeed: 50,
        totalPlanksInPack: 0,
        setNumPlankInLayerBrak: 0,
        checkL: 0,
        sxemaId: 0,
        sxema2Id: 0,
        rpackNumber: 0,
        smenaNum: 0,
        smenaName: "",
        gasketsLayerPeriod: 0
    });

    const [getPlc, setPlcDB] = useState({
        startNewPack: false,
        packResortedReaded: false,
        finishPackResorting: false,
        enable2Sxema: false,
        enableRolling: false,
        settingsWasApplyied: false,
        checkPlankL: false,
        finishDefectPack: false,
        defectPacketPushed: false,
        packDefectReaded: false,
        packParamChanged: false,
        packToPack: false,
        qLayersInPack: 0,
        vPlanksInPack: 0,
        qPlanksInLayer: 0,
        qPlanksInLayerBrak: 0,
        qpLankLayersBrak: 0,
        sxema: 0,
        sxemaNum: 0,
        plankW: 0,
        plankT: 0,
        sxema2: 0,
        sxema2Qlayers: 0,
        packL: 0,
        partId: 0,
        plankL: 0,
        itemId: 0,
        setNumLayer: 0,
        setNumPlankInLayer: 0,
        generalSpeed: 50,
        totalPlanksInPack: 0,
        setNumPlankInLayerBrak: 0,
        checkL: 0,
        lineInWork: false,
        packResorting: false,
        packResorted: false,
        rdyStartNewPack: false,
        rdyStart: false,
        defectPacketResorted: false,
        numLayerActual: 0,
        numPlankInLayerActual: 0,
        qPlanksOk: 0,
        qPlanksNotOk: 0,
        vPlanksOk: 0,
        qPlankBrak: 0,
        gasketsLayerPeriod: 0
    });

    const plankLarr =
        [
            { value: 1800 },
            { value: 2100 },
            { value: 2400 },
            { value: 2700 },
            { value: 3000 },
            { value: 3300 },
            { value: 3600 },
            { value: 3900 },
            { value: 4200 },
            { value: 4500 },
            { value: 4800 },
            { value: 5100 },
            { value: 5400 },
            { value: 5700 },
            { value: 6000 }
        ];
    var partItems = [];


    useEffect(() => {

        axios.all([
            ItemDataService.getAll(),
            GasketDataService.getAll(),
            PlcDataService.getPlcSetDB()
        ])
            .then(axios.spread((obj2, obj3, obj4) => {
                console.log("Items data", obj2);
                setItems(obj2);
                console.log("Gaslets data", obj3);
                setGaskets(obj3);
                console.log("Settings data", obj4);
                setSettings(obj4);

            }));


        async function getPlcDBfunc() {
            if (flagUpdate.current) {
                var response = await PlcDataService.getPlcDB();
                setPlcDB(response);
            }
        }

        const id = setInterval(() => getPlcDBfunc(), 1000);
        setIntervalId(id);
        return () => clearInterval(id);

    }, [])

    useEffect(() => {
        SearchItem();
    }, [items])

    useEffect(() => {
        SearchItem();
    }, [searchChanged])



    function SearchItem() {

        var filteredSearchItem = items && items.filter(data => {
            if (search.searchtextT === "" && search.searchtextW === "") return data
            if (search.searchtextT !== "" && search.searchtextW !== "" && data.thick === Number(search.searchtextT) && data.width === Number(search.searchtextW)) return data
            if (search.searchtextT !== "" && search.searchtextW === "" && data.thick === Number(search.searchtextT)) return data
            if (search.searchtextW !== "" && search.searchtextT === "" && data.width === Number(search.searchtextW)) return data

        })

        var combo = filteredSearchItem && filteredSearchItem.map(function (ol, index) {

            return (
                <option key={index} value={ol.id}>
                    {ol.itemName}:   {ol.thick}x{ol.width}x{ol.mixedLength === "Yes" ? "MIX" : ol.length };   {ol.quality}; {ol.breed}; {ol.moisture}
                </option>
            )
        })
        setSearchChanged(false)
        setItemsF(() => combo)
    }
    async function handleChangeSearch(e) {
        const { name, value } = e.target;
        await setSearch(prevState => ({ ...prevState, [name]: value }))
        console.log(search, "search")
        setSearchChanged(true)
    }

    const handleSettingsChange = e => {
        debugger
        const { name, value } = e.target;
        setSettings(prevState => ({ ...prevState, [name]: value }));

        if (name === "sxemaId") {
            var sx = gaskets.filter(data => {
                if (data.id === Number(value)) return data
            })
            var sxS = sx[0].sxema;
            setSettings(prevState => ({ ...prevState, sxema: sxS }))
        }

        if (name === "sxema2Id") {
            var sx = gaskets.filter(data => {
                if (data.id === Number(value)) return data
            })
            var sxS = sx[0].sxema;
            setSettings(prevState => ({ ...prevState, sxema2: sxS }))
        }

        
        if (name === "itemId") {
            var selItem = items && items.find(data => {if (data.id === Number(value)) return data})
            setSettings(prevState => ({ ...prevState, plankT: selItem.thick, plankW: selItem.width }))
        }

    }

    async function setSetPlcDBfunc() {
        debugger
        var response = await PlcDataService.setPlcDB(
            {
                startNewPack: settings.startNewPack,
                packResortedReaded: settings.packResortedReaded,
                finishPackResorting: settings.finishPackResorting,
                enable2Sxema: settings.enable2Sxema,
                enableRolling: settings.enableRolling,
                checkPlankL: settings.checkPlankL,
                finishDefectPack: settings.finishDefectPack,
                defectPacketPushed: settings.defectPacketPushed,
                packToPack : settings.packToPack,
                qLayersInPack: Number(settings.qLayersInPack),
                vPlanksInPack: Number(settings.vPlanksInPack),
                qPlanksInLayer: Number(settings.qPlanksInLayer),
                qPlanksInLayerBrak: Number(settings.qPlanksInLayerBrak),
                qpLankLayersBrak: Number(settings.qpLankLayersBrak),
                sxema: settings.sxema,
                sxemaNum: 0,
                plankW: Number(settings.plankW),
                plankT: Number(settings.plankT),
                sxema2: settings.sxema2,
                sxema2Qlayers: Number(settings.sxema2Qlayers),
                sxemaId: Number(settings.sxemaId),
                sxema2Id: Number(settings.sxema2Id),
                packL: Number(settings.packL),
                partId: -1,
                plankL: Number(settings.plankL),
                itemId: Number(settings.itemId),
                rpackNumber: Number(settings.rpackNumber),
                generalSpeed: Number(settings.generalSpeed),
                totalPlanksInPack: Number(settings.totalPlanksInPack),
                checkL: Number(settings.checkL),
                gasketsLayerPeriod : Number(settings.gasketsLayerPeriod)
            });
        flagUpdate.current = true
        setModalShow(false);
        console.log(response.status);
        if (response.status === 200 || response.status === 204) {
            alert('Данные успешно записались') 
        }
        else{
            alert('Данные не записались в ПЛК!!!');
        }
    }

    async function FinishDefectedPack() {
        var response = await PlcDataService.finishDefectPack()
        if (response.status === 200) {
            console.log(response.status);
        }
        else {
            alert('Data not saved');
        }
    }

    async function DefectPacketRemoved() {
        var response = await PlcDataService.defectPackPushed()
        if (response.status === 200) {
            console.log(response.status);
        }
        else {
            alert('Data not saved');
        }
    }

    async function ChangePackParamBrak() {
        var response = await PlcDataService.ChangePackParamBrak(settings && settings.setNumPlankInLayerBrak)
        if (response.status === 200) {
            console.log(response.status);
        }
        else {
            alert('Data not saved');
        }
    }

    async function finishPack() {
        var response = await PlcDataService.finishPack()
        if (response.status === 200) {
            console.log(response.status);
        }
        else {
            alert('Data not saved');
        }
    }

    async function ReadSettingsFromPlc() {
        var response = await PlcDataService.ReadSettingsFromPlc()
        console.log(response);
        if (response.status === 200) {
            //window.location.reload();
        }
        else {
            alert('Data not saved');
        }
    }

    
    async function GasketsBrakPut() {
        var response = await PlcDataService.GasketsBrakPut()
        console.log(response);
        if (response.status === 200) {
            //window.location.reload();
        }
        else {
            alert('Data not saved');
        }
    }


    async function ChangeSpeed() {
        debugger
        var response = await PlcDataService.SetSpeed(settings && settings.generalSpeed)
        console.log(response);
        if (response.status === 200) {
            //window.location.reload();
        }
        else {
            alert('Data not saved');
        }
    }


    async function PackReadyRidOff() {
        var response = await PlcDataService.PackReadyRidOff()
        console.log(response);
        if (response.status === 200) {
            //window.location.reload();
        }
        else {
            alert('Data not saved');
        }
    }

    async function GasketRidedOff() {
        var response = await PlcDataService.GasketsRidedOff()
        if (response.status === 200) {
            console.log(response.status);
        }
        else {
            alert('Data not saved');
        }

    }

    async function AddGasketsLayer() {
        var response = await PlcDataService.AddGasketsLayer()
        if (response.status === 200) {
            console.log(response.status);
        }
        else {
            alert('Data not saved');
        }

    }

    function getBin(sx) {
        const items = [];
        for (let i = 0; i < 21; i++) {
            items.push(sx & Math.pow(2, Number(20 - i)) ? "1" : "0");
        }
        return (items)
    }

    const toggleEnable2Sxema = (e) => {

        const { target } = e;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setSettings(prevState => ({ ...prevState, enable2Sxema: value }));

    };

    const toggleEnableRolling = (e) => {

        const { target } = e;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setSettings(prevState => ({ ...prevState, enableRolling: value }));

    };

    const toggleCheckPlankL = (e) => {

        const { target } = e;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setSettings(prevState => ({ ...prevState, checkPlankL: value }));
    };
    
    const togglePackToPack = (e) => {

        const { target } = e;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setSettings(prevState => ({ ...prevState, packToPack: value }));
    };

    const handleSubmitCorrect = async (event) => {
        debugger
        var response = await PlcDataService.ChangePackParam(Number(settings.setNumLayer), Number(settings.setNumPlankInLayer))
        if (response.status === 200) {
            console.log(response);
        }
        else {
            alert('Data not saved');
        }
    }

    const handleSubmit = async (event) => {

        debugger
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);

        if (
            (settings === "undefined" || settings.qpLankLayersBrak === 0 || settings.qpLankLayersBrak === "0") ||
            (settings === "undefined" || settings.qPlanksInLayerBrak === 0 || settings.qPlanksInLayerBrak === "0") ||
            (settings.checkPlankL && (settings === "undefined" || settings.checkL === 0 || settings.checkL === "0")) ||
            (settings.enableRolling && (settings === "undefined" || settings.packL === 0 || settings.packL === "0")) ||
            (settings === "undefined" || settings.qLayersInPack === 0 || settings.qLayersInPack === "0") ||
            (settings === "undefined" || settings.qPlanksInLayer === 0 || settings.qPlanksInLayer === "0") ||
            (settings.enable2Sxema && (settings === "undefined" || settings.sxema2Id === 0 || settings.sxema2Id === "0")) ||
            (settings.enable2Sxema && (settings === "undefined" || settings.sxema2Qlayers === 0 || settings.sxema2Qlayers === "0"))
        ) {
            return
        } else {
            flagUpdate.current = false
            setModalShow(true)
        }

    };

    function WarModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Вы собираетесь загрузить текущие настройки в ПЛК</h4>
                    <p>Нажмите кнопку "Продолжить" для того чтобы загрузить настройки в ПЛК или на кнопку "Отмена"</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button type="button" onClick={setSetPlcDBfunc} className="btn btn-primary">Продолжить</Button>
                    <Button className="btn btn-secondary" onClick={props.onHide}>Отмена</Button>
                </Modal.Footer>
            </Modal>
        );
    }



    function WarModalRead(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Вы собираетесь считать текущие настройки из ПЛК</h4>
                    <p>Нажмите кнопку "Продолжить" для того чтобы считать настройки из ПЛК или на кнопку "Отмена"</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button type="button" onClick={ReadSettingsFromPlc} className="btn btn-primary">Продолжить</Button>
                    <Button className="btn btn-secondary" onClick={props.onHide}>Отмена</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h3 className="display-4" >Перештабелирование пакетов  без партии</h3>
            </Jumbotron>

            <WarModal
                show={modalShow}
                onHide={() => { flagUpdate.current = true; setModalShow(false); }}
            />

            <WarModalRead
                show={modalReadShow}
                onHide={() => { flagUpdate.current = true; setModalReadShow(false); }}
            />

            
<Row>
                <Col sm="2">
                    <Card>
                        <Card.Header>
                            <ButtonToolbar>

                                <ButtonGroup >
                                    <Button variant="primary" onClick={handleSubmit} title="Записать настройки в ПЛК"><CloudDownload /></Button>
                                    <Button variant="danger" onClick={() => finishPack()} title="Закончить пересортировку"><StopCircle /></Button>
                                    <Button variant="info" onClick={() => {flagUpdate.current = false; setModalReadShow(true)}} title="Считать настройки из ПЛК" ><CloudUpload /></Button>
                                    <Button variant="success" onClick={() => PackReadyRidOff()} title="Пакет готов к выкатке"> <Truck /></Button>
                                    <Button variant="secondary" onClick={() => GasketRidedOff()} title="Прокладки убраны с ПФМ"> <BoxArrowUpRight /></Button>
                                    <Button variant="secondary" onClick={() => AddGasketsLayer()} title="Собрать дополнительный слой прокладок"> <BagPlus /></Button>
                                </ButtonGroup>



                            </ButtonToolbar>
                        </Card.Header>
                    </Card>
                </Col>

                <Col sm="2">
                    <Card>
                        <Card.Header>
                            <ButtonToolbar>

                                <ButtonGroup>
                                    <Button variant="danger" onClick={() => FinishDefectedPack()} title="Принудительно закончить пакет брака"><XCircle /></Button>
                                    <Button variant="primary" onClick={() => DefectPacketRemoved()} title="Пакет брака забрали"> <Truck /></Button>
                                    <Button variant="secondary" onClick={() => GasketsBrakPut()} title="Прокладки положены на ПФМ брака"> <BoxArrowInDownLeft /></Button>
                                </ButtonGroup>

                            </ButtonToolbar>
                        </Card.Header>
                    </Card>
                </Col>

                <Col sm="8">
                    <Card>
                        <Card.Header>
                            <Row>
                                <Col>
                                    {(Number(part && part.id) === Number(settings.partId)) ? <h6>Партия {part && part.partNumber}</h6> : <h6 className = "text-danger"> В настройках другая партия</h6>}
                                </Col>
                            </Row>
                        </Card.Header>
                    </Card>
                </Col>
            </Row>



            <Card bg={getPlc && getPlc.lineInWork ? "success" : "secondary"} text="light">
                <Card.Header>

                    <Row>
                        <Col>
                            № слоя: {getPlc.numLayerActual}, № доски в слое: {getPlc.numPlankInLayerActual}
                        </Col>
                        <Col>
                            Уложено в пакет ({getPlc.qPlanksOk} из {(settings.qPlanksInLayer * settings.qLayersInPack)}) шт
                        </Col>
                    </Row>

                </Card.Header>

                <Card.Text>
                    <ProgressBar now={getPlc.qPlanksOk / (settings.qPlanksInLayer * settings.qLayersInPack) * 100}
                        label={`${(getPlc.qPlanksOk / (settings.qPlanksInLayer * settings.qLayersInPack) * 100).toFixed(2)}%`} />
                </Card.Text>
                <Card.Text></Card.Text>
            </Card>


            <Card>
                <Card.Header>
                    <Form>
                        <Form.Row>

                            <Form.Group as={Col} controlId="validationqsetNumPlankInLayer">
                                <Form.Label>Корректировка номера доски в текущем слое ПФМ</Form.Label>
                                <Form.Control type="number" min="1" max={settings.qPlanksInLayer} name="setNumPlankInLayer" placeholder="Кол-во досок в слое ПФМ"
                                    value={settings.setNumPlankInLayer}
                                    onChange={handleSettingsChange}
                                    title="Корректировка номера доски в текущем слое ПФМ"
                                //isInvalid={settings === "undefined" || settings.setNumPlankInLayer === 0 || settings.setNumPlankInLayer === "0"}
                                />

                            </Form.Group>

                            <Form.Group as={Col} controlId="validationsetNumLayer">
                                <Form.Label>Корректировка номера слоя пакета</Form.Label>
                                <Form.Control type="number" min="1" max={settings.qLayersInPack} name="setNumLayer" placeholder="Кол-во слоёв в пакете, шт"
                                    value={settings.setNumLayer}
                                    onChange={handleSettingsChange}
                                    title="Кол-во слоёв в пакете, шт"
                                />

                            </Form.Group>

                            <Form.Group as={Col}>
                                <Form.Label> </Form.Label>
                                <ButtonGroup className="mb-2 btn-block">
                                    <Button variant="primary" onClick={handleSubmitCorrect}>Откорректировать</Button>
                                    {/* <Button  variant="info" onClick={startNewPack}>Новый пакет</Button> */}
                                </ButtonGroup>
                            </Form.Group>

                        </Form.Row>
                    </Form>
                </Card.Header>
            </Card>
            <Card>
                <Card.Header>
                    <Form>
                        <Row>
                            <Col sm={8}>
                                <Form.Group>
                                    <Form.Label>Скорость линии в ПЛК: {getPlc.generalSpeed}%. Задание: {settings.generalSpeed}%</Form.Label>
                                    <Form.Control type="range" name="generalSpeed" value={settings && settings.generalSpeed} onChange={handleSettingsChange} />
                                </Form.Group>
                            </Col>

                            <Col>
                                <Form.Group>
                                    <Form.Label> </Form.Label>
                                    <ButtonGroup className="mb-2 btn-block">
                                        <Button variant="primary" onClick={ChangeSpeed}>Откорректировать</Button>
                                        {/* <Button  variant="info" onClick={startNewPack}>Новый пакет</Button> */}
                                    </ButtonGroup>
                                </Form.Group>
                            </Col>
                        </Row>


                    </Form>
                </Card.Header>
            </Card>
            <Card>
                <Card.Header>
            <Form>
 

                    <Form.Row>
                        <Form.Group as={Col}>
                            <Form.Label>Поиск по толщине ITEM кода</Form.Label>
                            <Form.Control
                                required
                                type="number"
                                name="searchtextT"
                                placeholder="Поиск"
                                onChange={handleChangeSearch}
                                value={search.searchtextT}
                            />
                        </Form.Group>
                        <Form.Group as={Col}>
                            <Form.Label>Поиск по ширине ITEM кода</Form.Label>
                            <Form.Control
                                required
                                type="number"
                                name="searchtextW"
                                placeholder="Поиск"
                                onChange={handleChangeSearch}
                                value={search.searchtextW}
                            />
                        </Form.Group>
                    </Form.Row>
                    <Form.Group controlId="validationItemId">
                        <Form.Label> Список ITEM-кодов</Form.Label>
                        <Form.Control

                            as="select"
                            name="itemId"
                            placeholder="Выберите Item"
                            onChange={handleSettingsChange}
                            size="md"
                            title="ITEM-код для пакета"
                            value={settings.itemId}
                            isInvalid={settings.itemId === undefined || settings.itemId < 0}
                            isValid={settings.itemId !== undefined && settings.itemId >= 0}
                        >
                            <option value={-1}>Выберите ITEM-код для пакета</option>

                            {itemsF}
                        </Form.Control>
                        <Form.Control.Feedback type="invalid">
                            Пожалуйста, выберите ITEM-код для пакета
                        </Form.Control.Feedback>
                    </Form.Group>
                </Form>
                </Card.Header>
            </Card>

                <Form noValidate validated={validated} onSubmit={handleSubmit}>


                    <Card>
                        <Card.Body>
                            <Card.Text>

                                <Form.Row>

                                    <Form.Group as={Col} controlId="validationSxemaId">
                                        <Form.Control
                                            required
                                            custom
                                            as="select"
                                            name="sxemaId"
                                            placeholder="Выберите схему прокладок"
                                            onChange={handleSettingsChange}
                                            size="md"
                                            title="Выбор схемы слоёв прокладок"
                                            defaultValue={settings.sxemaId}
                                            value={settings.sxemaId}
                                            isInvalid={settings === "undefined" || settings.sxemaId === 0 || settings.sxemaId === "0"}
                                        >
                                            <option value="0">Выберите схему</option>
                                            {
                                                gaskets.map(res => {
                                                    return (
                                                        <option key={res.id} value={res.id}>
                                                            {res.name} ={">"} {getBin(res.sxema)}
                                                        </option>
                                                    )
                                                })
                                            }
                                        </Form.Control>
                                        <Form.Control.Feedback type="invalid">
                                            Выберите схему прокладок
                                        </Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group as={Col} controlId="formBasicCheckbox">
                                        <Form.Check type="checkbox" label="2 схемы" checked={settings.enable2Sxema} onChange={toggleEnable2Sxema} title="Собирать пакет двумя схемами прокладок" />
                                    </Form.Group>

                                    <Form.Group as={Col} controlId="validationSxemaQuant">
                                        <FormControl type="number" min="1" max="50" placeholder="Кол-во нижних слоёв прокладок" name="sxema2Qlayers"
                                            value={settings.sxema2Qlayers}
                                            onChange={handleSettingsChange}
                                            title="Кол-во нижних слоёв прокладок"
                                            disabled={!settings.enable2Sxema}
                                            isInvalid={settings.enable2Sxema && (settings === "undefined" || settings.sxema2Qlayers === 0 || settings.sxema2Qlayers === "0")}
                                        />
                                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                                        <Form.Control.Feedback type="invalid">
                                            Выберите кол-во нижних слоёв прокладок
                                        </Form.Control.Feedback>
                                    </Form.Group>





                                    <Form.Group as={Col} controlId="validationSxema2Id">

                                        <Form.Control
                                            required
                                            custom
                                            as="select"
                                            name="sxema2Id"
                                            placeholder="Выберите схему нижних слоёв прокладок"
                                            onChange={handleSettingsChange}
                                            size="md"
                                            disabled={!settings.enable2Sxema}
                                            value={settings.sxema2Id}
                                            title="Выбор схемы нижних слоёв прокладок"
                                            defaultValue={settings.sxema2Id}
                                            isInvalid={settings.enable2Sxema && (settings === "undefined" || settings.sxema2Id === 0 || settings.sxema2Id === "0")}
                                        >
                                            <option value={0}>Выберите схему</option>
                                            {
                                                gaskets && gaskets.map(res => {
                                                    return (
                                                        <option key={res.id} value={res.id}>
                                                            {res.name} ={">"} {getBin(res.sxema)}
                                                        </option>
                                                    )
                                                })
                                            }
                                        </Form.Control>
                                        <Form.Control.Feedback type="invalid">
                                            Выберите схему нижних слоёв прокладок
                                        </Form.Control.Feedback>
                                    </Form.Group>

                                </Form.Row>

                            </Card.Text>
                        </Card.Body>
                    </Card>

                    <Card>
                        <Card.Body>
                            <Card.Text>
                                <Form.Row>



                                    <Form.Group as={Col} controlId="validationqPlanksInLayer">
                                        <Form.Label>Кол-во досок в слое ПФМ, шт</Form.Label>
                                        <Form.Control type="number" min="1" max="300" name="qPlanksInLayer" i placeholder="Кол-во досок в слое ПФМ"
                                            value={settings.qPlanksInLayer}
                                            onChange={handleSettingsChange}
                                            title="Кол-во досок в слое ПФМ, шт"
                                            isInvalid={settings === "undefined" || settings.qPlanksInLayer === 0 || settings.qPlanksInLayer === "0"}
                                        />
                                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                                        <Form.Control.Feedback type="invalid">
                                            Выберите кол-во досок в слое ПФМ
                                    </Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group as={Col} controlId="validationqLayersInPack">
                                        <Form.Label>Кол-во слоёв пакете, шт</Form.Label>
                                        <Form.Control type="number" min="1" max="100" name="qLayersInPack" placeholder="Кол-во слоёв в пакете, шт"
                                            value={settings.qLayersInPack}
                                            onChange={handleSettingsChange}
                                            title="Кол-во слоёв в пакете, шт"
                                            isInvalid={settings === "undefined" || settings.qLayersInPack === 0 || settings.qLayersInPack === "0"}
                                        />
                                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                                        <Form.Control.Feedback type="invalid">
                                            Выберите кол-во слоёв в пакете
                                    </Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group as={Col} >
                                        <p> </p>
                                        <Form.Check label="Раскатать по длине" checked={settings.enableRolling} onChange={toggleEnableRolling} title="Раскатать пакет по заданной длине" />
                                    </Form.Group>

                                    <Form.Group as={Col} controlId="validationpackL">
                                        <Form.Label>Заданная длина пакета, мм</Form.Label>
                                        <Form.Control type="number" min="1800" max="6300" name="packL" placeholder="Заданная длина пакета, мм"
                                            value={settings.packL}
                                            onChange={handleSettingsChange}
                                            title="Заданная длина пакета, мм"
                                            disabled={!settings.enableRolling}
                                            isInvalid={settings.enableRolling && (settings === "undefined" || settings.packL === 0 || settings.packL === "0")}
                                        />
                                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                                        <Form.Control.Feedback type="invalid">
                                            Выберите длину пакета
                                    </Form.Control.Feedback>
                                    </Form.Group>

                                </Form.Row>
                                <Form.Row>

                                    <Form.Group as={Col} >
                                        <p> </p>
                                        <Form.Check label="Пакет в пакет" checked={settings.packToPack} onChange={togglePackToPack} title="Собирать пакет в пакет" />
                                    </Form.Group>

                                    <Form.Group as={Col} controlId="validationtotalPlanksInPack">
                                        <Form.Label>Кол-во досок в пакете, шт</Form.Label>
                                        <Form.Control type="number" min="1" max="1000" name="totalPlanksInPack" placeholder="Кол-во досок в пакете, шт"
                                            value={settings.totalPlanksInPack}
                                            disabled={!settings.packToPack}
                                            onChange={handleSettingsChange}
                                            isInvalid={settings.packToPack && (settings === "undefined" || settings.totalPlanksInPack === 0 || settings.totalPlanksInPack === "0")}
                                        />
                                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                                        <Form.Control.Feedback type="invalid">
                                            Укажите кол-во досок в пакете, шт
                                    </Form.Control.Feedback>
                                    </Form.Group>
                                    
                                    <Form.Group as={Col} >
                                        <p> </p>
                                        <Form.Check label="Проверять длину каждой доски" checked={settings.checkPlankL} onChange={toggleCheckPlankL} title="Проверять длину каждой доски" />
                                    </Form.Group>

                                    <Form.Group as={Col} controlId="validationplankL">
                                        <Form.Label>Проверяемая длина доски, мм</Form.Label>
                                        <Form>
                                            <Form.Group>
                                                <Form.Control
                                                    required
                                                    as="select"
                                                    name="checkL"
                                                    placeholder="Выберите проверяемую длину доски"
                                                    onChange={handleSettingsChange}
                                                    size="md"
                                                    disabled={!settings.checkPlankL}
                                                    value={settings.checkL}
                                                    title="Выбор проверяемой длины доски"
                                                    isInvalid={settings.checkPlankL && (settings === "undefined" || settings.checkL === 0 || settings.checkL === "0")}
                                                >
                                                    <option value="0">Выберите проверяемую длину</option>
                                                    {
                                                        plankLarr.map(res => {
                                                            return (
                                                                <option key={res.value} value={res.value}>
                                                                    {res.value} мм
                                                                </option>
                                                            )
                                                        })
                                                    }
                                                </Form.Control>
                                                <Form.Control.Feedback type="invalid">
                                                    Выберите проверяемую длину доски
                                            </Form.Control.Feedback>
                                            </Form.Group>
                                        </Form>


                                    </Form.Group>
                                </Form.Row>
                            </Card.Text>
                        </Card.Body>

                    </Card>



                    <Card>
                        <Card.Body>
                            <Card.Text>
                                <Form.Row>

                                    <Form.Group as={Col} controlId="validationqPlanksInLayerBrak">
                                        <Form.Label>Кол-во досок в слое ПФМ брака, шт</Form.Label>
                                        <Form.Control type="number" min="1" max="30" name="qPlanksInLayerBrak" placeholder="Кол-во досок в слое ПФМ бракаб шт"
                                            value={settings.qPlanksInLayerBrak}
                                            onChange={handleSettingsChange}
                                            isInvalid={settings === "undefined" || settings.qPlanksInLayerBrak === 0 || settings.qPlanksInLayerBrak === "0"}
                                        />
                                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                                        <Form.Control.Feedback type="invalid">
                                            Укажите кол-во досок в слое ПФМ брака, шт
                                    </Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group as={Col} controlId="validationqpLankLayersBrak">
                                        <Form.Label>Кол-во слоёв пакете брака, шт</Form.Label>
                                        <Form.Control type="number" min="1" max="100" name="qpLankLayersBrak" placeholder="Кол-во слоёв пакете брака, шт"
                                            value={settings.qpLankLayersBrak}
                                            onChange={handleSettingsChange}
                                            isInvalid={settings === "undefined" || settings.qpLankLayersBrak === 0 || settings.qpLankLayersBrak === "0"}
                                        />
                                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                                        <Form.Control.Feedback type="invalid">
                                            Укажите кол-во слоёв пакете брака, шт
                                    </Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group as={Col} controlId="validationgasketsLayerPeriod">
                                    <Form.Label>Кол-во слоёв досок между прокладками, шт (0 - без прокладок)</Form.Label>
                                    <Form.Control type="number" min="0" max="100" name="gasketsLayerPeriod" placeholder="Кол-во слоёв досок между прокладками, шт (0 - без прокладок)"
                                        value={settings.gasketsLayerPeriod}
                                        onChange={handleSettingsChange}
                                        
                                    />
                                    <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                                    <Form.Control.Feedback type="invalid">
                                    Кол-во слоёв досок между прокладками, шт (0 - без прокладок)
                                    </Form.Control.Feedback>
                                </Form.Group>

                                    <Col>
                                        <Form.Label>Уложено в пакет брака, шт</Form.Label>
                                        <h5>{getPlc.qPlanksNotOk} из {(settings.qPlanksInLayerBrak * settings.qpLankLayersBrak)}</h5>
                                    </Col>
                                </Form.Row>
                            </Card.Text>
                        </Card.Body>
                    </Card>

                    <Card>
                        <Card.Header>
                            <Form>
                                <Form.Row>
                                    <Form.Group as={Col}>
                                        <Form.Label>Корректировка номера доски в слое ПФМ брака, шт</Form.Label>
                                        <Form.Control type="number" min="1" max={settings.qPlanksInLayerBrak} name="setNumPlankInLayerBrak" placeholder="Номер доски в слое ПФМ брака, шт"
                                            value={settings.setNumPlankInLayerBrak}
                                            onChange={handleSettingsChange}
                                            title="Номер доски в слое ПФМ брака, шт"
                                        />

                                    </Form.Group>

                                    <Form.Group as={Col}>
                                        <Form.Label> </Form.Label>
                                        <ButtonGroup className="mb-2 btn-block">
                                            <Button  variant="primary"  onClick={ChangePackParamBrak}>Откорректировать</Button>
                                            {/* <Button  variant="info" onClick={startNewPack}>Новый пакет</Button> */}
                                        </ButtonGroup>
                                    </Form.Group>

                                </Form.Row>
                            </Form>
                        </Card.Header>
                    </Card>

                    <Button type="button" className="btn btn-primary btn-block" onClick={handleSubmit}>Загрузить параметры в ПЛК</Button>
                    <Button type="button" className="btn btn-secondary btn-block" onClick={() => history.goBack()}>Назад</Button>

                </Form>
        </Container>

    )



}

export default Resorting

