import React, { useState, useEffect } from 'react';
import ProviderDataService from "../Services/ProviderService";
import { Button, Form, Container, Jumbotron, Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { PencilFill, Trash } from 'react-bootstrap-icons';
import { useHistory } from "react-router-dom";

function ProviderList() {

    let history = useHistory();
    const [providers, setProviders] = useState(null)
    const [modalShow, setModalShow] = useState(false);
    const [activeProviderId, setActiveProviderId] = useState(null);

    useEffect(() => {
            getProviders();
    }, [])

    async function getProviders() {
        const providers = await ProviderDataService.getAll()
        setProviders(providers)
        console.log(providers, 'providers');
    }


    async function ImportCSV() {
        await ProviderDataService.ImportCSV()
        .then(json => {
            console.log("json", json);
            if (json.status === 200) {
                alert('Поставщики успешно импортированы');
            }
            else {
                alert('Не получилось импортиовать');

            }
            history.push("/");
            history.replace("/ProviderList");
        })
        .catch(
            function (error) {
                alert('Не получилось импортиовать');
                console.log(error);
                history.push("/");
                history.replace("/ProviderList");
            }
        )
    }

    function ExportCSV() {
        ProviderDataService.ExportCSV()
            .then(json => {
                console.log("json", json);
                if (json.status === 200) {
                    console.log(json.data.Status);
                }
                else {
                    alert('Data not saved');
                }
                
                history.push("/");
                history.replace("/ProviderList");
            })
            .catch(
                function (error) {
                    console.log(error);
                    alert('Не получилось экспортировать');
                    history.push("/");
                    history.replace("/ProviderList");
                }
            )
    }

    async function DeleteProvider() {
        setModalShow(false)
        var response = await ProviderDataService.remove(activeProviderId)

        if (response.status === 200) {
            alert('Постащик успешно удалён');
        }

        history.push("/");
        history.replace("/ProviderList");
    }

    function DeletedModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Вы собираетесь удалить поставщика</h4>
                    <p>
                        Нажмите кнопку "Удалить" для подтверждения или кнопку "Отмена"
                  </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={DeleteProvider}>Удалить</Button>
                    <Button onClick={props.onHide}>Отмена</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function setActiveProvider(id) {
        setActiveProviderId(id);
        setModalShow(true)
    }

    function addProvider() {
        history.push("/AddProviders")
    }

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Список поставщиков</h1>
            </Jumbotron>

            <hr />

            <DeletedModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

            <div className="row">
                <div className="col">
                    <Button color="primary" onClick={addProvider} >Добавить нового поставщика</Button>
                </div>

                <div className="col">
                    {/* <Button color="primary" onClick={ImportCSV} >Импорт</Button> */}
                    {providers ? <Button color="primary" onClick={ExportCSV} >Экспорт</Button> : " "}
                </div>
            </div>

            <hr />

            <Form className="form">
                {providers ?
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>ITEM</th>
                                <th>Наименование поставщика</th>
                                <th>VAT</th>
                                <th>Действия</th>
                            </tr>
                        </thead>

                        <tbody>
                            {providers && providers.map(function (el, index) {
                                return <tr key={index}>
                                    <td>{el.item}</td>
                                    <td>{el.name}</td>
                                    <td>{el.vaTno}</td>
                                    <td>
                                        <Link to={"/EditProvider/" + el.id} className="btn btn-success"><PencilFill /></Link>
                                        <button key={el.id} type="button" onClick={() => setActiveProvider(el.id)} className="btn btn-danger"><Trash /></button>
                                    </td>
                                </tr>
                            })}
                        </tbody>


                    </table>
                    : "Нет ни одного поставщика"}
            </Form>

        </Container>
    );


}

export default ProviderList