import React, { useState, useEffect } from 'react';
import { Form, Col, Button, Container, Jumbotron, Modal } from 'react-bootstrap';
import ProviderDataService from "../Services/ProviderService";
import { BrowserRouter as useHistory, useParams } from "react-router-dom";

function EditProvider(props) {
    const [modalShow, setModalShow] = useState(false);
    const [validated, setValidated] = useState(false);
    const [provider, setProviders] = useState({
        id: 0,
        item: "",
        name: "",
        vaTno: ""
    });
    let param = useParams();
    let history = useHistory();

    useEffect(() => {
        async function getProvider() {
            const provider = await ProviderDataService.get(Number(param.id))
            setProviders(provider)
            console.log(provider, 'provider');
        }
        
        getProvider();
    }, [])



    async function UpdateProvider() {
        return await ProviderDataService.update(param.id, { id: provider.id, item: provider.item, name: provider.name, vaTno: provider.vaTno });
    }

    const handleChange = e => {
        const { name, value } = e.target;
        setProviders(prevState => ({ ...prevState, [name]: value }));
    }

    function redir() {
        history.push("/");
        history.replace("/ProviderList");
    }

    const handleSubmit = async (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);

        if (provider.name.length > 0 && provider.item.length > 0 && provider.vaTno.length > 0) {
            var response = await UpdateProvider();
            if (response.status === 200) {
                redir();
            } else {
                setModalShow(true)
            }
        }

    };

    function ErrorModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Не получилось создать поставщика</h4>
                    <p>
                        Проверьте задаваемые параметры, убедитесь в наличии связи
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>Ok</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Редактирование поставщика</h1>
            </Jumbotron>

            <ErrorModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

            <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Form.Row>

                    <Form.Group as={Col} md="4" controlId="validationCustom01">
                        <Form.Label>Наименование поставщика</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Введите наименование поставщика"
                            name="name"
                            value={provider.name}
                            onChange={handleChange}
                        />
                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                            Пожалуйста, введите наименование поставщика
                            </Form.Control.Feedback>
                    </Form.Group>


                    <Form.Group as={Col} md="4" controlId="validationCustom02">
                        <Form.Label>ITEM</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Введите ITEM поставщика"
                            name="item"
                            value={provider.item}
                            onChange={handleChange}
                        />
                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                            Пожалуйста, введите ITEM номер поставщика
                            </Form.Control.Feedback>
                    </Form.Group>


                    <Form.Group as={Col} md="4" controlId="validationCustom02">
                        <Form.Label>VAT</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Введите VAT номер"
                            name="vaTno"
                            value={provider.vaTno}
                            onChange={handleChange}
                        />
                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                            Пожалуйста, введите VAT номер поставщика
                            </Form.Control.Feedback>
                    </Form.Group>

                </Form.Row>

                <Button type="button" className="btn btn-primary btn-block" onClick={handleSubmit}>Обновить поставщика</Button>
                <Button type="button" className="btn btn-secondary btn-block" onClick={() => history.goBack()}>Назад</Button>
            </Form>


        </Container>
    )

}




export default EditProvider;






