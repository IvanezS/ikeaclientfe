import React, { useState } from 'react';
import { Form, Col, Button, Container, Jumbotron, Modal } from 'react-bootstrap';
import ProviderDataService from "../Services/ProviderService";
import { useHistory } from "react-router-dom";

function AddProvider() {
    const [modalShow, setModalShow] = useState(false);
    const [validated, setValidated] = useState(false);
    const [providers, setProviders] = useState({
        item: "",
        name: "",
        vaTno: ""
    });

    let history = useHistory();

    async function CreateProvider() {
        return await ProviderDataService.create(providers);
    }

    const handleChange = e => {
        const { name, value } = e.target;
        setProviders(prevState => ({ ...prevState, [name]: value }));
    }

    function redir() {
        history.push("/");
        history.replace("/ProviderList");
    }

    function ErrorModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Не получилось создать поставщика</h4>
                    <p>
                        Проверьте задаваемые параметры, убедитесь в наличии связи
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>Ok</Button>
                </Modal.Footer>
            </Modal>
        );
    }
    const handleSubmit = async (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);

        if (providers.name.length > 0 && providers.item.length > 0 && providers.vaTno.length > 0) {
            var response = await CreateProvider();
            if (response.status === 200) {
                redir();
            } else {
                setModalShow(true)
            }
        }

    };

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Введите данные нового поставщика</h1>
            </Jumbotron>

            <ErrorModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

            <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Form.Row>

                    <Form.Group as={Col} md="4" controlId="validationCustom01">
                        <Form.Label>Наименование поставщика</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Введите наименование поставщика"
                            name="name"
                            value={providers.name}
                            onChange={handleChange}
                        />
                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                            Пожалуйста, введите наименование поставщика
                            </Form.Control.Feedback>
                    </Form.Group>


                    <Form.Group as={Col} md="4" controlId="validationCustom02">
                        <Form.Label>ITEM</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Введите ITEM поставщика"
                            name="item"
                            value={providers.item}
                            onChange={handleChange}
                        />
                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                            Пожалуйста, введите ITEM номер поставщика
                            </Form.Control.Feedback>
                    </Form.Group>


                    <Form.Group as={Col} md="4" controlId="validationCustom02">
                        <Form.Label>VAT</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Введите VAT номер"
                            name="vaTno"
                            value={providers.vaTno}
                            onChange={handleChange}
                        />
                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                            Пожалуйста, введите VAT номер поставщика
                            </Form.Control.Feedback>
                    </Form.Group>

                </Form.Row>

                <Button type="button" className="btn btn-primary btn-block" onClick={handleSubmit}>Создать поставщика</Button>
                <Button type="button" className="btn btn-secondary btn-block" onClick={() => history.goBack()}>Назад</Button>
            </Form>


        </Container>
    )

}

export default AddProvider;