import React, { useState, useEffect, useContext, Fragment } from 'react';
import { Form, Container, Jumbotron, Modal, Button, Row, Col, Accordion, Card, ListGroup, useAccordionToggle, AccordionContext, Table } from 'react-bootstrap';
import MainPartDataService from "../Services/MainPartService";
import { useHistory } from "react-router-dom";
import { ArrowRight, List, ListCheck, PencilFill, Trash, ClipboardData, ArrowDown, ArrowUp } from 'react-bootstrap-icons';
import { useSelector } from 'react-redux'


function MainPartList() {

    let history = useHistory();
    const [parties, setParties] = useState(null)
    const [activePartId, setActivePartId] = useState(null);
    const [activeMainPartId, setActiveMainPartId] = useState(null);
    const [modalShow, setModalShow] = useState(false);
    const [modalWarShow, setModalWarShow] = useState(false);
    const [modalStatisticShow, setModalStatisticShow] = useState(false);
    const [modalMainStatisticShow, setModalMainStatisticShow] = useState(false);
    const [search, setSearch] = useState({ searchtext: "" });
    const user = useSelector(state => state.auth.user)

    useEffect(() => {
        getParties();
    }, [])

    async function getParties() {
        const party = await MainPartDataService.getAll()
        setParties(party);
        console.log(party, 'parties');
    }

    async function DeletePart() {
        setModalShow(false)
        await MainPartDataService.remove(activePartId)
            .then(json => {
                if (json.status === 200) {
                }
            })
        getParties()
    }

    function DeletedModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Вы собираетесь удалить партию со всеми пакетами и статистикой</h4>
                    <p>Нажмите кнопку "Удалить" для подтверждения или кнопку "Отмена"</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={DeletePart}>Удалить</Button>
                    <Button onClick={props.onHide}>Отмена</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function WarningModal(props) {


        var mainPart = parties && parties.find(data => {if (data.id === activeMainPartId) return data})
        var part = activePartId && mainPart && mainPart.itemParts.find(data => {if (data.id === activePartId) return data})
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
                    </Modal.Title>
                </Modal.Header>
                
                    {part && part.packets.length === 0 ? 
                        <Fragment>
                            <Modal.Body>
                                <h4>Нет исходных пакетов для данной партии!</h4>
                                <p>Для доступа к пересортировке. Добавьте исходные пакеты для данной партии</p>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button onClick={props.onHide}>ОК</Button>
                            </Modal.Footer>
                        </Fragment>
                        :
                        <Fragment>
                            <Modal.Body>
                                <h4>Начать пересортировку партии?</h4>
                                <p>Для доступа к пересортировке нажмите кнопку "Начать пересортировку"</p>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="warning" onClick={()=>history.push("/ResortPart/" + activePartId)}>Начать пересортировку</Button>
                                <Button onClick={props.onHide}>Отмена</Button>
                            </Modal.Footer>
                        </Fragment>
                    }

            </Modal>
        );
    }

    function StatisticModal(props) {

        var mainPart = parties && parties.find(data => {if (data.id === activeMainPartId) return data})
        var part = activePartId && mainPart && mainPart.itemParts.find(data => {
            if (data.id === activePartId) return data
        })
        var totalVolumeIn = part && part.packets.reduce((total, currentValue) => total = total + currentValue.totalVolume, 0)
        var totalVolumeOut = part && part.resortedPackets.reduce((total, currentValue) => total = total + currentValue.totalVolume, 0)
        var BadPackets = part && part.resortedPackets.filter(data => {
            if (data.isDefected) return data
        })
        var totalVolumeBad = part && BadPackets.reduce((total, currentValue) => total = total + currentValue.totalVolume, 0)
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        СТАТИСТИКА ПО {part && part.state === 2 ? "ЗАКРЫТОМУ" : "НЕЗАКРЫТОМУ"} ITEM-КОДУ ПАРТИИ {part && part.partNumber}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col>
                            <h4>Исходные пакеты:</h4>
                            <p>Кол-во: {part && part.packets.length} шт</p>
                            <p>Объём: {part && totalVolumeIn.toFixed(3)} м3</p>
                        </Col>

                        <Col>
                            <h4>Нормальные пакеты:</h4>
                            <p>Кол-во: {part && part.resortedPackets.length - BadPackets.length} шт</p>
                            <p>Объём: {part && (totalVolumeOut - totalVolumeBad).toFixed(3)} м3</p>
                        </Col>
                        <Col>
                            <h4>Бракованные пакеты:</h4>
                            <p>Кол-во: {part && BadPackets.length} шт</p>
                            <p>Объём: {part && totalVolumeBad.toFixed(3)} м3</p>
                        </Col>
                        <Col>
                            <h4>Суммарно по пакетам:</h4>
                            <p>Кол-во: {part && part.resortedPackets.length} шт</p>
                            <p>Объём: {part && totalVolumeOut.toFixed(3)} м3</p>
                        </Col>
                    </Row>
                    <h4>Итоги:</h4>
                    <p>Баланс по объёму (Vdelta = Vисх - Vпересорт): {part && (totalVolumeIn - totalVolumeOut).toFixed(3)} м3</p>
                    <p>Процент брака: {part && (totalVolumeBad * 100 / totalVolumeOut).toFixed(3)} %</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>ОК</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function MainStatisticModal(props) {

                var mainPart = parties && parties.find(data => {if (data.id === activeMainPartId) return data})
                var PacketsOfPart = mainPart && mainPart.itemParts.reduce((prev, current) => prev.concat(current.packets), [])
                var ResortedPacketsOfPart = mainPart && mainPart.itemParts.reduce((prev, current) => prev.concat(current.resortedPackets), [])

                var totalVolumeIn = PacketsOfPart && PacketsOfPart.reduce((total, currentValue) => total = total + currentValue.totalVolume, 0)
                var totalVolumeOut = ResortedPacketsOfPart && ResortedPacketsOfPart.reduce((total, currentValue) => total = total + currentValue.totalVolume, 0)
                var BadPackets = ResortedPacketsOfPart && ResortedPacketsOfPart.filter(data => {if (data.isDefected) return data })
                var BadPacketsVolume = BadPackets && BadPackets.reduce((total, currentValue) => total = total + currentValue.totalVolume, 0)
                
                var ItemsOfPackets = mainPart && mainPart.itemParts.reduce((prev, current) => prev.concat(current.item), [])
                var ThicksOfItems = ItemsOfPackets && ItemsOfPackets.reduce((prev, current) => prev.concat(current.thick), [])
                var DistinctThicks = [...new Set(ThicksOfItems)]



                debugger

                return (
                    <Modal
                        {...props}
                        size="lg"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Modal.Title id="contained-modal-title-vcenter">
                                СТАТИСТИКА ПО {mainPart && getStateMainPart(activeMainPartId) === "#DDFFDD" ? "ЗАКРЫТОЙ" : "НЕЗАКРЫТОЙ"} ПАРТИИ {mainPart && mainPart.mainPartNumber}
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
 
                            {mainPart ?
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>ТОЛЩИНА</th>
                                <th colSpan="2" textAlign="center">ВХОД</th>
                                <th colSpan="2" textAlign="center">ФАКТ</th>
                                <th colSpan="3" textAlign="center">БРАК</th>
                                <th textAlign="center">БАЛАНС</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>Qисх, шт</th>
                                <th>Vисх, м3</th>
                                <th>Qсорт, шт</th>
                                <th>Vсорт, м3</th>
                                <th>Qбрак, шт</th>
                                <th>Vбрак, м3</th>
                                <th>Брак, %</th>
                                <th>Баланс V, м3</th>
                                
                                
                            </tr>
                        </thead>

                        <tbody>

                            {mainPart && DistinctThicks.map(function(res,i){
                                    var ItemsOfPartThick = mainPart && mainPart.itemParts.filter(data => {if (data.item.thick === res) return data })
                                    var PacketsOfPartThick = ItemsOfPartThick && ItemsOfPartThick.reduce((prev, current) => prev.concat(current.packets), [])
                                    var ResortedPacketsOfPartThick = ItemsOfPartThick && ItemsOfPartThick.reduce((prev, current) => prev.concat(current.resortedPackets), [])
                                    
                                    var totalVolumeInThick = PacketsOfPartThick && PacketsOfPartThick.reduce((total, currentValue) => total = total + currentValue.totalVolume, 0)
                                    var totalVolumeOutThick = ResortedPacketsOfPartThick && ResortedPacketsOfPartThick.reduce((total, currentValue) => total = total + currentValue.totalVolume, 0)
                                    var BadPacketsThick = ResortedPacketsOfPartThick && ResortedPacketsOfPartThick.filter(data => {if (data.isDefected) return data })
                                    var BadPacketsVolumeThick = BadPacketsThick && BadPacketsThick.reduce((total, currentValue) => total = total + currentValue.totalVolume, 0)
                      

                                    return(

                                        <tr>
                                            <th>T = {res} мм:</th>
                                            <td>{res && (PacketsOfPartThick).length}</td>
                                            <td>{res && (totalVolumeInThick).toFixed(3)}</td>
                                            <td>{res && (ResortedPacketsOfPartThick).length}</td>
                                            <td>{res && (totalVolumeOutThick).toFixed(3)}</td>
                                            <td>{res && (BadPacketsThick).length}</td>
                                            <td>{res && (BadPacketsVolumeThick).toFixed(3)}</td>
                                            <td>{res && (BadPacketsVolumeThick * 100 / totalVolumeOutThick).toFixed(3)}</td>
                                            <td>{res && (totalVolumeInThick - totalVolumeOutThick).toFixed(3)}</td>
                                        </tr>

                                    )
                            })}
                            <tr>
                                <th>Суммарно</th>
                                <td>{mainPart && PacketsOfPart.length}</td>
                                <td>{mainPart && (totalVolumeIn).toFixed(3)}</td>
                                <td>{mainPart && ResortedPacketsOfPart.length}</td>
                                <td>{mainPart && (totalVolumeOut).toFixed(3)}</td>
                                <td>{mainPart && BadPackets.length}</td>
                                <td>{mainPart && (BadPacketsVolume).toFixed(3)}</td>
                                <td>{mainPart && (totalVolumeIn - totalVolumeOut).toFixed(3)}</td>
                                <td>{mainPart && (BadPacketsVolume * 100 / totalVolumeOut).toFixed(3)}</td>
                            </tr>
                        </tbody>

                    </Table>
                    : "Нет ни одной партии"}



                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={props.onHide}>ОК</Button>
                        </Modal.Footer>
                    </Modal>
                );
            }


    function setActivePart(id) {
        setActivePartId(id);
        setModalShow(true)
    }

    function setWarningPart() {
debugger
        var mainPart = parties && parties.find(data => {if (data.id === activeMainPartId) return data})
        var part = mainPart.itemParts.find(element => { return element.id === activePartId });
        part.packets.length === 0 ? setModalWarShow(true) : gotoResort(activePartId);
    }

    function gotoResort(id) {
        history.push("/ResortPart/" + id)
    }

    function setStatePfunc(state) {

        if (state === 0) return ("")
        if (state === 1) return ("warning")
        if (state === 2) return ("success")
    }


    function getStateMainPart(MainPartId) {
debugger
        var mainPart = parties && parties.find(data => {if (data.id === MainPartId) return data})
        var PacketsOfPart = mainPart && mainPart.itemParts.reduce((prev, current) => prev.concat(current.packets), [])
        var StatesOfPacketsOfPart = PacketsOfPart && PacketsOfPart.reduce((prev, current) => prev.concat(current.state), [])
        var countTot = StatesOfPacketsOfPart.length
        var countOne = StatesOfPacketsOfPart && StatesOfPacketsOfPart.filter(data => {if (data === 1) return data }).length
        //var countTwo= StatesOfPacketsOfPart && StatesOfPacketsOfPart.filter(data => {if (data === 2) return data }).length
        if (countTot > 0 && (countTot - countOne) === 0) return ("#DDFFDD")
        return ("")
        
    }

    function QuantPack(packs) {
        var result = packs.packets.filter(element => { return element.state === 1 })
        return result.length
    }

    const handleChange = e => {
        const { name, value } = e.target;
        console.log("handleChange", name, value)
        console.log("parties", parties)
        setSearch(prevState => ({ ...prevState, [name]: value }));

    }


    function ContextAwareToggle({ children, eventKey, callback }) {
        const currentEventKey = useContext(AccordionContext);

        const decoratedOnClick = useAccordionToggle(
            eventKey,
            () => callback && callback(eventKey),
        );

        const isCurrentEventKey = currentEventKey === eventKey;

        return (
            <Button
                type="button"
                variant="link"
                onClick={decoratedOnClick}
            >
                {/* {children} */}
                {isCurrentEventKey ? <ArrowUp /> : <ArrowDown />}
            </Button>
        );
    }

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Список партий</h1>
            </Jumbotron>

            <DeletedModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

            <WarningModal
                show={modalWarShow}
                onHide={() => setModalWarShow(false)}
            />


            <StatisticModal
                show={modalStatisticShow}
                onHide={() => setModalStatisticShow(false)}
            />

            <MainStatisticModal
                show={modalMainStatisticShow}
                onHide={() => setModalMainStatisticShow(false)}
            />


            {user && (user.profile.role === "Admin" || user.profile.role === "Master") ?
                <Fragment>
                    <hr />
                    <Button type="button" onClick={() => history.push("/AddParties")} className="btn btn-primary" >Добавить новую партию</Button>
                    <hr />
                </Fragment>
                : ''}


            <Form className="form">
                <Form.Group>
                    <Form.Label>Поиск по номеру партии или поставщику</Form.Label>
                    <Form.Control
                        required
                        type="text"
                        name="searchtext"
                        placeholder="Поиск"
                        onChange={handleChange}
                        value={search.searchtext}
                    />
                </Form.Group>
                {/* {parties ? */}


                {parties && parties.filter(data => {
                    if (search === null) {
                        return data
                    } else {
                        if (data.provider.name.toLowerCase().includes(search.searchtext.toLowerCase()) || data.mainPartNumber.toLowerCase().includes(search.searchtext.toLowerCase())) return data
                    }
                }).map(function (el, index) {
                    return (

                        <Accordion key={index}>
                            <Card  border="dark" fluid 
                            style={{ backgroundColor: getStateMainPart(el.id) }}
                            //text='white'
                            >
                                <Card.Header >

                                    <Row>
                                        <Col>{el.mainPartNumber}</Col>
                                        <Col>{el.provider.name}</Col>
                                        <Col>{el.comment}</Col>
                                        <Col>
                                            <ContextAwareToggle eventKey={el.id} />


                                            <Button type="button" onClick={() => { setActiveMainPartId(el.id); setModalMainStatisticShow(true) }} className="btn btn-info" title="Статистика по партии"><ClipboardData /></Button>
                                            {user && (user.profile.role === "Admin" || user.profile.role === "Master") ?
                                                <Fragment>
                                                    <Button type="button" onClick={() => history.push("/EditPart/" + el.id)} className="btn btn-success" title="Редактировать данные партии"><PencilFill /></Button>
                                                    <Button type="button" onClick={() => setActivePart(el.id)} className="btn btn-danger" title="Удалить партию"><Trash /></Button>
                                              
                                                    <Button type="button" onClick={() => history.push("/AddItemPart/" + el.id)} className="btn btn-dark" title="Список ITEM-кодов для партии"><List /> </Button>
                                                    
                                                </Fragment>
                                                : ''}

                                        </Col>
                                    </Row>


                                    {/* <tr key={index} className={setStatePfunc(el.state)}> */}


                                </Card.Header>


                                <Accordion.Collapse eventKey={el.id}>

                                    <Card.Body>
                                        {el.itemParts.length > 0 ? <h6>Список ITEM-кодов партии:</h6> : "Нет ITEM-кодов партии: "}
                                        <ListGroup>

                                            {parties && el.itemParts.map(function (ol, index) {
                                                return (

                                                    <Card  border={setStatePfunc(ol.state)} fluid key={index}>
                                                        <Card.Header>
                                                            <Row>
                                                                <Col>ITEM-код: {ol.item.itemName}, T: {ol.item.thick} мм, W: {ol.item.width} мм, L: {ol.item.mixedLength === "Yes" ? "MIX" : ol.item.length + " мм"}, Качество {ol.item.quality}, Порода {ol.item.breed}, Сторона: {ol.item.side}, Влажность: {ol.item.moisture}</Col>
                                                            </Row>
                                                        </Card.Header>

                                                        <Card.Body>
                                                            <Row >
                                                                <Col>{ol.partNumber}</Col>
                                                                <Col>{ol.comments}</Col>
                                                                <Col>({QuantPack(ol)}/{ol.packets.length})-{'>'}{ol.resortedPackets.length}</Col>
                                                                <Col>
                                                                    <Button type="button" onClick={() => { setActiveMainPartId(el.id);setActivePartId(ol.id); setModalStatisticShow(true) }} className="btn btn-info" title="Статистика по партии"><ClipboardData /></Button>
                                                                    {user && (user.profile.role === "Admin" || user.profile.role === "Master") ?
                                                                        <Fragment>
                                                                            {/* <Button type="button" onClick={() => history.push("/EditPart/" + ol.id)} className="btn btn-success" title="Редактировать данные партии"><PencilFill /></Button> */}
                                                                            <Button type="button" onClick={() => setActivePart(ol.id)} className="btn btn-danger" title="Удалить партию"><Trash /></Button>
                                                                        </Fragment>
                                                                        : ''}
                                                                    <Button type="button" onClick={() => history.push("/AddPack/" + ol.id)} className="btn btn-secondary" title="Список исходных пакетов партии"><List /></Button>
                                                                    {user && (user.profile.role === "Admin" || user.profile.role === "Oper") ?
                                                                        <Fragment>
                                                                            <Button type="button" onClick={() => history.push("/AddResortedPack/" + ol.id)} className="btn btn-primary" title="Список пересортированных пакетов партии"><ListCheck /></Button>
                                                                            <Button type="button" onClick={() => { setActiveMainPartId(el.id); setActivePartId(ol.id); setModalWarShow(true) }} className="btn btn-warning" title="Пересортировка пакетов данной партии"><ArrowRight /></Button>
                                                                        </Fragment>
                                                                        : ''}
                                                                </Col>
                                                            </Row>
                                                        </Card.Body>
                                                    </Card>


                                                )
                                            })
                                            }
                                        </ListGroup>
                                    </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                        </Accordion>
                    )
                })}


                {/* : "Нет ни одной партии"} */}
            </Form>

        </Container>
    );


}

export default MainPartList