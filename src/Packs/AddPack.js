import React, { useEffect, useState, Fragment } from 'react';
import { Form, Button, Container, Jumbotron, Modal, Row, Col, InputGroup, Card } from 'react-bootstrap';
import PackDataService from "../Services/PackService";
import PartDataService from "../Services/PartService";
import MainPartDataService from "../Services/MainPartService";
import { useHistory, useParams } from "react-router-dom";
import { PencilFill, Printer, Trash, UpcScan, ArrowRight, List, ListCheck } from 'react-bootstrap-icons';


function AddPack() {

    const [modalShow, setModalShow] = useState(false);
    const [modalErrShow, setModalErrShow] = useState(false);
    const [validated, setValidated] = useState(false);
    const [pack, setPack] = useState({ "QPack": 0, "VPack": 0, "status": true });
    const [part, setPart] = useState(null)
    const [mainPart, setMainPart] = useState(null)
    const [otherPacks, SetOtherPacks] = useState(null)
    const [activePackId, setActivePackId] = useState(null);
    const [modalWarShow, setModalWarShow] = useState(false);
    const [activePartId, setActivePartId] = useState(null);
    let history = useHistory();
    let param = useParams();

    useEffect(() => {
        getPart();
        CalculateV();
    }, [])

    useEffect(() => {
        getOtherPacks()
    }, [mainPart])
    
    useEffect(() => {
        CalculateV()
        console.log("pack.QPack =", pack.QPack)
    }, [pack.QPack])

    async function getPart() {
        var response = await PartDataService.get(param.id)
        setPart(response)
        console.log(response, 'part');
        getMainPart(response.mainPartId) 
    }

    async function getMainPart(mainPartId) {
        var response = await MainPartDataService.get(Number(mainPartId))
        setMainPart(response)
        console.log(response, 'MainPart');
    }

    async function getOtherPacks() {
        var otherParts = mainPart && mainPart.itemParts.filter(data => {
            if (Number(param.id) !== Number(data.id)) return data
        })
        var Packets = otherParts && otherParts.reduce((prev, current) => prev.concat(current.packets), [])
        SetOtherPacks(Packets)
    }

    function getPartNameById(id) {
        debugger
        var otherParts = mainPart && mainPart.itemParts.filter(data => {
            if (Number(param.id) !== (data.id)) return data
        })
        var filteredPart = otherParts && otherParts.find(data => {
            if (id === data.id) return data
        })
        return filteredPart && filteredPart.partNumber
    }

    async function handleChange(e) {
        debugger
        const { name, value } = e.target;
        await setPack(prevState => ({ ...prevState, [name]: value }));
    }

    async function handleChangeV(e) {
        debugger
        const { name, value } = e.target;
        await setPack(prevState => ({ ...prevState, [name]: Number(value) }));
    }

    async function CalculateV() {
        debugger
        if (part && part.item.mixedLength === "No") {
            let V = part.item.thick * part.item.width * part.item.length * Number(pack.QPack) * 0.000000001
            await setPack(prevState => ({ ...prevState, VPack: V }));
        }
    }

    function setActivePack(id) {
        setActivePackId(id);
        setModalShow(true)
    }

    async function SetPackScaned(id) {
        var response = await PackDataService.markScan(id)
        if (response.status === 200) {
            getPart();
        }
    }

    async function PrintPack(id) {
        debugger
        var response = await PackDataService.PrintRpackPDF(id)
        if (response.status === 200) {
            alert('Пакет успешно распечатан');
        }
    }

    function setStatePfunc(state) {
        if (state === 0) return ("")
        if (state === 1) return ("table-success")
    }

    async function AddPack() {

        var response = await PackDataService.create(
            {
                State: 0,
                PartId: Number(param.id),
                TotalPlanks: Number(pack.QPack),
                TotalVolume: Number(pack.VPack.toFixed(3)),
                Note: pack.note,
                Status: pack.status
            }
        )

        setModalShow(false);
        return response

    }

    async function DeletePack() {
        debugger
        var response = await PackDataService.remove(activePackId)
        if (response.status === 200) {
            getPart();
        } else {
            setModalErrShow(true)
        }
        setModalShow(false)
    }


    const handleSubmit = async (event) => {
        debugger
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);


        if (pack.QPack !== undefined && !pack.VPack <= 0
            && pack.VPack !== undefined && !pack.VPack <= 0) {
            var response = await AddPack();
            if (response.status === 200) {
                getPart();
            } else {
                setModalErrShow(true)
            }
        }


    };

    function WarningModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
                    </Modal.Title>
                </Modal.Header>
                
                    {part && part.packets.length === 0 ? 
                        <Fragment>
                            <Modal.Body>
                                <h4>Нет исходных пакетов для данной партии!</h4>
                                <p>Для доступа к пересортировке. Добавьте исходные пакеты для данной партии</p>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button onClick={props.onHide}>ОК</Button>
                            </Modal.Footer>
                        </Fragment>
                        :
                        <Fragment>
                            <Modal.Body>
                                <h4>Начать пересортировку партии?</h4>
                                <p>Для доступа к пересортировке нажмите кнопку "Начать пересортировку"</p>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="warning" onClick={()=>history.push("/ResortPart/" + activePartId)}>Начать пересортировку</Button>
                                <Button onClick={props.onHide}>Отмена</Button>
                            </Modal.Footer>
                        </Fragment>
                    }

            </Modal>
        );
    }


    function DeletedModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Вы собираетесь удалить пакет</h4>
                    <p>
                        Нажмите кнопку "Удалить" для подтверждения или кнопку "Отмена"
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={DeletePack}>Удалить</Button>
                    <Button onClick={props.onHide}>Отмена</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function ErrorModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Не получилось создать пакет</h4>
                    <p>
                        Проверьте задаваемые параметры, убедитесь в наличии связи
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>Ok</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Список исходных пакетов {part && part.partNumber} </h1>
            </Jumbotron>


            <WarningModal
                show={modalWarShow}
                onHide={() => setModalWarShow(false)}
            />

            <Row>
                <Col>
                    <h3>{part && part.item.itemName}, {part && part.item.thick}x{part && part.item.width} L: {part && part.item.mixedLength === "Yes" ? "MIX" : part && part.item.length}, {part && part.item.quality}, {part && part.item.breed}, {part && part.item.side}, {part && part.item.moisture}</h3>

                </Col>
                <Col sm="4">
                    <Card>
                        <Card.Header>
                            
                            <Button type="button" onClick={() => {history.push("/AddItemPart/" + part.mainPartId)}} className="btn btn-dark" title="Список ITEM-кодов для партии"><List /> </Button>
                            <Button type="button" onClick={() => history.push("/AddPack/" + param.id)} className="btn btn-secondary" title="Список исходных пакетов партии"><List /></Button>
                            <Button type="button" onClick={() => history.push("/AddResortedPack/" + param.id)} className="btn btn-primary" title="Список пересортированных пакетов партии"><ListCheck /></Button>
                            <Button type="button" onClick={() => {  setActivePartId(param.id); setModalWarShow(true) }} className="btn btn-warning" title="Пересортировка пакетов данной партии"><ArrowRight /></Button>
                        </Card.Header>
                    </Card>
                </Col>
            </Row>
            <hr />

            <ErrorModal
                show={modalErrShow}
                onHide={() => setModalErrShow(false)}
            />

            <DeletedModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

            <Form noValidate validated={validated} onSubmit={handleSubmit}>

                <Row>

                    <Form.Group as={Col}>
                        <Form.Label>Кол-во досок в пакете, шт</Form.Label>
                        <Form.Control required type="number" min = "1" max = "2000" name="QPack" id="QPack" placeholder="Кол-во досок в пакете" value={pack.QPack} onChange={handleChange} isInvalid = {pack.QPack <= 0}/>
                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                            Пожалуйста, введите кол-во досок в пакете
                            </Form.Control.Feedback>
                    </Form.Group>


                    <Form.Group as={Col}>
                        <Form.Label>Объём пакета, м3</Form.Label>
                        <Form.Control required type="number" min = "0.001" max = "20.000" step = "0.001"  name="VPack" placeholder="Объём пакета" value={(part && part.item.mixedLength === "No") ? pack.VPack.toFixed(3) : pack.VPack} onChange={handleChangeV} disabled={part && part.item.mixedLength === "No"} isInvalid = {pack.VPack <= 0}/>
                        <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                            Пожалуйста, введите объём пакета
                            </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group as={Col}>
                        <Form.Label>Статус пакета</Form.Label>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <Button type="button" className="btn btn-success" onClick={() => setPack(prevState => ({ ...prevState, status: true }))}>OK</Button>
                                <Button type="button" className="btn btn-danger" onClick={() => setPack(prevState => ({ ...prevState, status: false }))}>NOK</Button>
                            </InputGroup.Prepend>
                            <Form.Control required type="text" name="status" placeholder="Введите статус пакета" value={pack.status === true ? "OK" : "NOK"} disabled />
                        </InputGroup>
                    </Form.Group>

                </Row>

                <Row>
                    <Form.Group as={Col}>
                        <Form.Label>Комментарий</Form.Label>
                        <Form.Control type="text" name="note" id="note" placeholder="Комментарий" value={pack.note} onChange={handleChange} />
                    </Form.Group>
                </Row>

                <Button type="button" className="btn btn-primary btn-block" onClick={handleSubmit}>Создать пакет</Button>
                <Button type="button" className="btn btn-secondary btn-block" onClick={() => history.goBack()}>Назад</Button>

            </Form>
            <p> </p>
            {part && part.packets.length > 0 ?
                <table className="table">
                    <thead>
                        <tr>
                            {/* <th>Состояние</th> */}
                            <th>Номер пакета</th>
                            <th>Кол-во досок, шт</th>
                            <th>Объём пакета, м3</th>
                            <th>Статус</th>
                            <th>Комментарий</th>
                            <th>Дата создания</th>
                            <th></th>

                        </tr>
                    </thead>
                    <tbody>
                        {part && part.packets.map(function (el, index) {
                            return (
                                <tr key={index} className={setStatePfunc(el.state)}>
                                    <td>{el.packetNumber}</td>
                                    <td>{el.totalPlanks}</td>
                                    <td>{el.totalVolume.toFixed(3)}</td>
                                    <td>{el.status ? "OK" : "NOK"}</td>
                                    <td>{el.note}</td>
                                    <td>{el.packDate}</td>
                                    <td>
                                        <Button type="button" onClick={() => history.push("/EditPack/" + el.id)} className="btn btn-success" title="Редактировать пакет"><PencilFill /></Button>
                                        <Button type="button" onClick={() => setActivePack(el.id)} className="btn btn-danger" title="Удалить пакет"><Trash /></Button>
                                        <Button type="button" onClick={() => PrintPack(el.id)} className="btn btn-info" title="Распечатать этикетку"><Printer /></Button>
                                        <Button type="button" onClick={() => SetPackScaned(el.id)} className="btn btn-secondary" title="Сделать пакет отсканированным"><UpcScan /></Button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                : "Нет пакетов партии"
            }

<hr />
            <h3>Другие пакеты партии:</h3>
            <hr />

            {(otherPacks && otherPacks.length > 0)  ?
                <table className="table">
                    <thead>
                        <tr>
                            <th>Имя партии</th>
                            <th>Номер пакета</th>
                            <th>Кол-во досок, шт</th>
                            <th>Объём пакета, м3</th>
                            <th>Статус</th>
                            <th>Комментарий</th>
                            <th>Дата создания</th>
                            <th></th>

                        </tr>
                    </thead>
                    <tbody>
                        {otherPacks && otherPacks.map(function (el, index) {
                            return (

                                <tr key={index}>

                                    <td>{getPartNameById(el.partId)}</td>
                                    <td>{el.packetNumber}</td>
                                    <td>{el.totalPlanks}</td>
                                    <td>{el.totalVolume.toFixed(3)}</td>
                                    <td>{el.status ? "OK" : "NOK"}</td>
                                    <td>{el.note}</td>
                                    <td>{el.packDate}</td>
                                    <td>
                                        <Button type="button" onClick={() => history.push("/EditPack/" + el.id)} className="btn btn-success" title="Редактировать пакет"><PencilFill /></Button>
                                        <Button type="button" onClick={() => setActivePack(el.id)} className="btn btn-danger" title="Удалить пакет"><Trash /></Button>
                                        <Button type="button" onClick={() => PrintPack(el.id)} className="btn btn-info" title="Распечатать этикетку"><Printer /></Button>
                                        <Button type="button" onClick={() => SetPackScaned(el.id)} className="btn btn-secondary" title="Сделать пакет отсканированным"><UpcScan /></Button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                : "Нет других пакетов партии"
            }
        </Container>
    )
}

export default AddPack