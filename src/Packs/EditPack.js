import React, { useEffect, useState } from 'react';
import { Form, Button, Container, Jumbotron, Modal, Row, Col, InputGroup } from 'react-bootstrap';
import PackDataService from "../Services/PackService";
import ItemDataService from "../Services/ItemService";
import { useHistory, useParams } from "react-router-dom";


function EditPack() {

    const [modalShow, setModalShow] = useState(false);
    const [validated, setValidated] = useState(false);
    const [item, setItem] = useState(null);
    const [pack, setPack] = useState({"totalPlanks" : 100, "totalVolume": 0, "status": true});
    const [itemActive, setItemActive] = useState(  {
        // "id": 0,
        // "itemName": "string",
        // "thick": 0,
        // "width": 0,
        // "length": 0,
        // "mixedLength": "No",
        // "quality": "string",
        // "breed": "string",
        // "side": "string",
        // "moisture": "string",
        // "q": 0
      });
    let history = useHistory();
    let param = useParams();

    useEffect(() => {
        async function getPack() {
            var response = await PackDataService.get(param.id) 
            setPack(response);
            console.log("PACKKKK", response)
            getItem(response.part.itemId)
        }

        async function getItem(id) {
            var response = await ItemDataService.get(id) 
            setItemActive(response);
            console.log("ITEMMMM", response)
        }
    

        getPack();
        //CalculateV();
    }, [])

    useEffect(() => {
         CalculateV()
         console.log("totalPlanks =", pack.totalPlanks)
    }, [itemActive])

    useEffect(() => {
        CalculateV()
        console.log("totalPlanks =", pack.totalPlanks)
    }, [pack.totalPlanks])
    
    async function handleChange (e) {
        debugger
        const { name, value } = e.target;
        await setPack(prevState => ({ ...prevState, [name]: value }));
    }

    async function CalculateV() {
        debugger
        if (itemActive && itemActive.mixedLength === "No") {
            let V = itemActive.thick * itemActive.width * itemActive.length * pack.totalPlanks * 0.000000001
            await setPack(prevState => ({ ...prevState, totalVolume: V }));
        }
    }

    async function UpdatePack() {

        var response = await PackDataService.update(
            Number(param.id),
            {
                State: pack.state,
                PacketNumber: Number(pack.packetNumber),
                TotalPlanks: Number(pack.totalPlanks),
                TotalVolume: Number(pack.totalVolume),
                Note: pack.note,
                Status: pack.status
            },
            {
                ParttId: pack.partId,
                ItemmId: pack.itemId
                
            }
        )

        if (response.status === 200) {
            alert('Data saved successfully');
            //history.push('/EditPartPacks/' + param.id);
        }
        else {
            alert('Data not saved');
        }
        return response

    }

    const handleSubmit = async (event) => {

        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        setValidated(true);

        //if (form.checkValidity() === true) {
            if (pack.totalPlanks > 0 
                && pack.totalVolume >0 
                && pack.totalVolume !== undefined 
                && pack.totalPlanks !== undefined) {
            var response = await UpdatePack();
            if (response.status === 200) {
                redir();
            } else {
                setModalShow(true)
            }
        }

    };

    function redir() {
        history.push("/");
        history.replace("/AddPack/" + pack.partId);
    }

    function ErrorModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Не получилось создать пакет</h4>
                    <p>
                        Проверьте задаваемые параметры, убедитесь в наличии связи
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>Ok</Button>
                </Modal.Footer>
            </Modal>
        );
    }

        return (
            <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Редактирование пакета </h1>
            </Jumbotron>

            <ErrorModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
            <Form noValidate validated={validated} onSubmit={handleSubmit}>

               <Row>
                        <Form.Group as={Col}>
                            <Form.Label>Номер пакета</Form.Label>
                            <Form.Control required type="number" name="packetNumber" id="packetNumber" placeholder="Кол-во досок в пакете" value={pack.packetNumber}  onChange={handleChange} />
                            <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                            <Form.Control.Feedback type="invalid">
                                Пожалуйста, введите номер пакета
                            </Form.Control.Feedback>
                        </Form.Group>

                    <Form.Group as={Col}>
                        <Form.Label>Статус пакета</Form.Label>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <Button type="button" className="btn btn-success" onClick={()=>setPack(prevState => ({ ...prevState, status: true }))}>OK</Button>
                                <Button type="button" className="btn btn-danger" onClick={()=>setPack(prevState => ({ ...prevState, status: false }))}>NOK</Button>
                            </InputGroup.Prepend>
                            <Form.Control required type="text" name="status" value={pack.status === true ? "OK":"NOK"}  disabled />
                        </InputGroup>
                    </Form.Group>

                    <Form.Group as={Col}>
                        <Form.Label>Локация пакета</Form.Label>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <Button type="button" className="btn btn-secondary" onClick={()=>setPack(prevState => ({ ...prevState, state: 0 }))}>NO</Button>
                                <Button type="button" className="btn btn-primary" onClick={()=>setPack(prevState => ({ ...prevState, state: 1  }))}>IN</Button>
                            </InputGroup.Prepend>
                            <Form.Control required type="text" name="state"  value={pack.state === 0 ? "Не поступал на линию" :"Пришёл на линию"}  disabled />
                        </InputGroup>
                    </Form.Group>

                </Row>

                <Row>
                
                        <Form.Group as={Col}>
                            <Form.Label>Кол-во досок в пакете, шт</Form.Label>
                            <Form.Control required type="number" name="totalPlanks" id="totalPlanks" placeholder="Кол-во досок в пакете" value={pack.totalPlanks} defaultValue={100} onChange={handleChange} />
                            <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                            <Form.Control.Feedback type="invalid">
                                Пожалуйста, введите кол-во досок в пакете
                            </Form.Control.Feedback>
                        </Form.Group>
             

                        <Form.Group as={Col}>
                            <Form.Label>Объём пакета, м3</Form.Label>
                            <Form.Control required type="number" min = "0.001" max = "20.000" step = "0.001"   name="totalVolume" placeholder="Объём пакета" value={(itemActive.mixedLength==="No") ? pack.totalVolume.toFixed(3) : Number(pack.totalVolume)} onChange={handleChange} disabled = {itemActive.mixedLength=="No"} />
                            <Form.Control.Feedback>Всё хорошо!</Form.Control.Feedback>
                            <Form.Control.Feedback type="invalid">
                                Пожалуйста, введите объём пакета
                            </Form.Control.Feedback>
                        </Form.Group>
      
                </Row>

                <Form.Group>
                    <Form.Label>Комментарий</Form.Label>
                    <Form.Control required type="text" name="note" id="note" placeholder="Комментарий" value={pack.note} onChange={handleChange} />
                </Form.Group>

                <Button type="button" className="btn btn-primary btn-block" onClick={handleSubmit}>Изменить пакет</Button>
                <Button type="button" className="btn btn-secondary btn-block" onClick={() => history.goBack()}>Назад</Button>
            </Form>

        </Container>
        )
    

}



export default EditPack