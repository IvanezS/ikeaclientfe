import React, { useState, useEffect } from 'react';
import { Form, Container, Jumbotron, Button, Modal } from 'react-bootstrap';
import PartDataService from "../Services/PartService";
import PackDataService from "../Services/PackService";
import { useHistory, useParams } from "react-router-dom";
import { PencilFill, Printer, Trash, UpcScan } from 'react-bootstrap-icons';
import { useSelector } from 'react-redux'

function PackList() {
    const user = useSelector(state => state.auth.user)
    let history = useHistory();
    let param = useParams();
    const [parties, setParties] = useState(null)
    const [packs, setPacks] = useState(null)
    const [activePackId, setActivePackId] = useState(null);
    const [modalShow, setModalShow] = useState(false);
    const [modalErrShow, setModalErrShow] = useState(false);
    const [modalWarShow, setModalWarShow] = useState(false);

    useEffect(() => {
        async function getPacks() {
            var response = await PackDataService.getByPartId(param.id);
            setPacks(response);
        }
    
        async function getPart() {
            var response = await PartDataService.get(param.id);
            setParties(response);
        }
        
        getPart();
        getPacks();


    }, [])

    async function DeletePack() {
        debugger
        var response = await PackDataService.remove(activePackId)
        if (response.status === 200) {
            history.push("/");
            history.replace("/EditPartPacks/" + param.id);
        }else{
            setModalErrShow(true)
        }
    }

    function setActivePack(id) {
        setActivePackId(id);
        setModalShow(true)
    }

    async function SetPackScaned(id) {
        var response = await PackDataService.markScan(id)
        if (response.status === 200) {
            history.push("/");
            history.replace("/EditPartPacks/" + param.id);
        }
    }

    async function PrintPack(id) {
        debugger
        var response = await PackDataService.PrintRpackPDF(id)
        if (response.status === 200) {
            alert('Пакет успешно распечатан');
        }
    }

    function setStatePfunc(state) {
        if (state === 0) return ("")
        if (state === 1) return ("table-success")
    }


    function setWarningPack(id) {
        packs && packs.length === 0 ? setModalWarShow(true) : gotoResort(id);
    }

    function gotoResort(id) {
        history.push("/ResortPart/" + id)
    }

    function DeletedModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Вы собираетесь удалить пакет</h4>
                    <p>
                        Нажмите кнопку "Удалить" для подтверждения или кнопку "Отмена"
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={DeletePack}>Удалить</Button>
                    <Button onClick={props.onHide}>Отмена</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function ErrorModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ОШИБКА!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>Что-то пошло не так. Проверьте связь, обновите страницу</p>

                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>OK</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    function WarningModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        ВНИМАНИЕ!
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Нет исходных пакетов для данной партии!</h4>
                    <p>Для доступа к пересортировке. Добавьте исходные пакеты для данной партии</p>
                </Modal.Body>
                <Modal.Footer>
                    {/* <Button variant="danger" onClick={gotoResort}>Продолжить</Button> */}
                    <Button onClick={props.onHide}>ОК</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    return (
        <Container fluid className="App">

            <Jumbotron fluid className="vertical-center justify-content-center">
                <h1 className="display-4">Список пакетов партии {parties && parties.partNumber}</h1>
            </Jumbotron>
            <p>Имя: "{parties && parties.partNumber}", Комментарий: "{parties && parties.comments}"</p>
            
            <DeletedModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

            <ErrorModal
                show={modalErrShow}
                onHide={() => setModalErrShow(false)}
            />

            <WarningModal
                show={modalWarShow}
                onHide={() => setModalWarShow(false)}
            />

            <hr />
                {user && (user.profile.role === "Admin" || user.profile.role === "Master") ?
                    <Button type="button" onClick={() => history.push("/AddPack/" + param.id)} className="btn btn-primary" title="Пересортировка пакетов данной партии">Добавить новый пакет</Button>
                :''}
                {user && (user.profile.role === "Admin" || user.profile.role === "Oper") ?
                <Button type="button" onClick={() => setWarningPack(param.id)} className="btn btn-warning" title="Пересортировка пакетов данной партии">Пересортировка</Button>
                :''}
            <hr />


            <Form className="form">
                {packs ?
                    <table className="table">
                        <thead>
                            <tr>
                                {/* <th>Состояние</th> */}
                                <th>Номер пакета</th>
                                <th>Кол-во досок, шт</th>
                                <th>Объём пакета, м3</th>
                                <th>Статус</th>
                                <th>Комментарий</th>
                                <th>Дата создания</th>
                                <th></th>

                            </tr>
                        </thead>
                        <tbody>
                            {packs && packs.map(function (el, index) {
                                return (
                                    <tr key={index} className={setStatePfunc(el.state)}>
                                        <td>{el.packetNumber}</td>
                                        <td>{el.totalPlanks}</td>
                                        <td>{el.totalVolume.toFixed(3)}</td>
                                        <td>{el.status ? "OK" : "NOK"}</td>
                                        <td>{el.note}</td>
                                        <td>{el.packDate}</td>
                                        <td>
                                            <Button type="button" onClick={() => history.push("/EditPack/" + el.id)} className="btn btn-success" title="Редактировать пакет"><PencilFill /></Button>
                                            <Button type="button" onClick={() => setActivePack(el.id)} className="btn btn-danger" title="Удалить пакет"><Trash /></Button>
                                            <Button type="button" onClick={() => PrintPack(el.id)} className="btn btn-info" title="Распечатать этикетку"><Printer /></Button>
                                            <Button type="button" onClick={() => SetPackScaned(el.id)} className="btn btn-secondary" title="Сделать пакет отсканированным"><UpcScan /></Button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                    : "Нет пакетов партии"
                }
            </Form>

        </Container>
    )

}

export default PackList